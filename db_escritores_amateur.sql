﻿CREATE DATABASE db_escritores_amateur
GO

USE db_escritores_amateur
GO

CREATE TABLE pais
(
    id_pais INT IDENTITY NOT NULL,
    nombre VARCHAR(50) NOT NULL UNIQUE DEFAULT 'México',
    CONSTRAINT pk_id_pais PRIMARY KEY(id_pais)
)

CREATE TABLE estado
(
    id_estado INT IDENTITY NOT NULL,
    nombre VARCHAR(50) NOT NULL,
    id_pais INT NOT NULL,
    CONSTRAINT pk_id_estado PRIMARY KEY(id_estado),
    CONSTRAINT fk_id_pais FOREIGN KEY(id_pais) REFERENCES pais(id_pais)
)

CREATE TABLE ciudad
(
    id_ciudad INT IDENTITY NOT NULL,
    nombre VARCHAR(50) NOT NULL,
    id_estado INT NOT NULL,
    CONSTRAINT pk_id_ciudad PRIMARY KEY(id_ciudad),
    CONSTRAINT fk_id_estado FOREIGN KEY(id_estado) REFERENCES estado(id_estado)
)

CREATE TABLE tipo_usuario
(
    id_tipo_usuario INT IDENTITY NOT NULL,
    nombre_tipo VARCHAR(50) NOT NULL,
    CONSTRAINT pk_id_tipo_usuario PRIMARY KEY(id_tipo_usuario),
    CONSTRAINT uq_nombre UNIQUE(nombre_tipo)
)

CREATE TABLE usuario
(
    id_usuario INT IDENTITY NOT NULL,
    nombre VARCHAR(50) NOT NULL,
    apellido_pat VARCHAR(50) NOT NULL,
    apellido_mat VARCHAR(50),
    correo VARCHAR(80) NOT NULL,
    avatar VARCHAR(80),
    municipio INT NOT NULL,
    telefono char(10),
    sitio_web VARCHAR(120),
    biografia TEXT,
    username VARCHAR(50) NOT NULL,
    contrasenia VARCHAR(50) NOT NULL,
    tipo_usuario int NOT NULL,
    CONSTRAINT pk_id_usuario PRIMARY KEY(id_usuario),
    CONSTRAINT fk_tipo_usuario FOREIGN KEY(tipo_usuario) REFERENCES tipo_usuario(id_tipo_usuario),
    CONSTRAINT fk_municipio FOREIGN KEY(municipio) REFERENCES ciudad(id_ciudad),
    CONSTRAINT uq_username UNIQUE(username),
    CONSTRAINT uq_correo UNIQUE(correo)
)

CREATE TABLE sinopsis
(
    id_sinopsis INT IDENTITY NOT NULL,
    cabecera VARCHAR(50),
    contenido TEXT NOT NULL,
    CONSTRAINT pk_id_sinopsis PRIMARY KEY(id_sinopsis)
)

CREATE TABLE categoria
(
    id_categoria INT IDENTITY NOT NULL,
    nombre_cat VARCHAR(50) NOT NULL,
    CONSTRAINT pk_id_categoria_hist PRIMARY KEY(id_categoria),
    CONSTRAINT uq_nombre_cat UNIQUE(nombre_cat)
)

CREATE TABLE prologo
(
    id_prologo INT IDENTITY NOT NULL,
    cabecera VARCHAR(50) NOT NULL,
    contenido TEXT NOT NULL,
    CONSTRAINT pk_id_prologo_hist PRIMARY KEY(id_prologo)
)


CREATE TABLE historia
(
    id_historia INT IDENTITY NOT NULL,
    titulo VARCHAR(50) NOT NULL,
    portada_url VARCHAR(150) NOT NULL,
    id_sinopsis INT NOT NULL,
    id_prologo INT NOT NULL,
    id_categoria INT,
    CONSTRAINT pk_id_historia PRIMARY KEY(id_historia),
    CONSTRAINT fk_id_categoria FOREIGN KEY(id_categoria) REFERENCES categoria(id_categoria),
    CONSTRAINT fk_id_prologo FOREIGN KEY(id_prologo) REFERENCES prologo(id_prologo),
    CONSTRAINT fk_id_sinopsis FOREIGN KEY(id_sinopsis) REFERENCES sinopsis(id_sinopsis)
)

CREATE TABLE comentario
(
    id_comentario INT IDENTITY NOT NULL,
    texto TEXT NOT NULL,
    fecha DATETIME NOT NULL,
    id_usuario INT NOT NULL,
    id_historia INT NOT NULL,
    CONSTRAINT pk_id_comentario PRIMARY KEY(id_comentario),
    CONSTRAINT fk_id_usuario FOREIGN KEY(id_usuario) REFERENCES usuario(id_usuario),
    CONSTRAINT fk_id_historia FOREIGN KEY(id_historia) REFERENCES historia(id_historia)
)

CREATE TABLE califica_historia
(
    --id_calificacion INT IDENTITY NOT NULL,
    id_usuario INT NOT NULL,
    id_historia INT NOT NULL,
    cant_estrellas INT NOT NULL,
    CONSTRAINT fk_id_usuario1 FOREIGN KEY(id_usuario) REFERENCES usuario(id_usuario),
    CONSTRAINT fk_id_historia1 FOREIGN KEY(id_historia) REFERENCES historia(id_historia),
    CONSTRAINT ck_cant_estrellas CHECK(cant_estrellas >= 0 AND cant_estrellas <= 5)
    --CONSTRAINT pk_id_calificacion PRIMARY KEY(id_calificacion)
)

CREATE TABLE genero
(
    id_genero INT IDENTITY NOT NULL,
    nombre_genero VARCHAR(50) UNIQUE NOT NULL,
    PRIMARY KEY(id_genero)
)

CREATE TABLE genero_historia
(
    id_genero_historia INT NOT NULL,
    id_histori INT NOT NULL,
    CONSTRAINT fk_id_genero_hist FOREIGN KEY(id_genero_historia) REFERENCES genero(id_genero),
    CONSTRAINT fk_id_historia2 FOREIGN KEY(id_histori) REFERENCES historia(id_historia)
)

CREATE TABLE usuario_escribe
(
    id_autor INT NOT NULL,
    id_historia INT NOT NULL,
    fecha DATETIME NOT NULL,
    CONSTRAINT fk_id_autor FOREIGN KEY(id_autor) REFERENCES usuario(id_usuario),
    CONSTRAINT fk_id_historia3 FOREIGN KEY(id_historia) REFERENCES historia(id_historia)
)

CREATE TABLE usuario_lee
(
    id_lector INT NOT NULL,
    id_historia INT NOT NULL,
    fecha_lectura DATETIME NOT NULL,
    CONSTRAINT fk_id_lector FOREIGN KEY(id_lector) REFERENCES usuario(id_usuario),
    CONSTRAINT fk_id_historia4 FOREIGN KEY(id_historia) REFERENCES historia(id_historia)
)


CREATE TABLE status_historia
(
	id_status INT,
	nombre_estatus VARCHAR(50) UNIQUE,
	CONSTRAINT pk_id_status PRIMARY KEY(id_status)
)

INSERT INTO status_historia VALUES(0, 'PENDIENTE');
INSERT INTO status_historia VALUES(1, 'NO ACEPTADO');
INSERT INTO status_historia VALUES(2, 'ACEPTADA');

CREATE TABLE admin_revision
(
    id_admin INT,
    id_historia INT NOT NULL UNIQUE,
    fecha_aprovacion DATETIME NOT NULL,
    id_estatus INT NOT NULL,
    CONSTRAINT fk_id_admin FOREIGN KEY(id_admin) REFERENCES usuario(id_usuario),
    CONSTRAINT fk_id_historia5 FOREIGN KEY(id_historia) REFERENCES historia(id_historia),
	CONSTRAINT fk_id_estatus FOREIGN KEY(id_estatus) REFERENCES status_historia(id_status)
)


CREATE TABLE capitulo
(
    id_capitulo INT IDENTITY NOT NULL,
    titulo VARCHAR(50) NOT NULL,
    contenido TEXT NOT NULL,
    id_historia INT NOT NULL,
    CONSTRAINT pk_id_capitulo PRIMARY KEY(id_capitulo),
    CONSTRAINT fk_id_historia6 FOREIGN KEY(id_historia) REFERENCES historia(id_historia)
)

CREATE TABLE favoritos
(
    id_favorito INT IDENTITY NOT NULL,
    ruta_url VARCHAR(120),  -- opcional
    id_historia INT NOT NULL,
    CONSTRAINT pk_id_favoritos PRIMARY KEY(id_favorito),
    CONSTRAINT fk_id_historia7 FOREIGN KEY(id_historia) REFERENCES historia(id_historia)
)

CREATE TABLE usuario_favoritos
(
    id_usuario INT NOT NULL,
    id_favorito INT NOT NULL,
    CONSTRAINT fk_id_usuario2 FOREIGN KEY(id_usuario) REFERENCES usuario(id_usuario),
    CONSTRAINT fk_id_favorito FOREIGN KEY(id_favorito) REFERENCES favoritos(id_favorito)
)

CREATE TABLE conversacion
(
    id_conversacion INT IDENTITY NOT NULL,
    id_user1 INT NOT NULL,
    id_user2 INT NOT NULL,
    CONSTRAINT pk_id_conversacion PRIMARY KEY(id_conversacion),
    CONSTRAINT fk_id_user1 FOREIGN KEY(id_user1) REFERENCES usuario(id_usuario),
    CONSTRAINT fk_id_user2 FOREIGN KEY(id_user2) REFERENCES usuario(id_usuario)
)

CREATE TABLE mensaje
(
    id_mensaje INT IDENTITY NOT NULL,
    texto TEXT NOT NULL,
    fecha DATETIME NOT NULL,
    estado INT NOT NULL, -- 0 = no intregado, 1 = entregado
    id_user_creador INT NOT NULL,
    id_conversacion INT NOT NULL,
    CONSTRAINT pk_id_mensaje PRIMARY KEY(id_mensaje),
    CONSTRAINT fk_id_user_creador FOREIGN KEY(id_user_creador) REFERENCES usuario(id_usuario),
    CONSTRAINT fk_id_conversacion FOREIGN KEY(id_conversacion) REFERENCES conversacion(id_conversacion)
)


-- INGRESAR DATOS----------------------------------------
INSERT INTO tipo_usuario VALUES('Escritor');
INSERT INTO tipo_usuario VALUES('Lector');
INSERT INTO tipo_usuario VALUES('Administrador');
---------------------------------------------------------

INSERT INTO pais VALUES('MEXICO');
INSERT INTO pais VALUES('USA');
INSERT INTO pais VALUES('POLONIA');
INSERT INTO pais VALUES('REINO UNIDO');

---------------------------------------------------------
INSERT INTO estado VALUES('YUCATAN', 1);
INSERT INTO estado VALUES('CAMPECHE', 1);
INSERT INTO estado VALUES('QUINTANA ROO', 1);
INSERT INTO estado VALUES('ESTADO DE MEXICO', 1);
INSERT INTO estado VALUES('CHIAPAS', 1);

INSERT INTO estado VALUES('TEXAS', 2);
INSERT INTO estado VALUES('CALIFORNIA', 2);
INSERT INTO estado VALUES('FLORIDA', 2);
INSERT INTO estado VALUES('ARIZONA', 2);
INSERT INTO estado VALUES('WASHINGTON', 2);

INSERT INTO estado VALUES('POMERANIAN', 3);
INSERT INTO estado VALUES('MAZOVIAN', 3);
INSERT INTO estado VALUES('LUBLIN', 3);
INSERT INTO estado VALUES('SILESIAN', 3);
INSERT INTO estado VALUES('LÓDZ', 3);

INSERT INTO estado VALUES('OXON', 4);
INSERT INTO estado VALUES('GRATER LONDON', 4);
INSERT INTO estado VALUES('DEVON', 4);
INSERT INTO estado VALUES('CUMBRIA', 4);
INSERT INTO estado VALUES('ESSEX', 4);

---------------------------------------------------------
INSERT INTO ciudad VALUES('MERIDA', 1);
INSERT INTO ciudad VALUES('UMAN', 1);
INSERT INTO ciudad VALUES('PROGRESO', 1);
INSERT INTO ciudad VALUES('CELESTUN', 1);
INSERT INTO ciudad VALUES('IZAMAL', 1);

INSERT INTO ciudad VALUES('CAMPECHE', 2);
INSERT INTO ciudad VALUES('CIUDAD DEL CARMEN', 2);
INSERT INTO ciudad VALUES('CHAMPOTÓN', 2);
INSERT INTO ciudad VALUES('HOPELCHÉN', 2);
INSERT INTO ciudad VALUES('ESCARCEGA', 2);

INSERT INTO ciudad VALUES('CANCÚN', 3);
INSERT INTO ciudad VALUES('BACALAR', 3);
INSERT INTO ciudad VALUES('ISLA MUJERES', 3);
INSERT INTO ciudad VALUES('COZUMEL', 3);
INSERT INTO ciudad VALUES('FELIPE CARRILLO PUERTO', 3);

INSERT INTO ciudad VALUES('TEXCOCO', 4);
INSERT INTO ciudad VALUES('CDMX', 4);
INSERT INTO ciudad VALUES('Valle de Bravo', 4);
INSERT INTO ciudad VALUES('Aculco', 4);
INSERT INTO ciudad VALUES('Teotihuacán', 4);
---------------------------------------------------------
INSERT INTO categoria VALUES('Guías');
INSERT INTO categoria VALUES('Juegos de mesa');
INSERT INTO categoria VALUES('Incunables');
INSERT INTO categoria VALUES('Códices');
INSERT INTO categoria VALUES('Libros ficticios');

INSERT INTO categoria VALUES('Diarios personales');
INSERT INTO categoria VALUES('Fotografía');
INSERT INTO categoria VALUES('Video juegos');
INSERT INTO categoria VALUES('Cuentos infantiles');
INSERT INTO categoria VALUES('Leyendas');

INSERT INTO categoria VALUES('Poesía');
INSERT INTO categoria VALUES('Grimorio');
INSERT INTO categoria VALUES('Historietas');
INSERT INTO categoria VALUES('Navegación');
INSERT INTO categoria VALUES('Programación');

INSERT INTO categoria VALUES('Gastronomicos');
INSERT INTO categoria VALUES('Mitos');
INSERT INTO categoria VALUES('Trabalenguas');
INSERT INTO categoria VALUES('Entrevistas');
INSERT INTO categoria VALUES('Póstumos');

---------------------------------------------------------
INSERT INTO genero VALUES('Lírica');
INSERT INTO genero VALUES('Épica o Narrativa');
INSERT INTO genero VALUES('Poema épico');
INSERT INTO genero VALUES('Romance');
INSERT INTO genero VALUES('Cuento');

INSERT INTO genero VALUES('Novela');
INSERT INTO genero VALUES('Oda');
INSERT INTO genero VALUES('Elegía');
INSERT INTO genero VALUES('Égloga');
INSERT INTO genero VALUES('Sátira');

INSERT INTO genero VALUES('Drama');
INSERT INTO genero VALUES('Tragedia');
INSERT INTO genero VALUES('Comedia');
INSERT INTO genero VALUES('Ópera');
INSERT INTO genero VALUES('Policial');

INSERT INTO genero VALUES('Aventura');
INSERT INTO genero VALUES('Terror');
INSERT INTO genero VALUES('Ciencia ficción');
INSERT INTO genero VALUES('Investigación');
INSERT INTO genero VALUES('Autoayuda');

---------------------------------------------------------
-- PROC 1
-- PROCEDIMIENTO QUE PERMITIRÁ VALIDAR SI UN USUARIO YA ESTÁ REGISTRADO, Y MANDAR UN ERROR
-- DE LO CONTRARIO EL NUEVO USUARIO SERÁ REGISTRADO
CREATE PROC SP_REGISTRO_USUARIO
(
	@nombre VARCHAR(50),
    @apellido_pat VARCHAR(50),
    @apellido_mat VARCHAR(50),
    @correo VARCHAR(80),
    @avatar VARCHAR(80),
    @municipio INT,
    @telefono char(10),
    @sitio_web VARCHAR(120),
    @biografia TEXT,
    @username VARCHAR(50),
    @contrasenia VARCHAR(50),
    @tipo_usuario int
)
AS
BEGIN
	IF EXISTS(SELECT * FROM usuario WHERE username = @username)
	BEGIN
		RAISERROR('EL USUARIO YA EXISTE CON ESTE USERNAME', 16, 1);
		RETURN -1
	END
	IF EXISTS(SELECT * FROM usuario WHERE correo = @correo)
	BEGIN
		RAISERROR('EL USUARIO YA EXISTE CON ESTE CORREO', 16, 1);
		RETURN -2
	END
	ELSE
	BEGIN
		INSERT INTO usuario(nombre, apellido_pat , apellido_mat, correo, avatar, municipio, telefono, sitio_web, biografia, username, contrasenia, tipo_usuario)
		VALUES(@nombre, @apellido_pat, @apellido_mat, @correo, @avatar, @municipio, @telefono, @sitio_web, @biografia, @username, @contrasenia, @tipo_usuario)
	END
END
GO

-- DATOS INSERTADOS
EXEC SP_REGISTRO_USUARIO 'Ricardo', 'Canul', 'Flota', 'ricardocanul7@gmail.com', null, 1, '9995852932', null, 'Soy estudiante de DSM', 'ricardocanul7', '12345678', 3;
EXEC SP_REGISTRO_USUARIO 'Alan', 'Moreno', 'Martinez', 'alanalexismoreno@gmail.com', null, 5, '9995846225', null, 'Me gusta leer libros', 'roku96', '12345678', 3;
EXEC SP_REGISTRO_USUARIO 'Aaron', 'Herrera', 'Gonzales', 'sheldon@gmail.com', null, 1, '9999685214', null, 'Me gusta crear historias', 'sheldonis', '12345678', 1;
EXEC SP_REGISTRO_USUARIO 'Jorge', 'Perez', 'Lopez', 'jorgeperez@gmail.com', null, 2, '5869254796', null, 'Me relajo leyendo historias', 'jorgelopez94', '12345678', 2;
EXEC SP_REGISTRO_USUARIO 'Pedro', 'Segovia', 'Pech', 'pedropech@gmail.com', null, 1, '9993236598', null, 'Hola soy buena onda :)', 'pedrin96', '12345678', 2;
EXEC SP_REGISTRO_USUARIO 'Isac', 'Bautista', 'Cohuo', 'chac93@gmail.com', null, 1, '9998569874', null, 'Me gusta el futbol pero tambien disfruto leer', 'chac93', '12345678', 2;
EXEC SP_REGISTRO_USUARIO 'Joaquin', 'Torres', 'Dzul', 'joaquintodz@gmail.com', null, 3, '6689587423', null, 'Me gusta perderme en los libros y sus historias', 'joaqto90', '12345678', 2;
EXEC SP_REGISTRO_USUARIO 'Seidy', 'Canul', 'Flota', 'lluviadeestrellas@gmail.com', null, 1, '9998591462', null, 'Me gusta compartir mis historias con el mundo', 'lluviadeestrellas96', '12345678', 1;
EXEC SP_REGISTRO_USUARIO 'Pamela', 'Nieto', 'Castillo', 'rizitos@gmail.com', null, 1, '9852687425', null, 'Soy deportista y en tiempos libres me gusta leer', 'rizitos92', '12345678', 2;
EXEC SP_REGISTRO_USUARIO 'Lorena', 'Pech', 'uk', 'lorepech@gmail.com', null, 1, '9632457812', null, 'Soy estilista y escritora de medio tiempo', 'lorepech', '12345678', 1;
EXEC SP_REGISTRO_USUARIO 'Roman', 'Canul', 'Perez', 'romanita@gmail.com', null, 4, '9995423687', null, 'Amante de la lectura', 'romanita92', '12345678', 2;
EXEC SP_REGISTRO_USUARIO 'Michael', 'Jonhson', null, 'michaelj@gmail.com', null, 6, '9652456582', 'www.john.us', 'Guitarrist and books lover', 'slash_john', '12345678', 2;
EXEC SP_REGISTRO_USUARIO 'Justyna', 'Klonowska', null, 'jusia@gmail.com', null, 11, '9995658412', null, 'Living in Cancún for now', 'jusia91', '12345678', 1;
EXEC SP_REGISTRO_USUARIO 'Juan', 'Figeroa', 'Sulub', 'janusz@gmail.com', null, 1, '9995624783', null, 'Disfruto leer cuando llueve', 'juancho94', '12345678', 2;
EXEC SP_REGISTRO_USUARIO 'Maria', 'Flota', 'Loeza', 'isabelflota@gmail.com', null, 1, '9991753914', null, 'Ama de casa', 'chevela80', '12345678', 1;
EXEC SP_REGISTRO_USUARIO 'José', 'Ramirez', 'Lopez', 'joselo@gmail.com', null, 1, '9996352145', null, 'Leer alimenta la mente', 'joselo', '12345678', 2;
EXEC SP_REGISTRO_USUARIO 'Gabriela', 'Toledo', 'Alcocer', 'gabuxa@gmail.com', null, 1, '9998547236', null, 'Soy cajera de banco y en tiempo libre escritora', 'gabuxa93', '12345678', 1;
EXEC SP_REGISTRO_USUARIO 'Rodrigo', 'Canul', 'Dzul', 'baterista96@gmail.com', null, 1, '9996553248', null, 'Soy baterista y estudiante', 'ginger_pastrana', '12345678', 1;
EXEC SP_REGISTRO_USUARIO 'Pablo', 'Jimenez', 'Ek', 'benedeto_chelmer@gmail.com', null, 1, '9996325157', null, 'Bajista de una banda y padre de familia', 'chelmer92', '12345678', 1;
EXEC SP_REGISTRO_USUARIO 'Daniel', 'Salazar', 'Blanco', 'dan_etem@gmail.com', null, 1, '9997523684', 'www.strange.mx', 'vocalista de una banda de metal y escritor', 'dan_etem', '12345678', 1;
---------------------------------------------------------
-- SINOPSIS ---------------------------------------------

--1
INSERT INTO sinopsis VALUES('SINOPSIS', '"Un hombre puede ser feliz con cualquier mujer mientras no la ame" —Oscar Wilde.
Esa es la filosofía del gran Alexander Blackstone.  

Nunca darle más lugar a una mujer, porque no lo merece, solamente debe darles un momento para perderse en el placer, no más. Él no necesita a una mujer a su lado, no necesita a una farsante.
Teniendo los gustos más exquisitos, ha decidido no volver a acostarse con la misma mujer en más de dos ocasiones para, así evitar cualquier pensamiento romántico. Alexander Blackstone no cree en el romanticismo.
Su pasado lo persigue creando nuevos enemigos, tratando de destruir el imperio que ha creado después de arduo trabajo; gracias a su tenacidad, imperioso control, pulcritud y esa increíble facilidad de palabra. Aunado a su sumamente meticulosa forma actuar, lo hacer ser es quien es.
El gran magnate neoyorquino: Alexander Blackstone.
Un hombre sin escrúpulos y de moral relajada, la ferocidad encarnada y dispuesto a todo por ser el mejor, siempre dispuesto a obtener el éxito, porque la suerte es para los mediocres y él es todo menos eso.
Y Alexander Blackstone, siempre, malditamente siempre gana.
Y está dispuesto a ir por ella, cueste lo que cueste, Sophie Watson le pertenecería más temprano que tarde.');
--2
INSERT INTO sinopsis VALUES('SINOPSIS', 'Para que desperdiciar ese pedazo de mujer 
Dicen que es malo jugar con el corazon 
Solo tengo que ganarmela para poder salir de aqui 
Que tan dificil puede ser enamorarla 
Es rubia tonta no se dara cuenta de que la usare para mi bien  
Eso era lo que pensaba hasta que la conoci bien, pero tengo que consentrarme despues de todo "Es solo un juego"');
--3
INSERT INTO sinopsis VALUES('SINOPSIS', 'Estoy bien, es solo que... Flash me molestó otra vez en clases y pues me sentí pésimo, incluso me dieron ganas de llorar. Pero no es nada. -Respondió, dejó sorprendido a su tío.
¿Quieres que vaya mañana a hablar con el director? -Preguntó nuevamente.  
No tío, pero gracias por preocuparte. -Respondió el castaño.  
Entiendo... eso solo empeoraría las cosas, o eso es lo que crees. No está mal sentir miedo y tampoco pena, lo que sí está mal es remprimirlo y mantenerlo oculto... deberías hablar con Flash y dejarle en claro que no te parecen divertidas sus bromas. -Reprendió el hombre.
Intentaré mañana, a ver qué acontece. -Respondió Peter.  
¿Solo eso es lo que te pasa? -Preguntó Ben una vez más. 
Sí tío, ¿por qué piensas que me pasa algo más? -Arremetió el castaño.  
Te ves enfermo, dime la verdad en este instante o te llevaré yo mismo al doctor para sacar un diagnóstico. -Ordenó el mayor. 
B-Bien... pues la verdad es que... me picó una araña. Pero estoy bien, era una araña pues normal, no era venenosa ni nada. Lo juro. -Respondió Peter.');
--4
INSERT INTO sinopsis(cabecera,contenido) VALUES('SINOPSIS', 
'Una herencia y la obsesión por el dinero traerán sorpresas.Una maldición caerá sobre dos hermanas: la desdicha de tener que convivir con dos hombres peculiares.
Descubrí.
Adentrate en la historia de estas dos mujeres luchadoras cuyo objetivo único es encontrar la felicidad.
 "Llévame contigo"')
--5
INSERT INTO sinopsis(cabecera,contenido) VALUES('SINOPSIS',
'Después de unas vacaciones de tres meses, Aria Rivers debe enfrentar las consecuencias que un amorío a corto plazo le dejó y encontrar la mejor manera de hablarlo con su padre, un hombre tan poderoso como aterrador, que probablemente terminará dándole la espalda una vez que descubra la verdad.
Daniel nunca pensó que abandonarla por dos semanas sería el tiempo suficiente para perderla por meses; no obstante, está decidido a encontrarla y no descansará hasta conseguir su perdón.')
--6
INSERT INTO sinopsis(cabecera, contenido) VALUES('SINOPSIS','Derek Hale ha perdido a su familia en un trágico incendio.  
De pronto se ve metido en un pozo de dolor, y angustia. Decidido a buscar la justicia y saber la verdad de todo lo que pasó, Derek llega a la comisaria, él y John, el padre del chico del cual ha estado enamorado desde que tenía quince años y el principal al mando de la policía en todo California, buscan todo aquello que los lleve a los culpables de la tragedia de su familia, y el motivo de hacer aquello.
¿Qué pasa cuando la amistad con John se vea afectada por lo que siente por su hijo? ¿dejará de perseguir a Stiles? O, ¿seguirá detrás del castaño hasta conseguir su corazón? y si lo consigue, ¿podrá protegerlo de las amenazas que están a su alrededor?')
--7
INSERT INTO sinopsis(cabecera, contenido) VALUES('SINOPSIS','«Esa no era su historia, solo era un ensayo para todo lo que se venía»
¿Cuántas veces no nos hemos dado cuenta que todas las personas tienen más caras que una moneda?
Sherlley Sgouru se dio cuenta poco a poco que la mayoría de la gente con la cual convivía todos los días, no eran las personas que pensaba que eran.
Todos estos años vagó entre mentiras, engaños que la harán que cambie.
Que todo lo que conocía cambie.')
--8
INSERT INTO sinopsis(cabecera, contenido) VALUES('SINOPSIS','«Harto de las mismas palabrerías de su padre, Hoseok, decide tomar sus propias cartas sobre el asunto; un contrato en blanco y Soul Partner, parece ser el desperfecto indicado»')
--9
INSERT INTO sinopsis(cabecera, contenido) VALUES('SINOPSIS','Justin creía que su mundo ya era una mierda, teniendo que soportar a las sucias personas que lo rodeaban y los problemas con lo que tenía que acarrear diariamente junto a su familia; pero se da cuenta de que la vida es mucho más difícil para otros exactamente cuando Artur Romanov, viejo amigo de los Bieber, llega a la ciudad con su esposa y sus dos hijas.
El mundo de Justin comienza a arder cuando conoce a Isabella Romanov, pero en realidad no sabe si ella es la que provoca el fuego, o la que intenta apagarlo.
La mafia trae consigo chantajes, violencia, abusos, mentiras y muertes, y nadie sabe si está destinado a soportarlo.')
--10
INSERT INTO sinopsis(cabecera, contenido) VALUES('SINOPSIS','Lucian estaba acostumbrado a lograr lo que se proponía, a que los otros hicieran lo que él imponía.
Elizabeth estaba acostumbrada a luchar por sus metas, por sus sueños. Siempre al margen de todo. 
Lucian es el jefe.
Elizabeth la secretaria.
Él es arrogante como el infierno.
Ella humilde como los cielos.
El jefe está comprometido.
Ella tiene a Elliot. Su hijo. 
Lucian Delacroix es tan sexy como un Dios celta.
Elizabeth Ward tan hermosa y dulce como una ninfa.
Ambos tienen una relación común, jefe y secretaria.
Los compromisos se rompen, llegan las órdenes y se dicta una propuesta.
Él quiere todo el gran imperio de su familia francesa.
Ella sólo quiere salvar la vida de su pequeño hijo.
Lucian se aprovecha de la precaria situación de Elizabeth y ella sabe que es su única salida para darle todo lo que no puede darle a su hijo.
Una madre está dispuesta a todo por salvar a su hijo.
Un hombre frío está dispuesto a pisotear a todos con tal de lograr sus propósitos.
Pero, ¿Qué sucede cuando la atracción es inminente? ¿Cuándo la ternura se apodera de un frío corazón de hierro? ¿Cuándo el amor se apodera del corazón de ambos? ¿Cuándo el contrato de darle una mejor vida al niño pasa a ser real? ¿Qué sucede cuando todo por lo que creías luchar era una equivocación? ¿Estás dispuesto a cambiar o sólo das un paso al costado y decides retirarte de la lucha?
Lucian no sabía nada... después de Elizabeth, lo sabe todo')
--11
INSERT INTO sinopsis(cabecera, contenido) VALUES('SINOPSIS','Una noche fría la familia de Maureen es obligada a abandonar su aquelarre, ella no comprende que sucede o a donde fueron su padre y abuelo. Lo único de lo que está consiente es que ahora huye con las dos mujeres más importante de su vida, su madre y abuela. Estas toman la decisión de buscar refugio en la vieja manada de lobos donde solían vivir cuando eran jóvenes.  
Pero para el alfa aceptar a unas brujas llegadas en mitad de la noche no es fácil. Sobre todo cuando saben los prejuicios y rencores que guardan con ardor los lobos hacia esa especie. Finalmente accede. Con condiciones.
La madre y abuela creen que han tomado la decisión correcta, más tarde Maureen se da cuenta de que lo que hicieron en realidad fue condenarlas a los malos tratos de los lobos. Con eso y el misterio de la desaparición de su padre y abuelo Maureen busca salidas fáciles por cualquier pizca de verdad.
Y entonces, como siya no fuera suficiente, un omega moribundo aparece...')
--12
INSERT INTO sinopsis(cabecera, contenido) VALUES('SINOPSIS','Cuando tenía seis años mientras jugaba con mis muñecas, escuché a mis padres hablar de un hombre que pronto vendría a trabajar para nuestra familia. Él se llamaba JungKook. Era un hombre serio, distante y tosco, de complexión atlética, alto y fuerte.
Sus ojos eran azules y su cabello negro azabache con un brillo excepcional. Su piel era pálida con algunas cicatrices que su vestimenta negra ocultaba muy bien.
Era un hombre que me daba miedo y me ponía nerviosa. Su simple contacto visual hacía que mis vellos se erizaran y mi pulso dispare por los cielos.
Pero sabía muy bien que algo escondía...')
--13
INSERT INTO sinopsis(cabecera, contenido) VALUES('SINOPSIS','Nick no se convertirá en un príncipe azul de un día para otro, sigue siendo homofóbico, el instituto tampoco tomará bien esta nueva relación, entonces, ¿cómo llevará su relación con Daniel?')
--14
INSERT INTO sinopsis(cabecera, contenido) VALUES('SINOPSIS','Stiles está en su último año de preparatoria, antes de ir a la universidad, por lo que es obvio que quiere hacer de sus últimos recuerdos algo memorable junto a sus amigos.
Pero cuando él cree que todo en su vida marcha a la perfección, a su rutina volverá un tormento del pasado, que ahora llega para convertirse en uno más grande, siendo su profesor de Ciencias. Derek Hale, el hombre del cual estuvo enamorado desde que tiene memoria, y aunque jura que ya no siente nada, él se dará cuenta de que el sol no se puede tapar con un solo dedo.
Stiles cree que el hombre no sabe de sus sentimientos, pero ambos se verán envueltos en situaciones para nada normales entre alumno y profesor, y Derek esconde un secreto que hará que sus vidas jamás vuelvan a ser las mismas.')
--15
INSERT INTO sinopsis(cabecera, contenido) VALUES('SINOPSIS','Alicia Murgot, tercera hija de los marqueses de Somerset, con su agudo gusto para los negocios y sus habilidades en las apuestas decide conquistar al hombre más adinerado de la región.
Lástima que sus expectativas tan altas se vieron opacadas cuando el marqués de Grafton, su mejor candidato a esposo descubre sus planes. Adrien Miller nunca imaginó que una pequeña broma que buscaba brindarle una lección a esa atrevida interesada, casi termina en una boda múltiple y lo más importante casando al hombre equivocado en el proceso.
Ahora Alicia tendrá que ir de camino al altar sin pena ni gloria de la mano del peor candidato posible, Jonathan Carrick, el conde de Wester, una pobre víctima de las circunstancias que como buen caballero.-a riesgo que su mejor amigo lo maté.-toma a su hermana como esposa. Sin embargo las cosas no son como parecen y quizás un matrimonio "forzado" no sea la mejor opción para ellos.
Un asesinato y los fantasmas del pasado, interferirán en cada avance o retroceso que hagan. ¿Al final quién tendrá la última palabra?
Ella era prohibida pero eso no evitó que la deseara, su precio era demasiado alto, entonces mayor debía ser su apuesta. Cuando dos corazones tan distintos se juntan es inevitable que uno salga lastimado, pero es incluso más probable que en el camino encuentren el amor.')
---------------------------------------------------------
--PROLOGOS---
--1
INSERT INTO prologo VALUES('PROLOGO', 'Todo está en absoluto silencio en casa mientras coloco el chupete e la pequeña boca de Aurora, una bebé hermosa de ojos enormes y brillantes, sostuve 
la mano del pequeño Iker mientras nos escondíamos en la alacena.
Los pesados pasos sonaban en el piso de madera, estaban cerca. No escucho la voz de Florencia, así que aprieto la manita de Iker para que me mire a los ojos, no puedo distinguir muy bien la 
oscuridad, pero sé que está asustado y no comprende lo que sucede a nuestro alrededor y eso es ahora lo mejor.
Llevo mi dedo índice a mis labios, indicándole que guarde silencio. Él asiente y repite mi acción, es un gran niño.
Entonces mi corazón se paraliza cuando escucho la risa macabra y desquiciada de una mujer. Me hiela la piel y la eriza, Iker tiembla a mi lado y el pequeño bulto que está en una canasta se 
remueve, ruego al cielo con una imperiosa plegaria que Aurora no llore ahora. Y parece que Dios me escucha y ella deja de mover su pequeño cuerpo.
—¡YO, MATE A LA ZORRA! —grita a todo pulmón, eufórica— ¡ESTA MALDITAMENTE ¡MUERTA! ¡MUERTA!
El corazón me late desmedido, mientras mi mente asume todo lo que sale de la boca de esa loca mujer. Ha matado a mi hermana. 
—Se lo merecía por zorra y robarme a mi marido, mi dinero, a mi hijo. —Sisea con rabia—. ¿Encontraron a mi hijo y a la bastarda?
—No, señora. Seguramente están con algún familiar de la mujer.
—Seguramente esa perra los deja con alguien, vámonos. 
~*~ 
Londres, 1995. 
—Mamá no llores, mami —suplico, tomando su rostro con mis manos—. Yo voy a cuidarte.
Sonríe con tristeza mientras sorbe por su nariz sonrojada, mami está triste porque papá está en el cielo.  
—Yo lo sé, bebé —llora con fuerza—. Eres lo que tu padre y yo más amamos en el mundo, Alex, no lo olvides nunca.
Besa mi frente y me abraza con fuerza. El cementerio no me gusta y no quiero ver como papá se duerme en esa caja de madera, no me gusta. Papá prometió que estaría conmigo y mamá para mi 
cumpleaños seis.
—Mami, ¿cuándo va a salir mi papá de ahí?
Su pequeña sonrisa se borra y creo que he dicho algo muy malo, porque sus ojos se vuelven a llenar de lágrimas mientras llora con fuerza, no quiero que mi mami llore.  
—Vendrá del cielo pronto, ¿verdad?  
Ella niega y mis ojos comienzan a picar, quiero ver a mi papá. Debíamos ir a trabajar juntos, él lo prometió.  
Mamá deja la rosa blanca en mi mano y se pone sobre sus rodillas a mi lado, toma mi rostro con suavidad mientras me acaricia las mejillas, lloro con fuerza mientras besa mi frente.  
—Papá está en el cielo, Alex, desde ahí va a cuidarnos y vernos siempre —niego con fuerza, mientras lloro—. No va o volver, amor, tu papi está en el cielo. Murió y cuando los hombres buenos 
mueren, se van al cielo para ser ángeles y cuidar su familia, como tu papi lo hace ahora con nosotros.  
—¡No! —lloriqueo.
Siento como mi pecho duele, no es como cuando me enfermo por mojarme cuando llueve, no es como cuando mucho chocolate. Duele de verdad, como cuando como sopa muy caliente.
—Ahora, tú eres el hombre de la casa, Alex. Será tan bueno como tu padre, mi amor.');
--2
INSERT INTO prologo VALUES('PROLOGO', 'Ví a una niña rubia llorando, me acerqué a ella para ver que le pasaba. 

-Oye, que te pasa?-dije rascandome la nuca algo incomodo
-Me caí y me lastimé la rodilla-dijo entre sollosos
-Ah, y donde te pegaste?-dije ya que no le habia prestado atencion
-Te dije que me pegué en la rodilla pedazo de imbecil presta atención-dijo en un tono irritado y tristre
-Ven, acompañame-dije tendiendole mi mano y mostrandole mi mejor sonrisa
-Y mi papá?-dijo preocupada
-Y a quien le interesa, solo ven conmigo- dije sonriendo y entralazando su mano con la mia
-E-está b-bien...-dijo sonrojada 

Es tan linda, todo va acorde a su hermoso rostro; su cabello rubio, su hermosa y suave piel blanca, sus lindas ropas y sus hermosos y chocolatados ojos. 

-Espera un momento-dijo ella soltando mi mano-Cual es tu nombre? 

Claro, como tan imbecil para no decirle mi nombre. 

-Natsu Dragneel, y el tuyo dulce dama?- dije yo haciendo una reverencia, a la cual a ella se le escapo una sonrisa
-Lucy Heartfilia, o solo dime Luce-dijo ella con un leve sonrojo
-Heartfilia que lindo apellido, pero debemos seguir, pase usted primero-dije denuevo haciendo una reverencia y dejando que camine adelante
-Gracias noble joven Natsu- dijo riendo
-El placer es todo mio-dije yo 

El resto del camino fue miy entretenido, lleno de bromas y juegos. Hasta que llegamos al pequeño lago. 

-Que hermoso lugar, me gustaria vivir aqui o por lo menos venir todos los dias-dijo muy emocionada
-Bueno podrias ir al parque, yo te puedo traer aquí, así nos podríamos bañar en el lago-dije yo sonrojandome
-Claro y para que querias traerme aqui?-me pregunto Lucy
-Para lavarte la rodilla ,por lo que veo ya es inecesario, y por que te queria mostrar mi escondite- dije mirando al horizonte y que al darme vuelta me encontre con sus grandes ojos color chocolate
-Aa, pero que lindo escondite es el que tienes aqui- dijo Lucy-Natsu creo que ya es hora de irme.
-Oh claro vamos Luce- dije yo  

El camino fue muy agradable conversando con ella, ademas es muy tierna, me conto que tiene 3 hermanos que son; Laxus, Sting y Rogue, y que tiene un primo que se llama Jeral, que porcierto es muy aburrido, segun Lucy.
-Adios Natsu-dijo ella meneando la mano y sonriendo
-Adios Luce-dije yo igualando su gesto 

Despierto sudando otra vez debo dejar de soñar lo mismo, no puedo cambiar de sueño. No es normal soñarlo cada noche. 

-Natsu, vamos a llegar tarde-dijo la voz de mi hermana menor Wendy entrando con su gato Charle
-Es necesario ir?, Wendy son vacaciones-dije yo refregandome los ojos y haciendole cariño a mi gato llamado Happy
-Pero Natsu, vamos, es un campamento "Fairy Tail" va a ser divertido, hasta van tus amigos-dijo tirandose sobre mi.
-Esta bien-dije-pero antes-y le empece a hacer cosquillas 

Después de todo el ajetreo de la mañana me detengo a pensar... Algún día te volveré a ver... Luce. 

-Natsuuu... vamos rapido-dijo bajandome de mi nube mi hermosa hermanita
-Ya voy Wend...-dije pero fui interrumpido por agua fria
-Ja callo el horno de patatas-dijo el desnudista de Gray 
-Si, cuanto era lo de la apuesta princesa de hielo-dijo el tornillo de Gajeel
-Como se los ocurre pedazos de..-dije pero fui interrumpido
-Estan peliando?-pregunto Erza con un aura oscura
-No como se te ocurre-dijimos en un unisonio los 3
-No se solo elevaron la voz, a y votaron mi pastel-dijo Titania 
-Mierda-dijimos los 3');
--3
INSERT INTO prologo VALUES('PROLOGO', 'En el principio de todas las cosas, existió un ser. Un ser más antiguo que el tiempo mismo y más poderoso que cualquier titán, ¿su nombre? La Presencia.
Una deidad, que solo unos pocos han logrado apreciar en su máximo esplendor, en ocasiones manifestada como seres territoriales o como se les suele decir: 
Avatares, el más conocido de todos ellos es el The One Above All, que es la representación física de la fuerza y poder que La Presencia posee.');
--4
INSERT INTO prologo(cabecera, contenido) 
VALUES('PROLOGO',
'-Eloenor, esposa. Llama a Eddyth y Emmaline. Diles... Diles que vengan,quiero...despedirme de mis hijas...antes...de partir. Ya es hora.-Pidió Domingo Acevedo a su cuñada, actual esposa,costándole hablar, debido a su avanzada enfermedad que poco a poco iba deteriorándolo más y más. El dolor que sentía lo estaba consumiendo poco a poco. Estaba decidido a irse, a abandonar la tierra.
En el fondo estaba feliz por ello, había disfrutado de su vida junto a su difunta esposa, la madre de sus hijas, se había casado con su cuñada para evitar que las niñas quedaran huerfanas pero a pesar de eso no había vueto a ser feliz y mucho menos a amar.
En cambio ahora volvería a reencontrarse con la madre de sus hijas, con la mujer que realmente había amado desde el primer día que la vio, a la primera y última.
A pesar de verse obligado a abandonar a las niñas, estaba feliz. Sabía que al fin descanzaría en paz.
Al cabo de unos minutos, las niñas, ingresaron a la oscura y silenciosa habitación. Ésta, como toda la casa, se encontraba desde que el señor Domingo había enfermado, con aspecto triste.
La puerta sonó y dos niñas hermosas, una rubia y otra morocha de piel blanca como la espuma entraron en la habitación.
-Padre, ya estamos aquí, tranquilo.
-¿Papi que le ocurre? ¿Se irá con mamá y nos dejará solas aquí?- dijo la niña más pequeña, Eddyth, que había entrado en el circulo de la vida hacía unos seis años, mientras que Emmaline  llevaba un par de años más.
-Calla, ¡Eddyth!-gruñó su hermana mayor.-Eso no pasará, padre es fuerte y se repondrá. -Y conduciendo la mirada a su padre agregó: -¿No es así, padre?
El hombre, cada vez más dolorido y con pocas fuerzas para hablar, las escuchaba discutir y sonreía. Esas niñas eran el reflejo de su madre, no solo se parecían a su esposa fisicamente, sino también en la forma de discutir, de hablar, de caminar...
-Niñas, por favor... no...no peleen. -el hombre hizo una pausa para respirar y continuó.-quiero...que se acerquen y me miren...-las niñas se acercaron y cada una de ellas les dió un beso en la mejilla.-algún día entenderán... que en esta vida, hijas mías, venimos, caminamos, a veces corremos, a veces nos detenemos a descanzar... es un largo camino por recorrer y ustedes están recién empezándolo, pero sepan algo: en todo paso que den, nunca dejen de sonreir. Nada es fácil en esta vida queridas mías, pero con voluntad y esfuerzo se puede...Eddy, Emmita: me voy en busca de mamá,  me está esperando, no quiero llegar tarde.
Sus palabras salieron despacio de su boca, le costaba pronunciarlas, su fiebre cada vez subia más.
-¿Y por qué no puedo ir yo también a ver a mamá?-la pequeña Eddyth insistía y su hermana desistía de hacerla dejar de hablar. Las palabras de su padre la habían conmovido más de lo esperado que se conmoviese una niña de su edad. A diferencia de Eddyth, ella sí había comprendido lo que aquellas palabras querían decir.
-Ana, encárgate de sus medicinas. -intentó sanjar Eleonor, la tía y madrastra de las niñas al tiempo que las tomaba de la mano para obligarlas a salir de allí.-Vamos niñas, su padre necesita dormir. Vamos...
 
-Eleonor, deja a mis hijas aquí. Deja que se acerquen, quiero despedirme...como corresponde.-Interumpió, el señor Acevedo la acción de su mujer. 
Las niñas obeciéndo a su padre, se acercaron a la cama con  poca intención de querer alejarse de allí.
-¿Despedirse padre? A dónde se va sin nosotras? Yo quiero ir contigo, espera que voy a buscar mis cosas... -Dijo Eddyth corriendo hacia la puerta. Con lágrimas en los ojos, su hermana, la detuvo y la cargó hasta la cama  dónde se sentaron y se abrazaron conduciéndo la mirada a la de su padre.
-Las amo. Ya...nos volveremos a ver. Solo busquen siempre ser felices y cuidénse. La una a la otra.
Las niñas asintieron con lágrimas en los ojos. Eddyth esta vez no dijo nada, se limitó a callar. Pareciera como si al fin comprendiera la partida final del juego y callara para asumir la jugada. 
Se cercaron aún más a Domingo, dónde éste les dedicó un prolongado beso a cada una. Un beso que sin dudarlo, quedaría grabado en sus corazones para siempre.')
--5
INSERT INTO prologo(cabecera, contenido) 
VALUES('PROLOGO', 
'Los errores estaban hechos para que uno aprendiera de ellos, eso era algo que siempre tuve claro a lo largo de mi vida. Sin embargo, nunca creí que llegaría a cometer el error de perder algo tan importante por dinero, por una posición que quizás nunca sería adecuada para mí y por lo tanto no debía ambicionar.
Hace más de cuatro meses que no había vuelto a saber nada de Aria, había contratado a alguien para que la buscara en Miami pero con los escasos datos que tenía de ella —su nombre y aspecto físico, porque sí, nunca me interesé en saber un poco más de su vida—, fue imposible que diera con ella.
En un principio mi intención había sido informarle que no estaba sola, que a pesar de no estar de acuerdo con muchas cosas, ella tendría mi apoyo económico, sólo eso porque nunca le ofrecí nada más. No obstante, con el pasar de los días, semanas y meses ellos se habían convertido nuevamente en mi obsesión y ya no estaba seguro si estaba haciendo lo correcto.
Cuando partí a Miami hace varios meses lo hice con la intención de prepararme para lo inevitable, pero desde el primer día Aria se había puesto en mi camino poniendo mi mundo patas arriba, haciendo que recordara lo lindo que era reír, divertirse y pasar un buen rato.
Ella había sido la frescura que mi vida necesitaba después de tantos años de trabajo; y yo... Le había arruinado la vida y roto el corazón sin detenerme a pensar un solo segundo en ella, o mejor dicho en ellos.
En ese momento para mí, mi única familia estaba en Londres; mis padres y mis hermanos, los que siempre habían sido mi prioridad, por lo que fue tan fácil darles la espalda y salir huyendo que ahora recordarlo sólo hacía que me diera vergüenza y ganas de vomitar.
La había considerado poca cosa, alguien que era vulgar e ignorante sólo porque sabía sonreír y verle el lado positivo a la vida; y la última vez que la vi, la había considerado una horrible piedra en mi camino que debía patear y enviar lejos si quería triunfar.
Y ahora... Ahora sabía que esa mujer podía aplastar todos mis años de trabajo con un simple chasquido y aun así no podría odiarla porque la amaba con cada fibra de mi ser y me merecía lo peor.
Los aplausos hicieron que regresara a la realidad y me apoyara en la mesa de bebidas viendo como todo el mundo admiraba a la hija menor de los Rivers; la mujer que enamoré, rechacé y abandoné hace más de cuatro meses cuando entre los dos ya existía un lazo que jamás podría romperse.')
--6
INSERT INTO prologo(cabecera, contenido) 
VALUES ('PROLOGO','Corro lo más rápido que mis pies me lo permiten, siento mi corazón palpitar fuerte, pero eso no me importa, solo quiero llegar a donde los carros de bomberos van, y aunque no quiera admitirlo, sé a donde se dirigen.
Mis músculos arden, pero no me importa, quiero llegar y solo ver que lo que he escuchado de boca de la gente ha sido una maldita broma.
Por favor, que sea así.
Cuando entro en el camino que se dirige a mi casa, siento el olor a cenizas y mis ojos se llenan de lagrimas, por favor, por favor no. Corro más rápido y de pronto estoy llegando a mi casa a la velocidad de la luz. Y ahí lo veo, está pasando, esto es real.
Mi casa esta en llamas, los bomberos han sacado las mangueras grandes y tratan de controlar las llamas que se alzan por encima de los arboles, me aproximo a la casa y trato de entrar en ella, pero alguien me detiene, no me importa quien sea, lo empujo con fuerza y corro hacia la entrada, pero de nuevo, alguien me atrapa, y luego alguien más y otro mas y otro, y de pronto son cinco personas las que intentan detenerme.
—¡POR FAVOR! ¡DÉJENME ENTRAR! MI FAMILIA ESTA ADENTRO— Pero nadie me lo permite, las lagrimas se derraman por mis mejillas y grito a mi madre, pero no hay respuesta, solo la gente tratando de detenerme.
Mi garganta arde por gritar fuerte, pero no le presto atención, solo quiero ver a mi mamá y que me diga que todo está bien, que nada malo ha sucedido, pero la opresión y el dolor en mi pecho me indican que eso no va a pasar. Siento una aguja en mi brazo y sigo luchando para que me suelten, pero mis músculos comienzan a fallar, y ya no siento las piernas y de pronto, la oscuridad me consume.')
--7
INSERT INTO prologo(cabecera, contenido) 
VALUES ('PROLOGO','"Escribo esto mientras estoy sentada. Lo escribo en una libreta la cual tendré bajo llave, para jamás olvidar lo que pasó.
Justo un año después de que me recuperé de mi mala gripa y mi choque de medicamentos, ocurrió lo que jamás me había pasado por la mente que sucediera.
Estaba en la sala, esperando a Nicolás. Se había ido al trabajo en la mañana, y gracias a que lo regañe por no decirme sobre lo de su empresa, le ayude y entre los dos la levantamos.
Tenía que renovar el contrato con los japoneses, y quería recibirlo como se debe. Quedé leyendo un libro, habían dado las once de la noche y se escuchó un portazo en la puerta principal. Era Nicolás, venía desarreglo y molesto.
— ¿Pasó algo? —preguntó. Él seguía riéndose.
No entendía. 
—Maldita sea, ¿no estabas dormida ya? —habló groseramente y me miró. Sus ojos estaban rojos y tenían bolsas debajo de ellos.
Seguía sin entender, me acerque a él.
—Apestas a alcohol y... —habían dos olores más. Pero el que más me sorprendió fue uno, perfume de mujer. Lo miré—. ¿Qué pasó?
— ¿Qué debía de pasar? —preguntó con sarcasmo—. Declinaron la oferta que les hice. Maldita sea.
Suspire. Debía de entenderlo, era difícil que algo en que habías trabajado tanto se viera mal.
Con mis manos le acomodé la camisa. Un gran nudo en la garganta se comenzó a hacer. Él seguía ido, es su mente estaba en otro mundo mientras yo repasaba con mis dedos el hallazgo.
Salieron lágrimas de mis ojos. Un beso de una mujer en la camisa.
— ¿Qué es esto, Nicolás? —no hablo—. ¡Te estoy hablando Nicolás Rousset! ¿Qué mierda es esto?
Me alejó de él tomando ambas manos y dejándome ahí. Lo jale del brazo con todas mis fuerzas –por lo ido que estaba–se estampó contra un florero y lo quebró.
— ¿Pues de qué diablos hablas, mujer? ¿Qué mosca te picó? —preguntó enojado mientras miraba el jarrón.
— ¿Yo? —hablé con sarcasmo—. ¡Tienes un maldito beso en tu camisa y vienes todo desarreglado! ¿Puedes explicarme eso?
Se miró en el espejo.
—Yo no veo nada —habló cínico.
— ¿Me estás jugando de loca, Nicolás? —hablé enojada.
—Estoy cansado, no empieces.
Salió de la habitación dejándome sola y con la jarrón en el piso. En mi cuerpo iba y venía la cólera. Tomé un pedazo de papel de una revista y recogí los pedazo de cerámica del piso. Mis lágrimas mojaron el papel y tiré todo en el bote de basura.
Iba dispuesta a enfrentarlo cuando sonó el timbre y fui hacia la puerta. Abrí la puerta y estaba Isa.
El mismo labial que había encontrado en el cuello de la camisa de Nicolás.
Le solté una cachetada tan sonora que resonó en toda la sala. Ella me miró con orgullo.
—Fuiste tú.
—Fui yo —habló victoriosa—. Pensé que no te darías cuenta nunca.
Mis lágrimas seguían saliendo de mis ojos. Mi alma comenzaba a romperse de pedazo en pedazo mientras la miraba sobandose la mejilla.
—Eras mi hermana... —susurre y me miró—. Largate de mi casa. 
—Está bien, solamente dile a Nick que dejó su corbata —me la aventó en la cara—. Adiós.
—Maldita zorra malnacida —grité y ella se paró en seco. Se giró molesta—. Que triste que tengas que mendigar amor con hombres casados, ¿qué se siente siempre ser la del acostón?
—Retractate, Sgouru.
Habló molesta.
—No, porque dije la verdad Isabella —hablé su nombre completo y crujió los dientes molesta—. Ya vete, mis perros huelen las cosas echadas a perder y tienden morderlas.
—Maldita hija de... —quiso acercarse.
—Seguridad —hablé recta y David se acercó a tomarla de los brazos.
—Siempre maldita, siempre te has ganado todo lo que yo quería. Esposo guapo que te ame, hijos adorables, familia que te quiera... ¿Y yo? —preguntó gritando—. Pues bienvenida a mi infierno, Sherlley. Aquí yo soy la jefa.
Me solté riendo.
—Mira mamasita, la perra y jefa soy yo. La dueña de esta casa y señora Rousset soy yo, así que te me largas en este momento de mi casa y mi vida ¡No te quiero volver a ver! —sonreí—. ¿Sabes algo? No te proclames como la más perra, porque ni a Chihuahua llegas, cariño.
—David, sacala de aquí.
Él asintió y cerré la puerta. Fui directo hacia mi habitación en donde estaba el susodicho. Salió de bañarse y se veía que le dolía la cabeza.
Tomé una de mis maletas y metí todas mis cosas que habían en el ropero, él miró todo y estaba desorientado. Comencé a aventar todos mis zapatos hacia la cama.
— ¿Qué mierda...? —preguntó mirando todo—. ¿Qué haces, Sherlley? —preguntó extrañado.
—Nada que te importe Nicolás. No te quiero cerca de mí —hablé al mirar que acercaba.
—No entiendo.
Solté una carcajada.
Fui hacia el cesto de la ropa sucia y saqué su camisa. Desdoblé el cuello y ahí estaba aún el beso, le enseñé el cuello y lo miró extraño, frunció el ceño después.
— ¿Ya te refresque la memoria, Rousset? —pregunté con veneno—. De todas las personas del mundo con las cuales pudiste ponerme el cuerno... ¿Con mi mejor amiga?
— ¿Qué?
—Te odio Nicolás... ¡Te odio, maldita sea! —grité mientras buscaba las tijeras y cortaba toda la camisa en pedazos.
Me miraba como si estuviese loca, y eso, me ponía más furiosa.
—Sherlley, no te puedes ir. Todo tiene una explicación, amor.
Le di una cachetada tan fuerte que mi mano ya ardía por pegar tan fuerte dos veces.
— ¿Y te atreves a decirme amor? —pregunté dolida—. No encuentro explicación alguna, ¿ella qué tiene que no tenga yo? ¿Por qué fuiste a buscar en ella el consuelo y con tu esposa no?
No hablaba. 
—Me llevaré a mis hijos conmigo.
Él negó enojado.
—Puedes irte tú, pero a mis hijos no te los vas a llevar.
— ¡Pues debió de importate eso antes de hacer lo que hiciste! —grité enojada—. Ah, y dice Isa que aquí tienes tu corbata, se te olvidó allá.
Se la aventé en la cara.
—Se me olvidó...
Él me miró con lágrimas en los ojos y le respondí de la misma manera.
—No lo digas...
—Quiero el divorcio, Nicolás.
Él negó muchas veces. Pasaron los días y todo empeoró, él me contestaba fastidiado y yo hacía lo mismo. Una semana después de lo acontecido nos divorciamos dejando a nuestros hijos como un acuerdo personal de ambos sin llevarlo al juzgado.
Me fuí de ahí. Mi padre quería buscarlo y golpearlo, pero lo de tuve diciéndole que él era el padre de sus nietos, que no quería que ellos salieran afectados. Ivvy se volteó contra mí, era obvio, siempre lo quiso más a él que a mí. Por más que Nicolás la ha regañado y yo he hecho lo mismo, por más que todos nuestros familiares han hablado, que le hemos castigado hasta el aire. Nada, sigue igual y ya me cansé. Va con el psicólogo, pero sigue sin funcionar, me tiene como la mujer mala sin corazón.
He tomado terapias para callar el dolor, pero el odio que siento contra él y él hacia mí es muy fuerte.
Aunque nuestro mutuo odio nunca se ha metido con nuestros hijos, que ya es lo único que nos une."')
--8
INSERT INTO prologo(cabecera, contenido) 
VALUES ('PROLOGO','Si el señor Jung hubiese tenido la oportunidad de describir a su hijo hace trece años atrás, lo hubiese descrito como el niño más lindo, carismático y amoroso del universo entero. Su luz, su esperanza, su sol.
Su vida entera.
Pero ahora, y por cosas de su propia culpa e índole, delante de sus ojos se hallaba un bello joven de veintitrés años. Un poco más alto que él. Comprendiendo de aquella falta de empatía, bondad y felicidad en sus ojos. Ya no era aquel pequeño niño de sonrisa angelical, ahora, era un hombre de fuertes pensares ególatras y egocéntricos.
Un joven de sueños rotos y caprichos ambiciosos; que solo cumplió con sus órdenes y mandatos.
Cruel error.
Una mesa de madera tallada finamente de abeto los separaba. El ambiente era intranquilo y confuso a pesar de saber muy bien el por qué de aquella reunión familiar. Padre e hijo se amenazaban entre mirada y mirada. Hoseok sabía muy bien el motivo de aquella llamada que le había hecho su padre, razón, por la cual no había dejado de fruncir su ceño desde que había puesto un pie en la oficina. En cambio, el señor Jung estaría dispuesto a pagar con creces cada mala palabra y pasada que le había hecho pasar y sentir a su hijo, lástima, que pudiese ser de la peor forma.
—Si es lo que pienso, inmediatamente le digo que no —su voz sonó demandante y enojada
—No te estoy obligando a tener una pareja y te cases con ella. Solo te estoy pidiendo, que por lo menos, tengas un amigo, Hoseok.
Jung mayor ya se estaba cansando de la terquedad de su hijo.
—Estudio y trabajo a tiempo completo, no estoy para sus estupideces, padre.
Obviamente, Hoseok no se iba a quedar atrás.
—Nunca te he visto siquiera uno. No me gusta verte so-
Pero fue interrumpido.
—¿Solo? —espetó con burla—. Los dos sabemos muy bien que siempre ha sido así. Y no, ¡No menciones aquel tema! —gritó. Levantándose de golpe de la silla, chirriando la misma y logrando que ambas manos cayeran de lleno contra la mesa en un sonido estridente—. Pero si eso es lo que quieres. Lo tendrás.
El sonido de un fuerte portazo se escuchó dentro de toda la habitación. 
A Hoseok le importó una reverenda mierda haber salido de la oficina con aquella actitud y de haber oído una afable grosería salir de los labios de su padre. Y aunque sabía de antemano que debería de cambiarla, no quería, no podía. Porque cada vez que trataba, las palabras de aquel hombre llegaban a su cabeza como timbales de estruendos: Es tiempo de que tengas amigos Hoseok, no puedes vivir toda tu vida solo. Debes de enfrentar las consecuencias del pasado, no debes de aferrarte a el como si tu vida dependiera de ello. Y aunque tuviera razón, Hoseok no se la daría.
Apretó sus puños.
La rabia y la furia contenida no hicieron nada más que hacerlo ignorar involuntariamente al conserje del edificio; sintiéndose mal cuando se dio cuenta. Yendo directamente a la parada de autobús para ir a la casa de su padre.
Porque Hoseok no era ningún hijito de papi para no tomar el autobús.
El silencio fue estremecedor cuando se subió, acarreando su mirada al frío paisaje de su andar, pensando en las distintas circunstancias de su vida. En las mil y un maneras de arreglarlas sin tener que recurrir siempre a los malos tratos; dibujando garabatos en el vidrio.
—A lo mejor él tiene razón Hoseok, no puedes pretender que la soledad te sacará de la misma. Es momento de cambiar, aunque probablemente la termine liando como siempre —concluyó—. Más con lo que tengo pensado en hacer —susurró contra su reflejo
Suspiró frustrado ante tales pensamientos, dejando una vaga estela de vaho en el cristal de la ventana.
En menos de quince minutos, ya estaría de vuelta en casa.
El señor Jung siempre había sido un hombre de muy pocas palabras, arrogante y soñador cuando menos te lo esperabas, sin embargo, era un hombre de negocios y malos embrollos, tratando de facilitar la vida de su hijo en miramientos y dinero. Y conllevar a x personas para que se hicieran amigos de Hoseok, fue la gota que había derramado el vaso para explotar.
Logrando que la ética y moral se fueran por la borda.
Sus pies lo llevaron a cuestas al despacho de señor Jung cuando llegó a casa, porque: ¿qué mejor manera de pagarle a alguien para que sea su amigo? Después de todo, ¿no era ello lo que su padre tanto quería y anhelaba? Hoseok no era tonto, y no iba a volver a serlo, por ello, y sin tener ninguna gota de remordimiento ante su desconocida víctima, rebuscó entre los cajones y papeles de su padre algún contrato en blanco y con firma para robarlo.
Porque, si esto era lo que tanto quería el señor Jung, lo tendría.
—Ahora solo falta mi víctima.')
--9
INSERT INTO prologo(cabecera, contenido) 
VALUES ('PROLOGO','Y así llegó, como una hoja verde y fresca que el viento trajo. Con una oleada de polvo que al principio me entorpeció, pero con una calidad que al final de todo logró hacerme sentir cómodo y aliviado.
Me ahogó, su personalidad real me sobrepasó, sobrepasó todas mis expectativas, todos mis límites, de forma en que se metió bajo mi piel de una manera adictiva.
Ella era rebelde sin causa, pero yo podría asegurarle a quien quisiera de que esa no era su naturalidad. Yo la podría describir perfectamente ya que ella misma me había dejado conocerla en lo más profundo de su ser. Se había metido en mi corazón de la misma forma en que yo me metí en el suyo y mi más grande error había sido notarlo después de tiempo.
Ella era débil, pequeña, era una chica que solo había aprendido a luchar contra el dolor usando su máscara de rebeldía, y por esa razón me había cautivado, logrando anclarse en lo más profundo de mi corazón de piedra, porque a pesar de todo teníamos algo en común; ambos queríamos escapar del sucio mundo que nos rodeaba.')
--10
INSERT INTO prologo(cabecera, contenido) 
VALUES ('PROLOGO','Sólo quería cumplir mis sueños y para ello tenía que salir de este pueblo.
Ésas fueron las palabras que dije en cuanto comenté que quería ir a estudiar a Nueva York.
―¿Cómo piensas sobrevivir?― Preguntó mi madre mientras se sentaba en el sofá.―¿Para qué quieres estudiar? Muy pronto te casarás y tendrás que dedicarte a tu marido.―Odiaba el pensamiento machista que tenían mis padres y el pueblo en general.
Las mujeres no eran autónomas, su mayor sueño era tener hijos y un marido quien se encargaría de ellas... No se permitían soñar con otra cosa, no eran libres, eran prisioneros de un pensamiento retrograda inculcado por padres y madres de mentes cerradas y cuadradas, yo no quería ser como ellas. No quería esto.
Eran pocos lo que salían de este pueblo, para nunca volver, para ser la excepción. Yo quería eso.
―No me casaré.―Anuncié. Mi padre se levantó de golpe de su cómodo asiento y caminó hacia mí, levanté mi mentón con todo el orgullo de un Ward.―Estudiaré artes.―Esta vez crucé la línea, era inaudito para mi familia que fuera una artista, era algo profano y repugnante, siempre me lo decían... recordando el gran error de mi tío. El único Ward que se salió de las expectativas de la familia, el único que me apoyó en mi sueño, en mi ideología, en mis creencias, creyó y forjó en mi éstas esperanzas, ésta manera de ver el mundo.
Y mi padre me golpeó.
―No me decepciones así.―Sus ojos reflejaban aquella ira que mostraba cada vez que yo hablaba de mis sueños y espectativas.―¿Por qué no puedes ser como tu hermana, quien decidió casarse y dedicarse a su familia o por lo menos como tu hermano?―Parecía harto de tener que luchar contra mí y mis sueños.―¿Por qué siempre estás dándonos vergüenza en vez de paz y alegrías?―Mis ojos estaban llenos de lágrimas, pero no me permití llorar, no delante de ellos para desmostrarles lo débil que era. No. Yo no era así, mi orgullo no me permitía doblegarme ante nada ni nadie.
―Nada cambiará mi decisión.― Susurré.
―Niña ingrata, ¡Dios se encargará de castigarte!―Exclamó mi madre mientras se levantaba de golpe.
―Pues, estaré esperándolo con los brazos abiertos.―Los ojos de mis anticuados padres se posaron en mí.
―¡Blasfemia!―Mi madre jadeo y se desmayó.
Corrimos a socorrerla, ella era así. Dramática. Siempre que había una discusión y cuando se veía acorralada entre la espada y la pared, se desmayaba y todo se acababa, dando por vencedora a la Señora Mariela. Mi madre.')
--11
INSERT INTO prologo(cabecera, contenido) 
VALUES ('PROLOGO','Mi cuerpo no hace más que estremecerse, el frío se cuela sin problema por las rotas ventanas del viejo carruaje. Las mantas que me cubren no son suficientes para proteger mi pequeño cuerpo del frío cruel.
Mis ojos buscan los de mi madre quien también parece asustada, quiere protegerme de la verdad pero sé lo que sucede. 
Estamos vulnerables, expuestas y sin mi padre...
Lágrimas de dolor y miedo ruedan por mi helado rostro. Me sobresalto al sentir una mano tibia rozar mi mejilla, en un instante sé que es mi abuela. 
— No llores, Mau —mis dientes castañetean—, ¿Tienes frío? —asiento incapaz de hablar, mi abuela lleva su mirada hasta la nuca de quien guía a los caballos para luego chocar sus ojos conmigo—. Recuerdas lo que te enseñe ¿verdad? —niego.
Mi abuela me ha querido enseñar tantas cosas en tan poco tiempo. No sé qué quiere decir con eso.
— Fuego —susurra en mi oído, relamo mis labios y con nervios asiento—. Extiende tu mano —hago lo que me dice, dejo mi palma al descubierto con mis dedos extendidos, puedo ver mis pequeñas uñas negras—. Ahora, concéntrate —cierro mis ojos recordando—, solo piensa en lo que quieres y lo tendrás.
Un escalofríos recorre mi cuerpo entero, siento la familiar corriente espesa desplazarse hacia un punto en particular, mi mano.
"Escucha, hija del bosque.
Sentirás arder tu sangre
Cuando magia haya en ella
Espesa es su voluntad.
Hija del bosque,
Tu alma va a chispear
Cuando llegue el final".
— Imagínalo, piensa en su forma su color... —la voz tranquila de mi abuela me saca de mi mente.
Mi cerebro recrea la imagen de una fogata, con sus majestuosas llamas anaranjadas.
— Abre los ojos, Maureen.
Los abro.
Muerdo mi labio impresionada intentando no llamar la atención.
Mi mano.
En ella hay una pequeña flama que la calienta, pero no al punto de quemar. Muevo mi otra mano sobre la fogata que acabo de crear. 

Siento como si mi sangre chispeara dentro de mí, pero sé que no se trata de mi sangre.
Es la magia. 
El poder.
— ¡Nada de magia en el carruaje, brujas! —la voz gruñona del señor me asusta tanto que la flama en mi mano se apaga antes de que acabe la frase.
— Perdone usted, señor —la cantarina voz de mi madre se apronta a decir—. No volverá a pasar. 
Observo a mi madre, su flacucho cuerpo cubierto con las telas viejas de un vestido, ella es idéntica a mi abuela a excepción de que su rostro posee unos rasgos más prominentes.
Los brazos de mi abuela me cubren y me sientan en su regazo, ella es tan cálida, quisiera aprender a estarlo, con tanto frío no sé cómo logra extraer calor.
— Estas tan fría —murmura en mi oído—, cuando lleguemos a nuestro nuevo hogar te enseñare a adsorber el calor —más lagrimas se acumulan en mis ojos. 
— No quiero una nueva casa, me gusta la anterior, no quiero ir al territorio de los lobos —sollozo tratando de hacer el menor sonido, no quiero que el hombre gruñón nos regañe.

— ¿Tienes alguna idea de cómo logro mantenerme tibia? —sorbo por la nariz y niego.
Ni mi madre ni ella me dan una explicación concreta de porque tenemos que irnos a vivir en ese territorio, tan tenebroso, donde las personas son tan odiosas.
— Por mi collar de cuna —eso logra llamar mi atención—. Fuego. Esa es la palabra escrita en él, es a lo que estoy atada —sus ojos del color de la miel me observan con cuidado—, ¿guardaras mi secreto?
Fuego.
Ese es su más arriesgado secreto.
Y ella me lo ha dado para cuidar.
— Sí, abuela.
— El fuego es parte de mí, logro crearlo aunque no pueda extraer la fuerza de algún lugar... —sonríe abiertamente, el carruaje se mueve de manera brusca pero su sonrisa no se borra—. Cuando tengas edad suficiente, podrás leer la palabra en tu collar de cuna.
Abrazo a mi abuela con fuerza, adoro la capacidad que tiene de distraerme en los momentos más difíciles.
El carruaje se sigue moviendo, cada vez más deprisa, puedo escuchar las patas de los caballos chocar contra el suelo y sus resoplidos. El calor que desprende mi abuela me envuelve y acuna, mis parpados se vuelven pesados hasta que los cierro por completo.
— Estamos en su territorio —la voz de mi madre acude a mis oídos—, ¿Estás seguro que nos recibirá?
— Estoy en deuda contigo, María —el señor gruñón dice el nombre de mi madre con cierta molestia—, no me gusta deberle a brujas')
--12
INSERT INTO prologo(cabecera, contenido) 
VALUES ('PROLOGO','- ¡Jessica, ven por favor! - el llamado de mi madre interrumpió por completo mis juegos de mesa con mi Nana. Arreglé mi vestido y me despedí de Nana con una reverencia.
- ¿Sucede algo madre? - pregunté un vez que estuve a su lado.
- Papi quiere presentarte a alguien. Ven. - me ofreció su mano y gustosamente la acepté con una sonrisa.
Caminábamos por los largos pasillos de la mansión. Mi madre cantaba un canción de cuna mientras yo observaba todo a mi alrededor.
Nos detuvimos en una imponente puerta de roble barnizado con delicados detalles tallados en ella. La puerta se abrió dejando ver a mi padre junto a un hombre alto y corpulento vestido totalmente de negro a excepción de su camisa blanca y sus guantes del mismo color.
Era él, ese hombre que me daba miedo.
Su mirada de hielo chocó con la mía y me encogí en mi lugar. Apreté el agarre de la mano de mi madre y me escondí detrás de ella.
- Jessica, ¿Todo va bien? - la voz de mi padre hizo que levantara mi cabeza y lo observara.
Asenti sin rechistar y me separe un poco de mi madre, pero sin quitar mi mano de la suya.
- Jess, él es...
«Jeon JungKook»
Me dije a mí misma pues ya sabía de él. Noches atrás había oído la conversación de mis padres junto a ese hombre.
- Jeon JungKook y desde ahora en adelante nuestro mayordomo.
¿Mayordomo? No entendía por qué un hombre de ese porte sería nuestro mayordomo. A leguas se notaba que ese tal JungKook pertenecía a la nobleza, ¿Por qué desperdiciar sus lujos para servir a nuestro Clan?
- Un placer conocerte, Jessy. - el hombre de cabellos negros se acercó a mí y se agachó para quedar a mi altura.
Su mirada seguía siendo fría y vacía, pero con una chispa de fuego en ella. Estiró su mano para tomar la mía y me hice hacía atrás nuevamente.
- Oh, lo siento mucho. - se disculpó mi madre. - Nuestra hija es muy tímida con las personas que recién conoce. Pero pronto eso cambiará, ¿O no Jessica? - mi madre también se agachó a mi altura e hizo descubrir mi rostro que yacía escondido en su vestido.
Asentí con los nervios a flor de piel y una vez más hice contacto visual con los ojos de hielo de JungKook.
Una corriente fría invadió todo mi cuerpo. Él no me inspiraba confianza, él escondía algo.
- Bien JungKook, empezarás con los labores mañana por la mañana. ¿Tienes algo que decir? 
La voz de mi padre hizo que JungKook se levantara lentamente y acomodara mejor sus finas ropas. Lentamente se dió la vuelta y le sonrió a mi padre.
- No, mi señor.')
--13
INSERT INTO prologo(cabecera, contenido) 
VALUES ('PROLOGO','Me arreglé la camisa y la corbata, Ben, el amigo de Nick me dió una paliza horrible, ambos son unos chicos muy guapos pero intimidan a los que no tienen amigos, como yo.
A pesar de que ambos son unos matones terribles, odiados por la mayoría, yo caí enamorado de Nick, no solo por ser alto, moreno, con un cuerpazo, si no que ví más allá de lo que quiere mostrar, siento que hay mucho dolor detrás de su mirada. Siempre está rodeado de gente, aunque la mayoría es cínica, y mujeres guapísimas que mojan las bragas por él, por lo que jamás estaré a su nivel.
Tengo 17 años, y soy gay, otro motivo por el que Ben me pega, ambos, incluyendo a Nick, son homofóbicos, pero a pesar de eso Nick no gasta su tiempo en golpearme como Ben, él golpea más a los que tienen buenas notas.
Quizá ahí está su frustración.... Y la de Ben sería que... ¿es homosexual? Jaja ni yo lo creo.
Camino un poco más y me topo con Nick, está saliendo del gimnasio, seguramente después de practicar baloncesto. Me escondo para verlo sin que él me note, pero al ver a una rubia tonta acercarse a él me dan ganas de salir de mi escondite y golpear a la muy tonta por coquetear con mi Nick.
-Nick, hola- dice Elen, va a mi clase de mates.
-Hey.... Elena...- se rasca la nuca.
-Elen.- le corrige con una sonrisa muy hipócrita.- quiero salir contigo uno de estos días... ¿Me das tu numero?
-Obvio, te lo dicto- saco mi celular igual de ansioso que Elen.- 863....
Anoto en mi celular y guardo el contacto como "AP". Bloqueo la pantalla de mi cel y espero que ambos se vallan antes de salir de mi escondite e ir a casa.
Una vez ahí me rompo la cabeza pensando en que hacer, ¿le hablo? Claro que si Daniel, no seas estúpido. ¿Y si se molesta? Después de todo soy un hombre, no una mujer, él es hétero y homofóbico...
Siento como la ampolleta se prende, y no hablo de la de mi habitación. fingiré ser mujer, agarro mi celular y pongo de foto de perfil a mi hermana mayor, Anne. Es una rubia de ojos azules, un año mayor que yo, pero somos como gemelos, casi idénticos. Luego me arrepiento ¿y si se enamora de mi hermana?!, dejo mi foto en blanco.
-Hola, soy Daniel- escribí en mi movil, pero luego recapacité y lo puse en femenino.
-Hola, soy Danielle ;)')
--14
INSERT INTO prologo(cabecera, contenido) 
VALUES ('PROLOGO','Agarro mi mochila y salgo rápido de casa tratando de no tropezar en el camino hacia mi Jeep, abro la puerta apurado y me meto dando un portazo, meto la llave y la giro esperando el ronroneo, pero en vez de eso, recibo un maldito resoplido por parte del cacharro.
—¡Oh, vamos! funciona, maldición, voy tarde —Le doy un golpe al volante. Desde que compre éste Jeep, a veces para hacerlo encender se convierte en una lucha. —Vamos enciende —Giro nuevamente la llave, y al tercer intento funciona —¡Eso! —Como dice el dicho, el tercer intento es el vencido.')
--15
INSERT INTO prologo(cabecera, contenido) 
VALUES ('PROLOGO','La noche era oscura, tan oscura que su silueta desaparecía entre las sombras amparada por el sepulcral silencio que se perdía entre los callejones que rodeaban al sitio donde iba.
Una fina neblina acompañada su osada travesía que emprendió desde que puso un pie fuera del carruaje, su única forma de volver a la seguridad de su hogar lejos de aquellos barrios de mala muerte.
Se cubrió más con el abrigo mientras contemplaba el cartel raído y lleno de agujeros que colgaba de uno de los clavos de un viejo establecimiento.  

"Bar El Descenso" leyó con dificultad.
Había llegado. 

Entró y se acopló a los cientos de hombres y mujeres que bebían diversos licores o como ella preferían apostar.
Las apuestas en el Descenso, eran de las mejores que habían, así como los juegos de mesa que para variar captaban la atención de mujeres y hombres de diversa índole, aunque la presencia de nobles estaba en auge.
En aquel lugar no había títulos que importen, solo el dinero te distinguía de los demás y claro qué tan generoso eras con tus apuestas. Vanesa (la amante de su hermano) la había traído por primera vez al Descenso, más para demostrarle porqué ella nunca encajaría en aquel bar que para motivarla a quedarse allí. Lo que la cortesana nunca imaginó es que a la dama le terminaría gustando.
No era un lugar feo, por el contrario tenía bastante clase para estar tan alejado del centro de la ciudad. Además no estaba sola, aunque su acompañante no fuera del todo de su agrado.
Alicia soltó un largo suspiro antes de sentarse en una mesa con dos caballeros como acompañantes, dando inicio a la ronda de apuestas.

*** 

—Gana la dama de rojo.—anunció el juez y 20 libras fueron depositadas en su modesto saco de billetes.
"Modesto" ¿enserio? nada de lo que ella tenía era modesto.
El caballero en cuestión al verse derrotado se marchó despotricando en su contra.
Los hombres si que eran idiotas por subestimarla y después no conformarse con los resultados.
—Me retiró.—dijo haciendo un ademán para levantarse.
La noche había sido buena y las 200 libras que ganó, bastaban por ahora.
Estuvo a punto de ceder su puesto a alguien más cuando un hombre se sentó en frente suyo, dispuesto a retarla.
Otro tonto, pensó.
—No me negara está partida ¿verdad milady?.—el delicado acento inglés y recto proclamaba que su contrincante era un caballero de cuna, pero si eso no fuera suficiente toda su indumentaria también se lo clarificó a gritos.
Estaba vestido con telas muy finas y su traje era de una elegante confección, de color negro a medida que se ceñía a su escultural cuerpo.
Cuando alzó su mirada y contempló su rostro, estuvo tentada a esbozar la típica o para plasmar todo su asombro. Se abstuvo gracias a los años de experiencia.
Frente a ella como la viva imagen de la galantería y el cinismo, se encontraba Adrien Miller, marqués de Grafton. Nunca habían sido presentados y por obvias razones él no la conocía, sin embargo aún así se vio en la necesidad de cubrir más su identidad con el antifaz que utilizaba.
—Lo siento milord, estaba por retirarme.—intentó excusarse sabiamente. Casi un año asistiendo a garitos de juego le había enseñado a fijarse muy bien en las reputaciones de sus contrincantes. Nunca se iba a ciegas aunque tuviera la victoria asegurada y en este caso había un 50% de probabilidad que ella perdiera.

Jeremy , futuro marqués de Thornhill, lleva desde que tiene uso de razón total y irrevocablemente enamorado de Victoria Adams, la mayor del clan Adams y también la más testaruda, ambiciosa y puntillosa.
Victoria tiene como único objetivo en la vida no cometer ningún error y hacer siempre lo que las normas dictan pues tiene la absurda teoría de que nada malo le sucederá si así lo hace .
El problema es que los accidentes ocurren y sin darse cuenta acaba metida de lleno en un escándalo .
Uno que provoca que acabe comprometida con su mejor amigo , un hombre que ella solo ve como un hermano y nada más .
¿Podrá Jeremy cambiar la visión que tiene Victoria de él o estará condenado a vivir siempre siendo su mejor amigo y ... nada más ?

¿Por qué sabía eso? 
Muy simple su retador no solo era apuesto y asquerosamente rico sino que tenía fama de ser un hábil apostador y un conocedor exhuberante de diversos tipos de juegos. Tenía una desventaja de experiencia y de dinero.—su pequeña ganancia nunca se compararía con el monto de dinero que él podría llegar a apostar.—y por si fuera poco nunca retaría a su principal interés de conquista.
Era un mal momento y Alicia lo sabía, en sus planes figuraba que la primera vez que se conocieran sería en un lugar formal, con un baile y una sosa presentación. Incluso había repasado varias veces lo que le diría y como actuaría frente a él, pero si seguía allí todos sus planes se irían por la borda.
—Es muy grosero de su parte milady.—compuso acomodándose mejor en el asiento y ella a cambio le dió una sonrisa de boca cerrada. Tomó sus cosas y se levantó para marcharse.
Toda su actuación de señorita dulce y sumisa que era acordé con las preferencias del caballero se iría al traste si él descubría su identidad. No era un riesgo que estaba dispuesta a sobrellevar.
No, señor.
Con elegancia la dama se perdió entre la multitud de gente, velada en todo momento por los ojos del caballero.')

---------------------------------------------------------
---------------------------------------------------------
--HISTORIAS----------------------------------------------

INSERT INTO historia(titulo, portada_url, id_sinopsis, id_prologo, id_categoria)
VALUES('Él es Alexander Blackstone.', 'https://a.wattpad.com/cover/114145992-352-k32105.jpg', 1, 1, 1)
INSERT INTO historia(titulo, portada_url, id_sinopsis, id_prologo, id_categoria)
VALUES('Solo un juego', 'https://a.wattpad.com/cover/47669597-176-k174889.jpg', 2,2, 2)
INSERT INTO historia(titulo, portada_url, id_sinopsis, id_prologo, id_categoria)
VALUES('Spider-Man: ¡Academia de Héroes!', 'https://a.wattpad.com/cover/180758584-176-k765157.jpg', 3, 3, 13)
INSERT INTO historia(titulo, portada_url, id_sinopsis, id_prologo, id_categoria)
VALUES('Llévame contigo', 'https://a.wattpad.com/cover/11007364-176-k771427.jpg', 4, 4, 1)
INSERT INTO historia(titulo, portada_url, id_sinopsis, id_prologo, id_categoria)
VALUES('Perdón ', 'https://a.wattpad.com/cover/185737168-176-k221639.jpg', 5, 5,1)
INSERT INTO historia(titulo, portada_url, id_sinopsis, id_prologo, id_categoria)
VALUES('With You','https://a.wattpad.com/cover/91866652-176-k793196.jpg', 6, 6, 1)
INSERT INTO historia(titulo, portada_url, id_sinopsis, id_prologo, id_categoria)
VALUES('Danzando en el Infierno','https://a.wattpad.com/cover/133646214-176-k875603.jpg',7,7,1)
INSERT INTO historia(titulo, portada_url, id_sinopsis, id_prologo, id_categoria)
VALUES('Amor Contratado','https://a.wattpad.com/cover/126978639-176-k319359.jpg',8,8,1)
INSERT INTO historia(titulo, portada_url, id_sinopsis, id_prologo, id_categoria)
VALUES('Fairytale','https://a.wattpad.com/cover/143658604-176-k925923.jpg',9,9,1)
INSERT INTO historia(titulo, portada_url, id_sinopsis, id_prologo, id_categoria)
VALUES('Señora Delacroix','https://a.wattpad.com/cover/137219076-176-k448542.jpg',10,10,1)
INSERT INTO historia(titulo, portada_url, id_sinopsis, id_prologo, id_categoria)
VALUES('La bruja y los lobos','https://a.wattpad.com/cover/89424896-176-k631555.jpg',11,11,1)
INSERT INTO historia(titulo, portada_url, id_sinopsis, id_prologo, id_categoria)
VALUES('Contrato Infernal','https://a.wattpad.com/cover/174007100-176-k686378.jpg',12,12,1)
INSERT INTO historia(titulo, portada_url, id_sinopsis, id_prologo, id_categoria)
VALUES('Doble Persona','https://a.wattpad.com/cover/85478490-176-k437138.jpg',13,13,1)
INSERT INTO historia(titulo, portada_url, id_sinopsis, id_prologo, id_categoria)
VALUES('Aquello Que Siempre Quise','https://a.wattpad.com/cover/87592572-176-k770132.jpg',14,14,1)
INSERT INTO historia(titulo, portada_url, id_sinopsis, id_prologo, id_categoria)
VALUES('Apostando Por Tu Amor','https://a.wattpad.com/cover/142604430-176-k165917.jpg',15,15,1)

---------------------------------------------------------
-- TRIGGER 1
-- VALIDA QUE NO EXISTAN CONVERSACIONES DE UNA PERSONA CONSIGO MISMA
CREATE TRIGGER VALIDA_CONVERSACION
ON conversacion
FOR INSERT
AS
BEGIN
	DECLARE @VAL1 INT
	DECLARE @VAL2 INT

	SET @VAL1 = (SELECT conversacion.id_user1 FROM conversacion
				JOIN inserted
				ON inserted.id_conversacion = conversacion.id_conversacion);

	SET @VAL2 = (SELECT conversacion.id_user2 FROM conversacion
				JOIN inserted
				ON inserted.id_conversacion = conversacion.id_conversacion);
	IF (@VAL1 = @VAL2)
	BEGIN
		RAISERROR('UN USUARIO NO PUEDE MANDARSE MENSAJE A SI MISMO', 16,1);
		ROLLBACK TRANSACTION
	END
END

-- PROC 2
-- PERMITE CREAR UN MENSAJE NUEVO INCRUSTANDO LA FECHA Y HORA ACTUAL
-- ADEMAS PERMITE CREAR UNA CONVERSACION SI ESTA NO EXISTE, EN CASO DE QUE EXISTA
-- ENTONCES USARÁ AQUELLA EXISTENTE COMO MEDIO DE COMUNICACION
CREATE PROC insertar_mensaje
	@texto TEXT,
    --@fecha DATETIME, --1900-01-01 23:59:59
    --@estado INT, -- 0 = no intregado, 1 = entregado
    @id_user_creador INT,
	@id_user_receptor INT
AS
	DECLARE @CONVERSACION INT
	SET @CONVERSACION = (SELECT COUNT(*) FROM conversacion WHERE id_user1 = @id_user_creador AND id_user2 = @id_user_receptor
							OR id_user1 = @id_user_receptor AND id_user2 = @id_user_creador);
	DECLARE @id_conversacion INT
	SET @id_conversacion = (SELECT id_conversacion FROM conversacion WHERE id_user1 = @id_user_creador AND id_user2 = @id_user_receptor
							OR id_user1 = @id_user_receptor AND id_user2 = @id_user_creador);

	IF(@CONVERSACION > 0)
	BEGIN
		INSERT INTO mensaje(texto, fecha, estado, id_user_creador, id_conversacion) VALUES(@texto, GETDATE(), 0, @id_user_creador, @id_conversacion);
	END
	ELSE
	BEGIN
		INSERT INTO conversacion VALUES(@id_user_creador, @id_user_receptor);
		SET @id_conversacion = (SELECT id_conversacion FROM conversacion WHERE id_user1 = @id_user_creador AND id_user2 = @id_user_receptor);
		INSERT INTO mensaje(texto, fecha, estado, id_user_creador, id_conversacion) VALUES(@texto, GETDATE(), 0, @id_user_creador, @id_conversacion);
	END
GO

EXEC insertar_mensaje 'Hola', 1, 2
EXEC insertar_mensaje 'Hola, ¿Qué tal?', 2, 1
EXEC insertar_mensaje 'Bien gracias ¿Cómo te ayudo?', 1, 2
EXEC insertar_mensaje 'Solo quería saludar', 2, 1
EXEC insertar_mensaje 'Vale fue un gusto', 1, 2

EXEC insertar_mensaje '¿Cómo estás?', 3, 2
EXEC insertar_mensaje 'Ahora no puedo platicar, disculpa', 4, 2
EXEC insertar_mensaje 'Claro, descuida', 3, 2
EXEC insertar_mensaje 'Gracias', 4, 2
EXEC insertar_mensaje 'Ten un buen día', 3, 2

EXEC insertar_mensaje '¿Que hay de nuevo? Isac', 5, 3
EXEC insertar_mensaje 'Bien amigo gracias ¿y tú?', 6, 3
EXEC insertar_mensaje 'Aquí leyendo un poco', 5, 3
EXEC insertar_mensaje '¿Que lees ahora?', 6, 3
EXEC insertar_mensaje 'Algo de Sci-Fi', 5, 3

EXEC insertar_mensaje 'Seidy ¿como se llamaba tú escritor favorito', 7, 4
EXEC insertar_mensaje 'Lorena Pech', 8, 4
EXEC insertar_mensaje 'Vale muchas gracias', 7, 4
EXEC insertar_mensaje 'Por nada Joaquin', 8, 4
EXEC insertar_mensaje 'Cuidate', 7, 4


INSERT INTO capitulo(titulo, contenido, id_historia)
VALUES('Capítulo 1. "Feliz cumpleaños Sophie.', 
'Sophie.  
Retuerzo mis dedos entre sí, ansiosa y con el corazón latiéndome desbocado dentro de mi caja torácica. Estoy a punto de sufrir un colapso nervioso, puedo sentirlo. 
Mi jefe, Alexander Blackstone, me mira furioso, conozco esa mirada y sé que estoy en graves problemas. Su primo se ha metido a la fuerza en su despacho y eso él lo odia, pero no he podido impedírselo.
—¿Para carajos crees que te contrate? —espeta, colgando su saco en el perchero cromado.
Aprieto mis labios, impidiendo que algo malo pueda salir de ellos y me meta en problemas por eso. Necesito el trabajo y debo de mantener la boca cerrada. Sólo debo de pensar en cuanto dinero marca mi cuenta bancaria al final de la semana.
—Lo lamento, señor Blackstone, pero su primo...
Con un simple ademan de su mano hace que me calle. Reprimo el impulso de poner los ojos en blanco, eso me puede costar mi trabajo. Y lo necesito.
Aguanta un poco más, Sophie.
—Sí, lo que digas —gruñe entre dientes, caminando hasta mí, me niego a dar un paso atrás, sin parecer intimidada—. Vete y regresa con un café y las cuentas del mes, ahora.
Me giro sobre mis tacones con suma rapidez y camino sobre el piso encerado hasta las enormes puertas de cristal oscuro y las cierro detrás de mi espalda.
Suspiro, irritada.
Es insufrible, ni siquiera sé cómo es que no me ha echado en uno de sus malos ratos. Que francamente son muy seguidos, a penas y logro mantener mi boca cerrada cuando prácticamente me insulta con "clase".
Hago su café como de costumbre, el agua casi hirviendo, dos sobres medidos de café intenso, sin azúcar, ni ningún tipo de endulzante, menos lácteo ni ninguno de sus derivados.
Llego a mi escritorio sin derramar ni una sola gota y tomo las carpetas negras con el logo de la empresa y camino hasta las puertas dobles, toco dos veces, como siempre y luego entro, empujando la puerta con el hueso de mi cadera.
El señor Blackstone me mira con una ceja levantada, su expresión es impasible, aparta la mirada gélida de mí.
—Que sea la última vez que permites que suceda algo como lo de hoy, Sophie —dice fríamente.
Asiento, aunque no me está viendo. Dejo la taza de café sobre su escritorio, donde pueda tomarlo con facilidad y después acomodo la pila de cinco carpetas y por último coloco la de las cuentas del mes.

esta es una historia sencilla, en donde ambos se necesitaban pero aun no lo sabían, y gracias a las vueltas de la vida y  después de algunos  altos y bajos...el amor florece..convirtiéndose ambos en uno solo...
el es Bruno Bianchi...un hombre normal, que nunca imagino volver a amar en esta vida....
ella es Vanesa Fredes....una mujer, que nunca ha buscado el amor, tan inocente, que no se da cuenta que el amor llega y sin pedir permiso...
Portada por ladysempaiii.....gran amiga....

—Retírate. 
Asiento de nuevo y camino a la salida, pero entonces su chasquido de dedos me hace detenerme, aprieto los labios y pongo los ojos en blanco, odio que me chasqueo los dedos como si fuera un animal.
—¿Qué estabas haciendo cuando llego, Albert?
Me giro y lo miro con parsimonia y digo.
—Estaba corrigiendo el contrato que me envió esta mañana, me hizo anotaciones sobre mis errores —sus ojos azules increíblemente fríos me atraviesan y casi hace que me tiemblen las piernas.
—¿Lo terminaste?
—Sí, señor Blackstone.
—Tráelo ahora.
Me giro de nuevo y camino hasta la puerta, haciendo muecas.
—Sophie, pídele a Prescott que suba.
—Por supuesto.
Salgo a toda prisa de su oficina y tomo el auricular y llamo al número privado del jefe de seguridad del señor "me vale mierda todo el mundo".
Prescott aparece a los pocos minutos con su rostro impasible, que sólo cambia cuando una pequeña sonrisa se desliza en dirección a mí, le correspondo del mismo modo y sin más entra en la oficina, tomo lo que necesito y entro minutos después, luego de tocar, para evitar enterarme de algo que no me concierne.
Mis pasos se detienen cuando el señor Blackstone cierra la puerta en mi rostro. Parpadeo sin saber qué demonios acaba de ocurrir, lo más seguro es que el señor Blackstone tenga alguno de sus comunes y desagradables ataques de ira.
Siempre logra asustarme de verdad cuando eso pasa, es escalofriante y terrorífico. Pobre la mujer que tenga que vivir con él y soportar sus cambios drásticos de humor y lo violento que es.
Me estremezco y vuelvo a mi asiento, no tengo nada de trabajo pendiente que hacer, no ha mandado a mi correo ni ha enviado a una nota, ni un texto, ni menos ha llamado. Tomo de mi bolso y saco mi libreta, comienzo a hacer la tarea de la universidad, como siempre en mis ratos libres, que no suelen ser demasiados.
Leo un par de veces más, tratando de comprender lo que dice las palabras en alemán, entiendo muy poco lo que dice el texto. Leo y busco sus significados, escribiéndolas debajo de cada palabra y ahora todo comienza a tornar un poco de sentido.
Me sobre salto cuando aparece la señora Blackstone, la madre de mi jefe. Luce impecable y sumamente hermosa con un vestido azul cielo. Sus de color azul brillan con sinceridad y carisma mientras me observa, es una mujer muy dulce y amable, todo lo contrario, a su hijo.
—Buenos días, señora Blackstone —saludo cordialmente poniéndome de pie.
Ella sonríe dulcemente.
—Buen día, Sophie —deja una pequeña caja roja sobre mi escritorio—feliz cumpleaños.
Sonrío sin poder evitarlo, ni siquiera recordaba que hoy es mi cumpleaños.
—Hoy es veinticinco de octubre —dice acercándose a abrazarme, le devuelvo el abrazo con suavidad y una calidez se desata en mi interior.
—Gracias, señora Blackstone —terminamos el cálido abrazo—. No tenía que hacer esto.
Susurro refiriéndome al regalo. Ella hace una ademan con la mano, restándole importancia.
—No digas tonterías, ha sido un placer. Además, no todos los días se cumplen veinte años.
Sonrío de nuevo, cándida.
—Y ya te dije que me digas Coral —se acerca de nuevo y esta vez tiene una pequeña caja azul en sus manos, adornada con un lazo plateado. 

Lo miro con extrañeza, mientras ella la deja en mis manos, abro los ojos sorprendida, no puedo aceptarlo. Es de Tiffany. La joyería de la que en mi vida podré comprar algo.
—No, no se te ocurra rechazarlo. —Refuta, seriamente, al notar mi estupefacción—. Es un regalo de Alexander.
El corazón definitivamente deja de latirme.
¡No lo puedo creer, el maniaco me ha regalado algo! ¡Y, por cierto, carísimo!
¡Oh, Dios!
No puedo aceptarlo, además él de seguro ni siquiera lo ha elegido, soy yo la que siempre busca los regalos para la señora Coral, no él.
Sin mencionar, que esto es carísimo, no puedo aceptarlo. Si fuera una pluma, un libro, lo aceptaría sin problema, pero es una joya. Una joya de verdad.
—Sophie —dice en tono cansino y convencedor—. Lo ha elegido él, tengo la impresión de que te quiere deslumbrar.
Abro la boca, repentinamente seca y sintiéndome acalorada.
¿Deslumbrarme? A mí, debe de ser una broma de muy mal gusto.
—Creo que va a gustarte.
Silencio.
Tomo un par de respiraciones para poder soportar todo lo que sigue, es decir, esto es de locos. El señor Blackstone me ha comprado un obsequio y no sé cómo sentirme, bueno, sé que me siento increíblemente incomoda con todo esto.
Alexander Blackstone nunca regala nada y yo quiero deberle nada.
—Gracias, pero es demasiado. No puedo aceptarlo —susurro con timidez.
Ella inclina el rostro hacia un lado y me observa con atención.
—Eres una buena chica —dice, trémulamente—. Pero eso tendrás que decírselo tú misma.
El aire se me escapa de los pulmones y lo miro a un lado mío, me sobresalto y pongo un poco de distancia entre ambos.
—Bueno, yo tengo que irme —besa la mejilla de su hijo y luego la mía—. Hoy deja que vaya temprano a casa, hijo.
Ni siquiera espera algún comentario por nuestra parte porque sale de inmediato rumbo a los ascensores, Prescott tampoco está y me siento nerviosa.
Una atmósfera extraña carga el ambiente y no sé dónde meterme para escapar.
Y entonces como un revuelo en el ambiente siento el brazo del señor Blackstone rodeando mi cintura, me paralizo. Me quedo sin aire en los pulmones y mi pulso se acelera a niveles exorbitantes.
Su tacto me pone nerviosa, me remuevo con incomodidad, alejándome de su brazo. No entiendo que le sucede.
Tengo un año trabajando con él y en la vida me ha dirigido una palabra que no sea de trabajo y menos me ha tocado, a menos que sea por un descuido al arrebatarme algo con brusquedad.
Mi trasero choca contra el vidrio de mi mesa de trabajo. Me quedo estática y miro a Alexander, sus ojos brillan peligrosamente, una mirada casi feroz.
Mi cuerpo se estremece con una sensación desconocida.
—¿No aceptaras mi regalo? —inquiere solemne, metiendo sus manos en los bolsillos de su pantalón negro.
Su semblante tiene cierta oscuridad en su rostro, el mentón apretado con fuerza y los ojos con brillo peligrosamente febril, su rostro enmarca facciones duras, para ser tan apuesto, es muy ermitaño.

Niego tácitamente, aun siendo presa de una tormenta desconcertante.
—No puedo aceptarlo, señor Blackstone —balbuceo, mirando su apuesto rostro—. Muchas gracias, pero no puedo aceptarlo es demasiado.
Levanta una ceja y aguza la mirada, estudiándome con ella.
—Claro que puedes aceptarlo y lo harás, Sophie —estira su mano y aparta un mechón suelto de mi cabello y lo deja delicadamente detrás de mi oreja, estoy inmóvil, contengo la respiración y el pulso se me acelera aún más.
Tengo la boca seca y mis fosas nasales captan su aroma sumamente masculino y atrapante. Huele a menta con una mezcla de cuero, y por supuesto, a whisky escoses.
Me obligo a mantener la cabeza levantada, para poder observarlo, no logro comprender que es lo que le sucede. Él no es amable con nadie. Siempre es arisco, hosco y sumamente frío en su trato.
Su cercanía me descoloca, ahora su rostro muestra cierta suspicacia, entrecierro los ojos, buscando alguna respuesta en los suyos, mas no la encuentro, Alexander Blackstone es un misterio, todo un paradigma.
Abro la boca, decida a decir algo. Cualquier cosa, pero que me ayude a salir de este momento tan perturbable para mí. 
—Sophie, ven a mi oficina —pide, luego de unos segundos en lo que sólo se dedica a mirarme.
Asiento con evidente incomodidad. 
Sigo sus pasos en total silencio, preguntándome que carajos sucede con mi jefe. Todo esto es tan extraño y poco razonable viniendo de él, no sé qué puedo esperar. 
Parpadeo un par de veces, mientras me detengo a un lado de las grandes puertas, mirando los edificios que son tan altos como el mismo Blackstonefire. 
El señor Alexander Blackstone está de espaldas a mí, sirviendo algún tipo de licor, puedo escucharlo. 
—Nos iremos a Europa —comienza con voz profunda—. Tengo varios negocios que necesito concluir antes de la primera quincena de noviembre. —Se gira y me mira—. Por supuesto, tú irás conmigo, viajaremos a Londres, Italia, Alemania y Dinamarca.
Asiento en comprensión a todo lo que me dice, no es primer viaje de negocios al que voy con él. Sé cómo funciona todo este asunto, estar auxiliándolo, justo como lo hago aquí, en New York.
Bebe de su copa y luego dice: 
—Nos iremos mañana mismo —su mirada es vacía—. Puedes irte a casa, Sophie. 
Oh. 
—Gracias, señor Blackstone —digo tímidamente. 
Deja la copa vacía sobre su escritorio y me mira sin decir una palabra, cuando estoy a punto de voltearme para salir, su voz profunda me detiene en el acto. 
—Lleva lo que te regale a casa —aprieto los labios—. Feliz cumpleaños, Sophie.
', 1)
INSERT INTO capitulo(titulo, contenido, id_historia)
VALUES('Capítulo 2. "Trampa peligrosa.', 
'Sophie.  

Jesse observa con expresión catatónica la delicada gargantilla que el señor Blackstone me ha regalado, es hermosa y muy costosa. Sólo de imaginarlo se me revuelven las entrañas. Estoy hecha una maraña enorme de sensaciones, todo es realmente desconcertante.
¿Es normal que tu jefe malhumorado te obsequie una joya el día de tu cumpleaños?
Agotada de pensar lo mismo, guardo mis pensamientos en lo más profundo de mi mente. Desvío la mirada hasta la delicada joya. 
Es un zafiro en forma de gota, impresionante, pendiendo de una delicada cadena de oro blanco. El diamante es de tamaño medio, en este momento odio no saber sobre los quilates.
—Ese hombre quiere posiblemente dos cosas contigo, reina —comienza, rápidamente—. O planea follarte por toda Europa o quiere algo verdadero contigo.
Suelto una carcajada y niego.
—Estás mal, Jesse. El señor Blackstone y yo jamás tendremos algo.
Pone los ojos en blanco y chasquea la lengua, bebe de su cerveza antes de decir:
—A veces eres tan tonta, Sophie —hago una mueca—. Ese hombre quiere algo, ¿segura que no tienes nada con él?
Es mi turno de poner los ojos en blanco.
—No —gruño.
Doy un mordisco a mi pizza y gimo al sentir el queso derretido en mis papilas gustativas.
Jesse ha sido mi mejor amigo desde el colegio, ahora vivo con él, es como mi hermano mayor, mi confidente, mi todo. La estancia con mis padres no duró demasiado, la relación con mi padre siempre fue dura y siempre intentaba tocarme, eso me mantuvo asustada, por esa misma razón salí de casa. Beth, mi hermana mayor, está casada y tiene una hermosa bebé de ocho meses, Amber, y por si fuera poco se hace cargo de mi pequeño sobrino, Aiden de cinco años, porque su padre, Ian, mi hermano, es incapaz de cuidarse por sí solo, todo el tiempo está en problemas, cada vez es más frecuente las veces que va a prisión por hacer cosas ilegales. Nuestra familia no es la mejor, mi padre es casi un alcohólico consuetudinario, le encanta apostar lo que no tenemos, fue así, como tuve que dejar de estudiar y trabajar para ayudar con todos los gatos y con la salud de mi madre, a pesar de que Ben, el esposo de Beth es médico, él no es cardiólogo y no siempre puede ayudarnos.
Además, no tiene obligación de hacerlo, bastante hace con ayudarnos a cuidar a Aiden y contribuyendo con la mitad de la hipoteca de la casa de mis padres. 
—¡Sophie! —grita atrayendo mi atención.
Jesse me está mirando con irritación.
—¿Qué?
—Te he estado hablando como idiota, ¿en qué piensas?
Encojo mis hombros, restándole importancia. Me he perdido entre tantos pensamientos que me abruman.
—Nada en especial.
—¿Alexander Blackstone? —dije sugerentemente. 
Aprieto los labios y arrugo la nariz. 
—Él no tiene nada que ver con lo que pasa por mi mente —aseguro, irritada.
¿Por qué tendría que pensar en mi molesto jefe? 


Me remuevo incomoda sobre el asiento de piel negra y reconfortante. Alexander Blackstone lee su iPad sin prestarme atención en lo absoluto. Miro mis uñas color rosa pálido, estamos cerca del aeropuerto y sé que debemos descender por las pistas privadas. Nunca he ido a Dinamarca. Tampoco a Italia. Así que sólo tal vez, pudiera ver un poco de ellos. Claro, si es que el señor Blackstone me dejaba tiempo libre.
Por mi vista periférica soy capaz de darme cuenta qué, el señor Blackstone me observa, quizás lo he molestado al estar moviéndome, aunque no estamos en el mismo asiento, no me atrevo a voltear en su dirección, sigo sentir su mirada intensa, sobre mí. Me incomoda, mas no puedo hacer nada al respecto.
—Sophie —mi mirada se topa con la suya de inmediato—. Tenemos que hablar sobre algo importante.
—Por supuesto —respondo de inmediato, notando un leve temblor en mi cuerpo.
Su semblante es impasible, como siempre. Estoy acostumbrada a él, de cierta manera es con la persona que más tiempo paso desde hace un poco más de año. Prácticamente estoy impuesta a sentirlo cerca, claro no tan cerca.
Prescott ni Jules, —otro agente de seguridad—, están en la misma cabina que nosotros y no sé si eso es bueno o malo. Sólo sé que hay una extraña sensación en mi estómago desde esta mañana que vi a Alexander dentro del auto para irnos al aeropuerto. En mi pecho hay una opresión que logra abrumarme y ponerme en alerta. Las palabras que mi jefe a dicho sólo consiguen ponerme más nerviosa de lo que estoy.
Faltan unos minutos para que aterricemos en Londres, puedo ver las luces de la ciudad, me recuerda a las Vegas y New York, ciudades que no conocen la noche.
Cuando Mirta, la sobrecargo, nos pide que abrochemos nuestros cinturones y ha retirado cualquier bebida o alimento de las mesas, sé que estamos a punto de aterrizar y entonces siento el vacío en mi estómago al momento que el Jet desciende, siempre sucede lo mismo.
El aire frío golpea mi rostro, alborotando mis cabellos, hace frío y puedo sentir como traspasa mi jean, el blazer me arropa el pecho, está lloviendo con fuerza y el viento helado sólo hace que desee estar en casa, acostada con Jesse y arropada mientras vemos una película.
Lo que me deja atónita es que Alexander me sujeta del brazo, acercándome a él, tal vez estoy temblando, no lo sé, simplemente me siento patética en este momento, me tenso al sentir ese mismo brazo rodear mi cintura
¿Qué es lo que está sucediendo?
Un hombre al que no conozco nos cubre con un paraguas negro, se lo agradezco infinitamente, no quiero enfermarme. Y entonces creo que el señor Blackstone va a soltarme, pero no es así, me sigue sujetando hasta que me hace entrar en el Bentley color acero.

—Espero que no estés agotada por el viaje —dice cerca de mi oído.
No tengo tiempo de observarlo, porque me hace entrar en el auto, para después sentarse a mi lado. Frunzo mi ceño, no me ha gustado como suenan sus palabras. Mi corazón late con fuerza, sopesando sus palabras.
A mi mente vienen las palabras de Jesse.
No puedo aceptar la idea de que Alexander Blackstone busque algo conmigo. Es inconcebible, somos demasiado distintos. Incompatibles.
Además, él a mí no me interesa en lo absoluto.
—Debemos hablar cuanto antes —explica, segundos después—. Puede ser después de cenar. 
Asiento, bastante desconcertada, pero igualmente de intrigada. 


Mis ojos están fijos en los suyos. Alexander Blackstone no deja de mirarme, bebe su vino con tranquilidad. Mi respiración está acelerada, no puedo creer que tenga un informe de cada uno de los miembros de mi familia. Parece que el corazón va a salírseme del pecho.
—Es increíble que sigas ayudando a la escoria de tu hermano —habla seriamente, rompiendo el silencio incómodo y agobiante que nos rodea.
Abro la boca, enfada. Él no tiene derecho a husmear en mi vida privada, se supone que hemos firmado un acuerdo de confidencialidad cuando comencé a trabajar para él, se supone que entraban ambas partes en él. Es un patán entrometido.
—Usted no tiene derecho a hacer esto —argumento en un siseo.
Su expresión no denota ninguna señal de molestia ni sorpresa, nada. Sigue manteniendo una postura impenetrable y autoritaria. Me molesta que sea de ese modo, inescrutable.
Mastica lentamente su asado, mientras yo aprieto furiosa, la carpeta con fotografías y datos de mi familia. Es un maldito aprovechado.
Me levanto de la silla, completamente indignada, humillada, él no debe hacerlo. Tiene que respetar mi vida a mi familia. Esto es demasiado.
—¿Ya te contó tu hermano que me debe dinero? —la solemnidad de su tono no es lo que me hace caer de nuevo en la silla.
¿Ian? ¿Acaso se volvió estúpido o demente?
Pedirle un préstamo a Alexander Blackstone es lo último que yo haría en mi vida, es como comprar un boleto al infierno.
—Me debe setecientos mil —mi presión se desploma y el sabor de la bilis me aborda las papilas gustativas.
Tengo el corazón demasiado acelerado, no tengo hambre y verlo así, como si nada sucediera me hace querer abofetearlo hasta que reaccione y deje de ser un idiota, aunque dudo que sea posible.
Mis palmas comienzan a sudar a causa de los nervios. La cabeza me da vueltas, ¿de dónde pretende Ian pagar ese dinero?
—Es una lástima que tu hermano tenga que ir a prisión.
Tengo un nudo enorme en la garganta, la boca seca y los nervios de punta. Los ojos me escosen, estoy a punto de romper a llorar frente a él. ¿Cómo puede ser tan cruel?
—¿Por qué le dio esa cantidad de dinero si es más que obvio que no podremos pagarla? —inquiero con un hilo de voz.
Mis manos tiemblan y no sé si es de rabia o nerviosismo.
Sus ojos azules no demuestran más que frialdad y vacío. No hay nada que pueda decirme que espere algo bueno de todo esto, sino todo lo contrario. Mi estómago está hecho un nudo, doloroso, al igual que mis entrañas. Parpadeo, alejando las lágrimas calientes que luchan por salir de mis ojos.


—Porque hay algo que tú si puedes hacer —responde imponentemente.
Siento el alma en los pies, sus palabras me han logrado erizar la piel y no de una buena manera, mis manos forman dos puños, clavando las uñas en la carne blanda de mis palmas. No importa si duele, me siento humillada ante sus palabras. Es un poco hombre, embustero.
—Piensa en que, si no lo hubiese dado ese dinero a tu hermano, ahora mismo tu madre estuviera llorando su muerte —pausa brevemente—. Si aceptas lo que te propongo, ni tu madre, ni tu padre tendrán ningún tipo de escases económica, Sophie. Piensa en tu madre, que está enferma del corazón y necesita una operación con carácter de urgente, no tendrás estar viendo notificaciones de una hipoteca que debes pagar, tu sobrino tendrá la mejor educación que cualquier niño pueda disfrutar. No les faltara nada.
Sus palabras me atormentan y me ofuscan, está utilizando su poder de convencimiento. Su maldita facilidad de palabra.
Mi dignidad como mujer en este preciso momento está en suelo, siendo pisoteada por él.
—¿Cuánto tiempo crees que Ian duré vivo? —abro los ojos, incrédula—. Lo están buscando, tu hermano le debe demasiado dinero a la mafia que controla New York. Perdió unos kilos de la mejor cocaína y ellos quieren su dinero.
Trago saliva, dolorosamente. El pulso se me ha acelerado y de pronto siento que el aire me falta.
—El dinero que le di a Ian no es más que la mitad de lo que debe entregar mañana por la noche.
Limpio furiosa una lágrima que ha caído en mi mejilla izquierda, su semblante sigue siendo el mismo, frío y duro. Tengo la necesidad imperiosa de hacer girar su rostro con una bofetada, que recuerde el resto de su vida.
—¿Qué es lo que quiere a cambio, señor Blackstone? —digo entre dientes, furiosa, sintiendo una pequeña ola de valentía.
Ladea el rostro y una sonrisa maquiavélica se asoma en ellos, causándome un escalofrío y haciéndome perder cualquier tipo de valentía que mi alma pudiese albergar. No tengo posibilidades contra él, ni un millón de años. Es decir, él es un puto millonario arrogante y poderoso, tiene conexiones en todas partes, hace lo que le venga en gana.
Deja descansar sus grandes y cuidadas manos sobre la mesa de mármol, entrelazadas. Adoptando una aptitud pragmática y ventajosa.
—Podrías hacer muchas cosas por mí, Sophie —advierte con voz profundamente peligrosa.
Su actitud casi versátil me hace enfurecer aún más.
—Sólo dígalo —escupo con furia. 
La sonrisa se extiende por sus labios y sus ojos adquieren cierto brillo de triunfo y me hace sentir aún más humillada.
—Vas a ser mía desde hoy —promete con voz casi aterradora.', 
1);
INSERT INTO capitulo(titulo, contenido, id_historia)
VALUES('Capitulo 1 ', 
'-Señorita Lucy despierte
-Que hora es para que me molestes Virgo
-La hora para ir al campamento-dije hechando ropa a mi maleta
-Campamento? no jodas, eso es para gente naturista y todas esas estupideces-digo yo tapandome la cara con la almohada
-Ya, arriba-dice la voz mas aterradora de mi vida
-Aye Aquarius-digo en tono militar, saliendo disparada al baño2  a  

Tengo 16 años, vivo con mis hermanos Laxus y Sting, y mi primo favorito Jeral. A los 11 perdí la memoria y desde ese dia he tenido que vivir el presente. 

-Lucy ya estas lista?, vamos a llegar tarde a la presentación-dijo mi primo
-Si, ensegida salgo-digo vistiendome
-Apurate rubia oxigenada-dice mi hermano Sting, quien por cierto es mi mellizo y es rubio
-Tu tambien eres rubio pedazo de idiota-grito bajando las escaleras
-Hermanita yo soy natural, en cambio tu eres teñida, ah y por cierto buscate otro insulto-dijo entre carcajada
-Eres un bastardo, un aborto de mono fallido y...
-Y que?, ah, aborto de mono fallido ese me gusto, lo malo es que compartimos útero y día de cumpleaños-dijo mi hermanito querido, notese el sarcasmo
-Ya basta de pelear, par de rubios tontos-dijo mi hermano mayor Laxus, el tiene 18
-Que tu tambien eres rubio-gritamos en un unisonio con mi hermano Sting 
-Ya basta, vamos antes de que se nos haga mas tarde de lo que es-dijo calmadamente mi primito querido
-Si, ya vamos-dijo Sting
-Ah, por cierto hay que pasar a buscar a Yukino y a Rogue-dije yo
-Y tambien a Mirajane y a sus hermanos-dijo Laxus
-Por casualidad tendra una hermana de mi edad?-pregunto mi mellizo
-Si y se llama Lisanna-dijo Laxus mirando al frente
-Pero tocala y Mira que te rebana las pelotas-dije yo riendo
-Ah bueno yo decia-dijo mi hermano rescandose la nuca
-Quien se baja a buscar a Yukino y Rogue?-dijo Jeral
-Lucy-gritaron mis hermanitos queridos 
-Yo voy, no se preocupen-dije haciendome la victima 

Me baje a buscar a mis amigos para ir al campamento 

-Yukino, vamos-dije yo
-Ya voy-gritó desde adentro 

Yukino es mi mejor amiga, me crie con ella y su hermano Rogue, me lo dijeron mis hermanos, recuerden perdida de memoria, el es mejor amigo de mi hermano Sting. Son polos opuestos pero lo raro es que se llevan realmente bien

-Ya está, vamonos-dijo Rogue con las maletas de el y Yukino
-Ok, vamos- dije yo 

El viaje duró al menos 2 horas, entre que fuimos a buscar a los Stratuss y que se subieran al auto. El transcurso del viaje estuvo lleno de bromas y los mareos de Sting y Rogue, pedazos de imbeciles

-Ya llegamos-dijo mi hermano mayor
-Ya era hora-dijo mi mellizo junto con su amigo bajando apenitas
-Imbeciles-susurre
-A quien le dices imbecil-dijo mi hermano Sting
-Pues a ti-dije como si fuera obvio,
por que lo era
-Uh llego el equipo azul, con su trio de rubios tontos- se escucho una voz
-Que acaso quieres pelea afeminado-dije colocandome en posicion de pelea
-A quien le dices rubia oxigenada-dijo el pelirosa
-A ti marica aborto de mono fallido-dije yo
-Asi se hace hermanita, con el usa tu nuevo insulto-dijo mi mellizo', 2)
INSERT INTO capitulo(titulo, contenido, id_historia)
VALUES('Capitulo 2 ', 
'-Sting, cuantas veces te eh dicho que no fomentes la gran creatividad de Lucy respecto a los insultos-dijo cruzado de brazos un peli-azul con un extraño tatuaje en el ojo
-Pero si todo lo que dice mi melliza es verdad respecto a este marica-dijo el rubio teñido
-Olle siguo aqui-dije levantando la mano
-Y a quien le interesa afeminado? A verdad, a nadie-dijo la rubia sacandome la lengua, lo cual encuentro que es una acto muy inmaduro de parte
-Uh que maduros los rubios oxigenados-dije en burlandome
-A no, de mis hermanos si quieres burlate, pero de mi no-dijo un rubio alto con una cicatriz en el ojo, quien despues me golpeo en el ojo
-Uh que gran imbecil, perdón "hermano"-dijieron los otros rubios agregando un tono sarcastico
-Peliar es de hombres-dijo un albino alto
-Basta de pelea, guarden su energia para las pruebas del campamento-dijo Erza con un aura tenebrosa
-Aye-dije junto a los rubios 

Luego de armar nuestras carpas, llego una chica al equipo azul con el pelo negro
-Minerva-san llego tarde otra vez-dijo una chica albina de pelo largo
-Si lo que, pasa es que......-dijo alargando la e
-Que hiciste Minerva-dijo un peli negro 
-Es que posiblemente te enojes-dijo la peli negra jugando con sus dedos
-Miner...-fue interrumpido por una chica con pelo morado largo
-Lucy!!Yukino!!Mira!!Lisanna!! Las extrañe-grito esta chica
-Kagura!!-gritaron las cuatro
-Nosotras tambien te extrañamos amiga!!-dijo la rubia abrazandola
-Ya entiendo por que te ibas a enojar-dijo el peli azul riendo, quien no hablaba desde la pelea
-Calla Jeral-dijo el peli negro 

Jeral, Lucy y Sting esos nombres me parecen conocido.  

-Hola mocosos-dijo un pequeño anciano
-Y tu quien te crees que eres para hablarle asi al abuelo, ehh marica-dijo la rubia llamada Lucy
-Ara ara Lucy ese vocabulario-dijo una albina de cabello largo
-Si Lucy cuidado con esas palabras-dijo el gigante rubio de la cicatriz en el ojo
-Uhh solo por que Mira-san lo dijo tu lo repites-dijo Sting con cara de burla, mientras la albina y el rubio se sonrojaban
-Jellal, hueles eso-dijo Lucy respirando ondo
-Si, tu tambien lo sientes-dijo el peli azul imitando la acción de la rubia
-Pero creo que ya se que es-dijo una albina de pelo corto 
-Hay amor en el aire!!-gritaron los tres, haciendo que los 2 se sonrojaran como el cabello de Erza
-Hola siguo aqui-dijo el anciano pequeño
-Oh disculpe maestro-dijo la albina de pelo largo
-No te preocupes, mi nombre es Makarov Dreyer y ellos-dijo apuntando a los rubios-son mis nietos, a y se me olvida ¡BIENVENIDOS AL CAMPAMENTO FAIRY TAIL!
-Maestro, las presentaciones-dijo la albina de pelo corto
-A claro,gracias Lisanna-dijo el anciano-Equipo Azul presentece
-Mi nombre es Lucy Dreyer, el es mi hermano mellizo Sting Dreyer, mi hermano Laxus Dreyer y mi adorado primo Jeral Dreyer 
-Mi nombre es Mirajene Straussy el es mi hermano Elfman y mi hermana Lisanna 
-Mi nombre es Rogue y ella es mi hermana Yukino
-Mi nombre es Minerva y ella es mi hermana Kagura
-Ahora equipo rojo-dijo el anciano
-Mi nombre es Natsu Dragneel y ella es Wendy, y ellos son Erza, Gray, Juvia, Gajeel, Levy, Romeo, Cana y Lyon
-Bueno ella es Mavis-dijo el anciano apuntando a una rubia con un vestido
-Natsu ya conoces a Lucy cierto?-me pregunto la rubia
-Que, no disculpe pero se equivoco-dije yo calmadamente
-Seguro por que no le quitas la mirada de encima?-dijo Mavis mirandome con picardia
-Cabron si que son raros, son como tu horno de patatas-dijo Gray
-Que dijiste princesa de hielo-dije chocando frente con Gray
-Lo que escucha...-no termino por que fue noqueado por Erza
-Ja por mari..-dije y todo se volvio negro
-Imbeciles-esa voz es como la de Luce sera ella', 2)
INSERT INTO capitulo(titulo, contenido, id_historia)
VALUES('Algo espectacular',
'Tengo que volver con mis tíos y decirles lo que acaba de pasar. -Pensó el castaño.
Pero eso será después de clases. -Dijo.
Peter corrió lo más rápido que pudo hasta que finalmente llegó a la escuela, donde lo estaba esperando su mejor amigo Ned.
¡Hola Peter! -Gritó feliz su amigo rechoncho. 
Ned... -Susurró. 
Hola amigo. -Respondió el castaño.  
Ayer me preocupé, te fuiste muy rápido. -Dijo Ned regañándolo. 
Perdón Ned, es que ayer... estaba harto de Flash. -Respondió.  
Sí, es un patán... te diré algo, yo mismo lo derrotaré en el la actividad de hoy. -Dijo Ned.
¿Actividad de hoy? ¿Qué haremos? -Preguntó Peter un poco confundido. 
Hoy tendremos batallas de 1v1 con los estudiantes de nuestra clase, ¡será épico! Si así estamos en esta preparatoria ya me imagino el nivel de actividades que tendremos en la Universidad.
Será una buena oportunidad para usarlo. -Dijo Peter confundiendo a su amigo.
¿Usar qué? -Preguntó. 
Ned, tengo algo que contarte... yo... -Dijo el castaño siendo interrumpido.
¡Miren todos! ¡Iron Man está luchando contra un monstruo gigante! -Gritó una chica rubia.
Wow, esto va para el periódico escolar. -Dijo Peter sacando su cámara. 
Permiso voy a pasar. -Dijo Flash empujando a Peter.
¿No te habían suspendido por unos días, Flash? -Preguntó Ned algo molesto. 
Lo hicieron, pero pagué para que me quitaran la suspención. Perdedoreeeeeeeeeeeeeeeeeees. ¡JA! ¡FLASH ES EL MEJOR, BOOOOOYA!  -Dijo Flash burlándose.
Ese tipo parece que tiene una bellota en lugar de su cerebro. -Mencionó Peter levantándose. 
Flash escuchó eso. 
Te escuché, Parker. ¿Quieres volver a repetirlo? -Preguntó Flash molesto. 
Claro, cerebro de maní, cerebro de ardilla. -Respondió. 
¡¿P-POR QUÉ DIJE ESO?! -Se preguntó internamente. 
Tienes agallas, Parker. Tú, yo en la actividad de este día en clase de Connors. Si escapas, te iré a buscar yo mismo y no querrás poner un pie en esta miserable escuela. -Se fue furioso el pelinegro.
Creo que tengo mis días contados. -Dijo Peter dando un suspiro.
¡Heeeeeeeeey, señor Parker! -Gritó el hombre de acero aterrizando. 
¿Eh? -Preguntó. 
¡Hola! -Saludó quitándose el casco.  
H-hola señor Stark... -Respondió nervioso.  
Así que aquí es donde estudias, eh. -Dijo.  
S-sí. -Respondió. 
No está mal. ¿Sabes? Después de salir de aquí deberías entrar a MCU University. -Dijo el adulto. 
¡JA! ¿Pretende que un chico sin superpoder entre a la mejor universidad para gente con superpoderes? -Murmuraron algunos alrededor. 
El-ellos tienen razón... señor Stark, yo no estoy hecho para ser un superhéroe. Soy sólo un chico normal, o en este mundo anormal se podría decir, jaja. -Dijo Peter triste.  


No sabía que no tenías superpoderes... -Respondió el multimillonario.
Pues... ahora lo sabe. -Respondió el castaño.
Eso no cambia lo que pienso acerca de ti. -Respondió.
¿Qué? -Preguntó Peter.  
¿A quién le importa si tienes o no superpoderes? ¿Te digo un secreto? Yo no tengo ningún superpoder. -Reveló sorprendiendo a todos.
Lo único sobresaliente que tengo, es mi cerebro. Pero para eso estudié muchísimo, además de que nací siendo dotado debido a que mi padre sí poseía un superpoder que le permitía expandir sus conocimientos e inteligencia, desgraciadamente eso no me pasó a mí pero aún así soy lo suficientemente intelignete para ser uno de los más capaces superhéroes de la Tierra, sin contar con algún poder. -Dijo el hombre de acero.
Señor Stark... ¿usted cree... que... yo puedo ser un héroe? -Preguntó Peter con ojos brillantes, como si estuviera lagrimeando.
¡Por supuesto que sí, eres el más capaz de esta preparatoria... y eso que apenas y te conozco! -Gritó Tony sorprendiendo a todos.
Gr-gracias... Señor Stark. -Dijo Peter con una sonrisa sincera.
¿Dónde está ese chico Thompson? -Preguntó Tony.
Aquí estoy. -Dijo el rubio presentándose.
Hola, muchacho. -Respondió el multimillonario.
Hola señor Stark, es un placer conocerlo... soy su más grande fan. -Dijo el rubio muy entusiasmado.  
Gracias, pero te tengo una advertencia. -Dijo.
¿Qué? -Preguntó confundido.
Vuélvete a meter con Peter y él mismo te romperá ese estúpido ego que tienes. -Respondió Tony.
¿Eh? -Peter/Flash.
Tony se acercó al oído de Peter y le susurró:
Aplaasta a ese rubio egocéntrico, sé que tú puedes... araña. -Susurró Tony sorprendiendo a Peter.
¿Cómo lo...? -Peter.
Bueno, fue un placer. Ya me tengo que ir, adiós chicos. -Dijo Iron Man despegando.
Así que Stark se puso de tu lado, Parker... ¡Si antes estaba molesto contigo ahora estoy furioso, te aplastaré! -Gritó.
¡E-eh! -Gritó Peter asustado.
Ambos tomaron diferentes caminos, Flash porque estaba furioso y Peter porque quería evitarlo a toda costa.
Finalmente llegó la ansiada hora de Flash y la hora de la "perdición" para Peter.
A petición del señor Thompson los pondré en un 1v1, Peter... pero si no quieres, no tienes que participar. -Dijo Connors un poco nervioso por Peter.
Estoy bien. -Dijo Peter mirando fijamente a su oponente.
Parece que al fin dejaste de ser un niñito llorón, Parker. -Dijo Flash.

1...  
2...  
3...  

¡AHORA! -gritó un chico pelirojo.
Flash se cubrió de un material negro por casi todo el cuerpo.
¡Nosotros... somos VENOM! -Gritó.
¿Qué rayos es esa cosa? -Pensó Peter.   
Vaya, parece que alguien ya eligió su nombre de héroe y ni siquiera ha entrado a la Universidad aún. -Dijo Connors. 
"Venom" se acercó rápidamente hacia donde estaba Peter y le dio un poderoso golpe en el estómago. 
¡AAAAAAAH! -Gritó y se estremeció de dolor.   
¡¿ES TODO LO QUE TIENES PARKER, DÓNDE ESTÁ ESO DE LO QUE STARK ESTÁ TAN ORGULLOSO?! ¡LEVÁNTATE Y MUÉSTRAMELO! Eres BASURA. -Gritó Venom furioso.
¿A dónde estás mirando? -Preguntó el castaño.
De un movimiento rápido se posicionó arriba de Venom y le dio un poderoso golpe en la cara, para dejarlo sorprendido unos segundos.
¡¿QUÉ RAYOS FUE ESO?! -Gritaron sorprendidos todos los estudiantes.
Interesante... -Dijo un chico pelinegro con toques blancos. 
¡Ya no soy el mismo perdedor de hace unos días, Flash! ¡Y-yo... soy un futuro superhéroe! -Gritó.
Venom se enfureció más de lo que ya estaba y se dispuso a atacar a Peter y le dio varios golpes en el estómago y un derechazo en la mandíbula, pero este se repuso velozmente y curándose casi instantáneamente sorprendiendo al bravucón.
Esto... es un factor curativo, ¿eh? No es tan efectivo como el de Wolverine, pero servirá. -Dijo Peter poniéndose en pose de combate. 
Para desgracia de Peter él no sabía nada de combate pues nunca había entrenado, así que fue brutalmente golpeado por Venom, llegando ya al final...
¡Peter, levántate! -Gritó su amigo Ned.
N-Ned... -Dijo.
Admito que me sorprendiste, pero te llegó la hora Parker. -Dijo para juntar sus dos manos y darle un golpe que le hizo agrietar el suelo.
E-el ganador.. es... -Dijo Connors siendo interrumpido.
¡Espere, mire! -Gritaron.
En ese momento se levanta un Peter Parker muy lastimado de la cara y con varios huesos rotos, no sabe por qué se puede levantar todavía pero no se lo cuestiona y arremete un golpe en contra del chico peligro.
¿¡ES TODO LO QUE TIENES!? -Gritó Peter.
¿¡DESDE CUÁNDO TE HICISTE TAN FUERTE!? -Gritó Venom asustado.
¡Desde que dejé de tenerte miedo! -Gritó.   
¡MUERE DE UNA VEZ POR TODAS! -Gritó Venom, preparándose para dar el golpe final.
¡Yo... le hice una promesa al señor Stark, a mi tío y a mi tía! ¡Tengo que hacer que estén orgullosos de mí, y eso solo lo lograré... si me convierto en el hombre que esperan que sea! -Gritó dándole un golpe tan fuerte que lo lanzó varios metros lejos de él. 
El ganador ya se decidió... Peter Parker, por primera vez te has defendido y has ganado. Buen trabajo, tienes un +10. -Dijo Connors.
¡Sí! -Gritó Peter para luego caer desmayado.
¿Qué le sucedió? Hace un par de días era un chico sin habilidades. -Cuestionó una rubia.
Algo... Algo espectacular. -Respondió Ned muy feliz por su amigo.
E-eh... ¿Dónde estoy, por qué todo está tan oscuro? -Preguntó el castaño.  
Al fin nos conocemos, te estaba esperando... Bienvenido, Peter Parker. -???   
¿Quién es... quién es usted? -Preguntó.  
Nadie en especial, solo... una vieja conocida. -??? ', 3)
INSERT INTO capitulo(titulo, contenido, id_historia)
VALUES('capítulo 1',
'-¡¡Anaaaa!!...-Gritó Eleonor ya sobresaltada.- Jovencitas, ¡Por el amor de Dios! dejen de correr como niñitas! Es una orden, suban a sus habitaciones ahora mismo si no quieren quedarse sin merienda ni cena.
Emmaline y Eddyth le dedicaron una mirada de odio a su tía y como si aún fueran niñas entraron a la casa.
-Así nunca se casarán.-lamentó Eleonor recostándose en su silla-cama. -¡¡Anaaaaaaaa!! ANAAAAAAAAAAAA-Insistió.-.... Por Dios, ¿dónde está metida esta mujer?
Al cabo de unos segundos, una jovencita llegó a ella. 
-Disculpe señora, mi madre se siente muy mal, siente mucho dolor y la obligué a descanz...
Eleonor, interrumpiéndola, se levantó de golpe y se acercó a la jovencita. 
-¿Y quién demonios eres tú para dar ordenes a mis empleadas? Que yo sepa eres la hija de una sirvienta. ¡Llama a tu madre y dile que venga! Es una orden, ¡Peste! peste, peste. Eso eres. Tú y tu madre son una peste.
Desde dentro, Emmaline y Eddyth, observaban la escena. La joven a la que Eleonor llamaba peste, solo se limitó a bajar la cabeza y a tragarse el nudo que subía por su garganta.
-Pero... señora... Está débil, embarazada. Yo me haré cargo de sus tareas.-añadió la joven con valentía.
-¡Desgraciadaa!-gritó Eleonor que al decir esto la tiró al suelo de un chachetazo.- Trae a tu puta embarazada madre aquí, ¡que trabaje para mí o ya mismo quedan fuera de esta casa!. Lo hubiera pensado dos veces antes de acostarse con un vagabundo de por ahí.-Gesticuló Elenor.
La joven se alejó llorando desconsoladamente, nadie le había levantado la mano nunca, ni siquiera su propia madre. Mientras, las hijastras de Eleonor, ladeaban la cabeza con un gesto de desaprobación y negación. Era la primera vez que veían a su tía levantarle la mano a alguien. Aunque sí había rumores que lo hacía con toda la servidumbre, aquella vez lo estaban comprobando con sus propios ojos.
-Nunca creí que sería capaz. ¡Es una bruja!- Bajándo la voz, Eddyth acotaba lo que sus ojos veían.
-Nunca mejor dicho Eddy.-señalo Emmaline mientras analizaba la situación.
Al cabo de unos minutos una mujer de unos cuarenta y tantos apareció en escena: La madre de la jovencita que hasta hacía minutos había sufrido un acto de violencia aparecía dignamente y con la cabeza en alto. Debía de estar de unos siete u ocho meses, su barriga la delataba y su embarazo estaba teniendo algunas complicaciones pero no dudó en presentarse ante aquella bruja que había pegado a su hija.
Las jóvenes hermanas no podían entender cómo su tía era capaz de hacer una cosa así, la gente de la servidumbre era gente humilde, buena y trabajadora. Lo pudieron notar a lo largo de los años y les costaba ver cómo aquella bruja abuzaba de ellos, en especial de Ana, la mujer embarazada. Con el tiempo, ésta, se había convertido en una madre para Eddyth y Emmaline y Eloisa, su hija, en una hermana más. Hecho que le revolvía el estómago a la señora Eleonor.
 Al cabo de unos minutos Ana salió de la cocina y fue hasta Eleonor.
-Aquí está, su comida, señora. Lo que me pidió-Ana fue la encargada de cocinarle y traerle la comida a Eleonor, así lo había pedido y Ana no era mujer débil como para dejarse pisar tan facilmente. De forma indirecta le estaba desmotrando a La Bruja que su embarazo no le impedía hacer nada y Eleonor tragándose la vena, comía. No tenía derecho a desperdiciar aquella comida, Ana cocinaba como los dioses y eso Eleonor lo valoraba. Pero cuando terminó y la comida sobró, la tiró a los pies de Ana que se había mantenido inmóvil hasta ese momento.
-¡Come peste! Come esa mierda.-Dijo Eleonor con unas copas de más encima, luego se marchó riendo.
Diez minutos después, las hermanas Acevedo, fueron en busca de Eloisa para consolarla y pedirle disculpas.',4)
INSERT INTO capitulo(titulo, contenido, id_historia)
VALUES('1✔️ ',
'Sólo existía una palabra para describir como me sentía en este preciso momento y era: ansiosa. ¿O no...? Quizás emocionada, eufórica y apunto de abrazar a cualquiera que se sentara a mi lado.
Definitivamente era una mujer de muchas palabras que jamás podría usar sola una para definir algo en concreto.
Lo que estaba a punto de hacer era una locura y no podía dejar de pensar en todo lo que podría suceder en los próximos tres meses de libertad que tendría lejos de Londres.
Al ser una Rivers, mi vida siempre había estado llena de obligaciones hasta que cumplí los dieciocho años, cuando las cosas se aligeraron sólo un poco para mí, pues decidí salirme de la casa de mis padres y empezar a tener mayor independencia. No era que mis padres estuvieran todo el día en su casa, a decir verdad ellos tenían cosas más importantes que hacer; como viajar, trabajar y preocuparse de su imagen, pues eran dueños de una de las mejores cadenas hoteleras a nivel mundial y ahora mismo mi hermano, Andrés, estaba construyendo un nuevo imperio para la familia en el mundo del modelaje y la moda.
Sí, esas personas amaban el dinero y por ello debían trabajar sin descanso; sin embargo, mis planes eran algo... diferentes.
Comprendía que una vez que regresara tendría que ponerme a trabajar en los hoteles de mi padre —ese fue el trato que hice con Diego, mi progenitor—, había estudiado administración de empresas sólo porque él lo quiso y lo hice bien, fui la mejor de la clase y una de los muy pocos egresados por excelencia.
Me habían criado para ser la niña perfecta y a veces quería vomitar por ello.
Odiaba los números, odiaba los hoteles de mi padre y odiaba ser una Rivers.
Hasta ahora estuve manejándome con el apellido de mi madre, Johnson, para evitar que existiera una preferencia hacia mi persona.
Todo el mundo quería caerle bien a un Rivers y yo era el tipo de Rivers que no quería que todo el mundo le besara los pies; y sí, era la única de la familia que pensaba así. Muchas veces Andrés me había dicho que por eso no tenía amigas, pues a nadie le interesaría ser amiga de un don nadie.
Mi hermano era tan, pero tan adorable.
Aunque debía admitir que fue con ayuda de mi hermano que evité ser presentada como una Rivers a mis dieciocho ante los medios, por lo que todos sabían que había una Rivers, pero aún no tenían el placer —porque sí, ver mi hermoso rostro debería ser un placer para cualquiera— de conocerme.
Mis días de libertad estaban llegando a su fin, Diego me lo advirtió, pronto todos sabrían quién era y ya no podría seguir escondiéndome de ese mundo que no era de mi agrado. No podría moverme de un lugar a otro sin ser observada porque obviamente todos querrían saber qué tan perfecta era la hija menor de Diego Rivers.
—¡Dame mi libro! —Respingué al escuchar el lloroso grito de una niña y vi que un niño corría en mi dirección con un libro en la mano mientras una pequeña corría tras de él.
Quitárselo fue fácil, y satisfecha entregué el libro a su dueña. El niño gruñó con tristeza, claramente se le había acabado la fiesta mientras esperábamos nuestro vuelo, y la pequeña sonrió con felicidad.
Ella se fue corriendo y el chiquillo la siguió regalándome una última mirada de enojo. Eran hermanos y lo cierto era que me costaba entender ese jueguecillo de torturar a la hermana menor. Andy jamás hizo algo igual, al contrario, si alguien se atrevía a molestarme, era carne muerta. Pero... Andrés nunca fue un niño normal, ni siquiera ahora era una persona normal porque mi padre lo tenía atado a todas las responsabilidades familiares.

La madre de Jimena la dio todo el amor, la protegió, pero solo pudo hacerlo hasta que un desconocido se llevó su vida. A los 14 años, se vio acosada por una familia que la rechazaba por falta de "pedigrí". Nadie la ayudó, hasta que llegó su tía abuela Maggie, y con ella el dinero y el prestigio que los demás codiciaban. Pero ella ya había aprendido a odiar todo lo que ellos representaban.
Gabriel consiguió salir del pozo más oscuro, y lo hizo pagando un alto precio. Se abrió camino a codazos, hasta llegar a confraternizar con la clase alta. Antes ellos lo habían usado a él, ahora los papeles habían cambiado.
Lo que los unía a los dos, era lo que los separaba. Pero el destino tiene muchas formas de reírse de nosotros.

Ese pensamiento hizo que quisiera huir, no deseaba convertirme en alguien como Andrés, quien sólo pensaba en el dinero y quedar bien ante los ojos de los demás. El imperio Rivers no era para mí, sabía que era mi hogar pero por alguna extraña razón me sentía ajena a él.
Observé el pasaje que tenía en la mano y sin pensarlo lo presioné como si mi vida dependiera de ello.
Miami sería mi lugar de escape, tendría tres meses para saber qué querré hacer con mi vida para mi regreso. Los próximos tres meses me los pasaría lejos de Londres, a pesar de vivir sola, a mis veintitrés años eso ya no me bastaba; mientras más lejos estuviera del control de mi padre y hermano, mejor. Y, por supuesto, con lejos me refería a otro país, otro estado y otro continente.
Era una lástima que no pudiera irme a otro planeta.
Al darme cuenta de la dirección que estaban tomando mis pensamientos, esa que me llevaba a analizar a la poderosa familia Rivers, reí silenciosamente ladeando la cabeza.
Nada arruinaría mis vacaciones.
Al escuchar el llamado a todos los pasajeros de mi vuelo no pude evitar emocionarme más de lo normal, cualquiera pensaría que era la primera vez que me subía a un avión —cosa que no era cierta porque ya había viajado con mi familia en reiteradas ocasiones, recibiendo el apoyo de Andrés porque ciertamente los vuelos me daban pánico—, y di un salto entusiasmado.
¡Al fin abandonaría Londres!
Toda la multitud se puso de pie ante el llamado de la asistente de vuelo y me froté las manos sudorosas con nerviosismo. Nada malo pasaría, ya no era una niña, supuestamente el tiempo debería haber mejorado aquel miedo que siempre me carcomía cuando estaba en un avión, por lo que tenía la fe que nada malo sucedería durante las siguientes nueve horas de viaje que me quedaban.
Arreglé mi blazer rojo con torpes movimientos y tragué saliva al darme cuenta que esto me estaba inquietando más de lo normal; no obstante, sólo llegaría a Miami lo más antes posible por este medio de transporte por lo que no podía quejarme. La gente se movía con prisa y yo aún no era capaz de dar un solo paso.
Inhalé y exhalé con fuerza y asentí con tranquilidad, la última vez que vomité en un avión fue a mis nueve años, ¿qué tan mala suerte podría tener para repetir una escena tan bochornosa como esa?
Nuevamente volvieron a llamar a los pasajeros de mi vuelo y sobresaltada, consciente que si quería libertad debía avanzar, di un paso hacia la puerta, lamentando al instante mi decisión.
—¡Maldita sea! —bramó el hombre que tuvo la mala suerte de ponerse en mi camino y abrí los ojos de par en par al ver como el café —que seguramente estaría muy caliente— se deslizaba por el costoso saco del rubio.
—Lo siento tanto —dije al instante usando la manga de mi blazer para limpiar un poco del desastre que ocasioné en el susodicho de la mala suerte, y con un gruñido él apartó mi brazo de una manera muy poco caballerosa y hasta cierto punto violenta.
Tal vez ya no lo sentiría tanto.
—Sólo está empeorando la mancha.
Me fijé en el resultado de mi arduo esfuerzo e hice una mueca dándole toda la razón. Lo mejor habría sido que me quedara quieta. Desde un principio, claro está.
—Lo siento —susurré verdaderamente apenada y levanté la mirada, encontrándome así con el hombre más atractivo en la faz de la tierra que fácilmente podría hacer de doble para el actor que interpretaba a Thor. O quizás mejor a uno porno, esos que salían con sus montañas de músculos y piernas fuertes presumiendo su... Un momento, yo no veía porno, ¿cómo sabía que salían así? ¿O quizá lo hice alguna vez? Bueno, sería un misterio que jamás podría ser resuelto. Aunque... imaginándome a ese hombre en paños menores, estaba a poco de replantearme la idea de añadir un nuevo género cinematográfico a mis preferencias.

Le di una rápida inspección de pies a cabeza —porque no podía darle una vuelta y analizar hasta su retaguardia—, y contuve un suspiro al comprobar que efectivamente se parecía un poco a mi actor favorito. Rasgos firmes, una hermosa piel besada por el sol y profundos ojos celestes, ¡era un muñeco Ken en la versión del dios del trueno!
Evité exteriorizar lo mucho que me afectaba su aspecto físico mientras me preguntaba una y otra vez que tal se vería en un traje de baño.
Con un chasquido de dedos el rubio me hizo regresar a la realidad y me vi obligada a dar un paso hacia atrás al detectar el enojo en su mirada. Claro, ¿cómo pude olvidarlo? Acababa de arruinarle un muy costoso traje.
—¿Me estás escuchando?
Arrugué el entrecejo. Su voz también era linda, pero su carácter era una mierda.
Recordé que mi imprudencia posiblemente no podría hacer feliz a nadie y toleré su tono de voz haciendo acopio a mi buena personalidad.
Miré a los alrededores, siempre fui una persona algo torpe pero nunca pensé que el no corregir ese insignificante defecto —que todos tenían uno, o dos, o quizás un poco más como en mi caso— me llevaría a conocer a un hombre tan guapo y gruñón en estas circunstancias. 
Adiós a la posibilidad de encontrar al amor de mi vida en el hermano perdido de Thor.
Guiada por mis instintos femeninos acaricié mis ondas rubias como señal de nerviosismo y esperé que el hombre me dejara ir; no obstante, él me miró con fijeza esperando que dijera algo y no tuve más remedio que pensar en mis siguientes palabras.
—Le pagaré.
No adoraba ser una Rivers, pero el orgullo lo llevábamos en la sangre y emitir una disculpa era un trabajo muy duro para nosotros, más cuando ya me había disculpado con el imbécil y él me había tratado de manera desagradable.
El susodicho se rio sin humor alguno y estoy segura que no dije ningún chiste. 
—¿Tú? —No me gustó el tono despectivo que utilizó ni mucho menos como me miró de pies a cabeza con desprecio—. Sí, cómo no. Apártate de mi camino, muchacha, no estoy para perder el tiempo.
Lo miré con incredulidad, ¡nadie hablaba a un Rivers en ese tono y se salía con la suya!
—Puedo pagarle.
Con el dinero de mi padre, pero podía hacerlo y eso era lo importante en ese momento.
El hermoso rubio que ahora esperaba fuera el doble de Thor para las escenas más peligrosas —quizás una donde casualmente el dios del trueno cayera en un mar lleno de tiburones—, me pasó de largo golpeándome con su fuerte brazo en mi hombro para que me apartara.
Al ser una mujer de estatura pequeña, salí disparada hacia un costado y tardé unos segundos en ganar estabilidad. Contando hasta tres, conseguí regularizar mi respiración y luego fulminé con la mirada al hombre que avanzaba hacia una de las puertas de embargue.
Atrevido, malcriado y animal, eran apelativos humildes para ese salvaje.
Sujetando mi bolso con seguridad y agradeciendo llevar zapatillas planas, salí disparada en su dirección y me clavé delante de él por segunda vez en esta noche. Mi paciencia había llegado a su fin y un Rivers enfadado no era una persona muy agradable para nadie.
—¿Qué quieres? —ladró el animal de dos patas dispuesto a empujarme otra vez.
Sin darle una respuesta alcé la mano golpeando el vaso de café que aún él tenía en la mano y salí disparada huyendo a toda prisa, mezclándome entre la multitud mientras escuchaba los gritos amenazantes del hombre al que ahora, estaba segura, le había arruinado su costoso traje de corte italiano.
¡Eso era para que aprendiera a respetar a las mujeres!', 5)
INSERT INTO capitulo(titulo, contenido, id_historia)
VALUES('Uno.','Paso el dedo por encima de las caras de mis padres, y el corazón de nuevo se me estruja. Sonrío con tristeza, y elimino las lágrimas que tratan de salir con apenas unos parpadeos.  
—Esto es por ustedes, chicos —Susurro mientras los miro, y me imagino a mamá sonriendo y diciéndome que soy valiente y muy fuerte por hacer esto. Sonrío ahora sí con ganas y asiento mientras respiro hondo y me preparo para lo que viene.
John entra, y viene con un par de tazas de café en las manos, me sonríe y parece de verdad alegre, yo lo estoy.
—Hoy es el día —Me dice mientras me tiende la taza y sonríe hacia mí. Asiento y la tomo, le doy unos sorbos y el café granizado me saluda mientras su sabor se esparce por mi sistema.
Tengo las manos hechas un manojo de nervios, aunque pensándolo bien, todo yo soy un manojo de nervios. Desde hace dos años he estado trabajando con John aquí en la comisaría, aunque no soy policía, casi que me he convertido en uno, paso aquí casi todos los días, y sí, estoy aquí por lo de mi familia, averiguando quienes eran los responsables de su muerte, y aunque aun no sé porque lo han hecho, ya sé quienes son.
Hoy es su juicio. 
Trabajar aquí me ha despejado y ayudado tanto que, John y yo hemos salido a resolver otros casos, por lo que todos en la comisaría me tratan como un superior, y eso que ni placa tarigo, aunque según John, dentro de dos semanas, será la graduación de los nuevos policías, y yo estaré ahí, para convertirme oficialmente en uno. 
Me termino la bebida, y me siento un poco más relajado. Tengo que estarlo, he esperado tres años para esto, para mirar a la cara a los desgraciados que me hicieron esto y poder decirles que se van a pudrir en la cárcel y manicomio, y que jamás volverán a ver la luz del sol.
Son las doce y media, el juicio es a la una, por lo que he estado aquí pensando en mis jugadas para este juicio, tengo que ser inteligente, y no dejarme llevar por las emociones, al menos no en el juicio.  
—¿Están todas las pruebas? —Le pregunto a John, él asiente. 
—Sí, nuestro abogado las tiene todas, y yo he hecho una copia, aunque sabes que no se puede, así que quédate callado —Y abre un cajón de su escritorio, saca una carpeta de color azul y me la pasa.
Le doy una ojeada, y veo todo, los sentimientos se me remueven, y les permito hacerlo, ya dentro de unos minutos tengo que estar calmado, sereno y siendo todo un adulto, así que es mejor si lloro en este momento, que luego en el juicio. 
La casa en llamas, las imágenes de mi familia, la historia de como pasó según los culpables y las fotos de ellos. La rabia me sube y siento la sangre dentro de mí moverse furiosa, hoy no podré hacer nada contra ellos, pero eso no significa que no vaya a visitarlos a la cárcel algún día. 
Dejo la carpeta encima del escritorio de John y me calmo. Por fin se está haciendo justicia, es todo lo que necesito en la vida, todo lo que me haría feliz y me haría tener un buen descanso, porque es lo que necesito antes de comenzar con la venganza, porque sí, sé dentro de mí que esto no ha acabado, al menos no para mí, necesito regresarles el daño que me han hecho, necesito que sientan el dolor que yo he sentido por tres años seguidos.
Aunque sé que eso no me regresará a mi familia, la sangre que corre por mis venas necesita ser vengada. John me toca el hombro y me dice que es hora, miro mi reloj y el corazón me da un vuelco, asiento. Me levanto, me organizo el bleiser y la corbata.
—Vamos —Digo mirando a John. Él se contagia de la convicción que transmiten mi mirada y la postura de mi cuerpo.
 
••• 

Las manos me sudan, y siento el corazón en la garganta, pero no de miedo, para nada, es la ira que me recorre el cuerpo entero, los entrenamientos que hago cada día desde que tenía diez me tienen alerta, y soy capaz de dar un salto de casi tres metros y caer sobre las cabezas de ambos, pero eso sería dejárselas fácil, y no necesito que mueran, o al menos no aún, lo que necesito es hacerles daño, hacerlos sufrir y pagar por todo el daño que me han ocasionado, y no solo a mí, sino también a mi indefensa familia que murieron quemados.
Siento una mano en mi hombro, volteo a mirar hacia atrás y veo a Stiles que tiene lágrimas en los ojos, pero me sonríe de forma sincera, y todo la rabia se eclipsa por un momento. Asiente hacia mí diciéndome con la mirada que todo está bien y que ya no estarán más. Asiento de vuelta hacia él, a su lado, John nos mira con balas en los ojos a punto de ser lanzadas, no le presto atención y en cambio le aprieto la mano a Stiles por un momento, y luego me separo, él hace lo mismo.
—Bueno, dado el resultado demasiado evidente de los acusados, ésta corte dicta que hoy, cinco de agosto del dos mil diecinueve, Peter Hale y Katherine James Argent son culpables con los cargos de, piromania, allanamiento de residencia privada, y violación a la vida, a ochenta y siete años de cárcel —Y golpea el mazo contra la superficie. La satisfacción por eso me hace sonreír como un idiota, y todas las personas en la sala se levantan para aplaudir y dar vítores de alegría.
—¿¡TE DIGO ALGO?! —Un grito se escucha por entre todo el bullicio y el silencio se propaga, dirijo mi mirada hacia la voz —Ella gritó como nunca en su vida, y me pidió que no te dejara sin madre, pero era una perra egoísta, y tu eres quien paga las consecuencias, sobrinito —Sin pensar en lo que hago, solo obedeciendo a mis instintos, me lanzo por encima de la mesa y echo a correr hacia él, varios guardias vienen a detenerme pero a todos los esquivo con facilidad, me importa una puta mierda que las personas me vean con colmillos y garras afuera.
Lo mato, lo mato. Es todo en lo que puedo pensar en este momento, gruño tan fuerte, que la camisa se me rasga por detrás y por delante, él trata de correr despavorido, pero lo agarro del cuello del entero naranja que usan los presos y lo lanzo contra la pared, el golpe resuena, y escucho los gritos a mi alrededor de la gente horrorizada, pero la satisfacción de escucharlo gritar me consume, y lo levanto del suelo, para volverlo a lanzar contra él, algunos huesos se le rompen, pero eso no me basta.
Estoy a punto de agarrarlo de la cabeza para sacarle los dientes, pero siento como algo me aprisiona los brazos y luego una aguja se incrusta en mi brazo derecho, y segundos después el mundo se me vuelve negro.',6)
INSERT INTO capitulo(titulo, contenido, id_historia)
VALUES('01','El viento daba contra mi rostro, el aire del bosque que estaba al lado de mi casa me hacía sentirme plena, serena y con calma. Por instinto miré mis manos, las suavicé un poco y me solté riendo.
Cuando estaba casada no me quitaba los anillos para nada, ahora que estoy soltera sigo mirando que como ahí no me dio el sol, se quedo una pequeña huella. Alguien tocó el vidrio y giré, era mi pequeño Lucca. Sonreí.
La abrí y me agaché para estar a su altura.
— ¿Pasa algo, cariño? —pregunté mientras acomodaba su cabello que apenas había cortado.
Yo les cortaba el cabello, me gustaba hacerlo yo y los cursos de estilista me habían funcionado. Pasé mi mano por su suave mejilla, sus ojos azules me miraron con amor y ternura que reflejaba su alma. Miré a la celosa de Thya correr hacia nosotros y abrazar mi pierna.
—Olé, hermosa. ¿Por qué eres tan celosa, princesa? —pregunté mientras acomodaba su cabello café hacia atrás.
Ella sonrió victoriosa, Lucca no era celoso porque sabía que Thya lo era.
—Están tocando la puerta, mamá —habló Lucca y asentí.
Me acomodé la chaqueta negra y Thya se separó de mí. Comencé a caminar hacia la puerta principal, escuché que Lucca cerró la puerta para el balcón, a mí no me gustaba que ellos estuvieran ahí si no estaba yo, siempre respetaban esa regla y mejor se sentaron en la sala.
Abrí la puerta y era Nicolás. Asentí.
—Niños, ya vino su padre por ustedes —hablé sin ganas y ellos asintieron yéndose hacia la planta de arriba para recoger sus cosas.
— ¿Son tuyas? —preguntó mirando las flores blancas que yacían en la mesa.
Un ramo hermoso, un ramo envuelto en una hoja blanca con decorados dorados. Sonreí. Sí sabía de quién era, pero no necesitaba saber, no aún.
—Ah... Sí es mío —me alejé de la puerta y lo tomé entre las manos—. Gracias por acordarme, después estas bellezas se marchitan por mi culpa.
Él asintió serio. No estaba lista para decirle a nadie, aunque él piense que salgo con mi doctor –quien aclaro, es gay–, ni siquiera yo sé si se puede decir que estoy en algo serio con alguien. Tomé el florero gris y acomodé las flores mientras sentía la insistente mirada de Nicolás.
Escuché pisadas en las escaleras. 
—Niños, no corran en las escaleras —los regalé y ambos sonrieron.  
Fueron por sus productos personales a el baño de abajo, coloqué el florero en su lugar.  
—Pasa, si gustas puedes sentirte en la sala —hablé con cortesía y asintió.  
Cerró la puerta tras de sí y se sentó en el sillón. Tomé mi teléfono y comencé a escribir.  
Alois. 
Querida, ¿quedamos esta tarde? 
Sonreí.  
Sherlley. 
Claro.  
Alois. 
Paso por ti en dos horas.😏 
Bloqueé el teléfono.
—Insisto, ¿Quién te trajo las flores y no sabe qué no te gustan las rosas amarillas? —preguntó Nicolás detrás de mí.
Lo miré mientras dejaba mi teléfono en mi bolsillo.
— ¿Y quien dijo que no me gustaban? Son lindas, son únicas a decir verdad —alcé una ceja y su mandíbula se tenso.
Me da a igual si se pone celoso, no somos nada y eso se lo merece. Que le dé gracias a Dios que le estoy respondiendo y no lo dejo con la pala es en la boca, solo lo hago por cortesía y porque sigue siendo el padre de mis hijos, si no, no lo hacía.
—Ajá. 
¿Qué no podía quedarse callado el maldito bocón de mi ex esposo? «No te alteres, no lo merece»
Despedí a mis bebés más de cinco veces, los abracé cuantas veces me fue posible mientras se los encargaba a Nicolás y él solo respondía con un "Ajá, claro que sí" bastante motorizado. Se irían por una semana con la familia de Nicolás a la playa, los extrañaría y los llamaría todos los días.
Ellie me dijo que si quería ir, pero era mejor que estuvieran con su padre. Yo no encajaba en ese plan, era obvio.
Mantengo una... ¿Buena? ¿Sana? Relación ella, obviamente ya nada es como antes. Nos vemos muy poco, y platicamos lo más superficial, la confianza entre nosotras también acabó.
Como siempre, Nicolás se quedó un momento para hablar conmigo.
—Ah... Lucca no ha hecho su tarea, la lleva consigo, supongo —le mencioné y asintió.
Me miró. No podía decir que él no era atractivo, sus pequeñas arrugas lo hacían más interesante y su cabello recién cortado le daba un aspecto de hombre amable. Pero lastimosamente, ya solo lo miraba como el padre de mis hijos, no como un hombre.
—Entiendo... Si por alguna razón no se la llevó, te los traigo el Domingo —susurró y asentí.
—Y... ¿Ella cómo está? —pregunté con dolor en el corazón.
—No quiso bajar a saludarte, por más que la hice bajar y de hecho... —sacó el teléfono de Ivvy de su bolsa—. No quiso hacerlo.
Suspire, se me volvieron a hacer pequeñas lágrimas en mis ojos.
—Necesitamos hablar con ella, urgente. No voy a permitir que siga haciendo lo que quiere casa vez que te ve.
Habló él y asentí
—Ya hemos hablado con ella, Nicolás.
—Lo sé, car... Sherlley —lo miré y desvío la mirada hacia otro lado de la habitación—. Pero siempre vamos a lo mismo, tenemos que hablar con ella en serio, darle un alto.
—Dejarle en claro que lo que pasó solo es entre nosotros, y... A ella no le debe de incumbir —completé la oración y él asintió—. Bien, que les vaya bien. Ya cuando regresen nos podemos poner de acuerdo para hablar con ella.
Él asintió.
—Sher...
— ¿Sí, Nicolás?
— ¿Puedo pedirte un favor y preguntarte algo? —susurró.
Suspire.
—Ya lo hiciste, ¿no? —pregunté sin ánimos y me miró.
—Por favor.
Suspire. Sabía que venía después. 
—Nicolás, miré la grabación y no me cupo duda alguna de que buscaste amor en la golfa de mi ex mejor amiga —se desencajó—. Fuiste como perro jarioso detrás de ella, lo sé. No entiendo de qué quieres hablar.
Suspiró.
—Sherlley, por favor... Estamos a tiempo de...
— ¿Sabes qué? Mira... Pregúntate a ti que hubieras hecho si hubiera hecho yo lo mismo. —di un paso hacia el frente y fui acorralándolo contra la pared— Imaginate que alguien más me hubiera tocado con sus manos en donde a ti te pertenecía.
—Sherlley —advirtió.
—Imaginate las manos de otro sobre mi cuerpo, imagínate que mientras tú estabas cuidando a nuestros hijos yo estuviera revolcándome en la oficina con otro. 
Cerró los ojos y susurre en su oído.
—Pero fuiste tú quien falló, yo sólo me defendí.',7)
INSERT INTO capitulo(titulo, contenido, id_historia)
VALUES('001','Yoongi llevaba más de una hora sentado en la incómoda silla de una cafetería con el trasero cuadrado, y media hora de ella, recibiendo los regaños de su mejor amigo. Preguntándose internamente el por qué lo considera como uno. Oír la exasperante voz irritada y molesta lograban que su cabeza punzara de dolor y que su paciencia se estuviera agotando, pero tampoco era como si tuviera la necesaria para soportarlo por otro cinco minutos más. Tenía límites, y uno que estaba a muy pocos pasos de ser explotado, porque escuchar las mismas palabrerías de siempre lo estaban carcomiendo vivo. 
Aunque solo hubiese escuchado la mitad de su discurso, porque la siguiente parte, le había entrado por un oído y salido por el otro.
Su Mocca del día estaba siendo consumida de manera abrupta y desesperante. Hasta algo fría.
Kim Namjoon conocido especialmente como el Dios de la Destrucción por romper todo a su paso y por la forma inigualable de poder hacerlo, y su mejor amigo. O el único que aguantaba su personalidad arisca; lo había invitado a tomar un delicioso café a las afueras de su casa. Siempre visitaban el mismo lugar de todos los días, o específicamente, solo en las ocasiones en las que ambos podían concordar y ponerse de acuerdo en salir.
Cada uno tenía su propio mundo, sus propias metas y problemas, y carreras. Se habían conocido por casualidades de la vida, o por el solo hecho, de compartir los mismos gustos musicales.
En fin, la música los había unido.
—¿Cuántas veces te he dicho que tienes que controlar tu ira? No puedes ir por el mundo pidiendo un trabajo para que a los cinco días seas despedido por tratar mal al cliente —su tono de voz era demandante a pesar de ser el menor
—Fue su culpa —se justificó sin interés alguno
Nam rodó los ojos.
—Por las leyes de la política, el cliente siempre tiene la razón, Yoongi hyung.
—¡Que se jodan las leyes de mierda! —bebió lo último de su fría Mocca—. Además, él fue el idiota que se equivocó en pedir su hamburguesa con mostaza. ¿Acaso es mi culpa que el imbécil sea alérgico a ella? ¡No! —levantó sus brazos en señal de desinterés—. ¿Es mi culpa que casi se haya muerto y colapsado en el suelo? ¡No!
El menor volvió a rodar sus ojos.
A falta de paciencia, falta de respeto y humanidad.
—Estaba en su derecho de reclamar a pesar de que él se hubiese equivocado.
—Su único derecho, fue haberse estado muriendo por su estupidez.
—Contigo nunca hay caso —movió su cabeza negativamente—. ¡Ay, por Dios! Me comparezco de la pobre persona que tendrá el valor de amarte —Min lo miró feo, entrecerrado sus ojos en el proceso—. ¿Crees que Suran, tendrá esa paciencia?
Namjoon sonrió satisfecho ante sus palabras cuando la vista del mayor se desenfocó de la suya.
Yoongi siempre había estado al tanto de todo. Sabía que su personalidad siempre había sido impulsiva cuando se trataba de alguna cosa estúpida e inmadura, y aunque él también fuera algo infantil en muchas ocasiones, no le daría el gusto a Namjoon de decirlo en voz alta. Pero muy en el fondo, también se preguntaba si alguien llegaría a amar a un gruñón como lo era él.
O si llegaría a conocer a semejante persona.
O sí especialmente, Suran lo haría. 
Se había enamorado de ella la primera vez que la vio en algunas de las tiendas de música que siempre solía frecuentar con Kim. Se notaba que aquella mujer le llevaba unos cuantos años de diferencia, pero sin ser la consecuencia de su desamor y desinterés; la edad siempre le supo a insignificancia. Pero, también le parecía algo tonto si llegara a pensar que era un amor a primera vista, o que si en verdad la palabra amor existía en aquella franquicia, o que solo era una atracción vaga hacia su persona. Pero deleitarse con su delicadeza, su forma de hablar y brillar ante sus ojos mientras lo hacía, lo descolocaba en menos de un segundo. Algunas veces intentó acercarse a ella, pero muy en el fondo, Min era alguien tímido con todo lo que tenía que ver con el romanticismo.
Y su fracaso, siempre era burlado por su menor.
—Cambia esa cara de amargura que solo era una broma, Hyung —trató de animarlo, aunque Suran no le agradara del todo—. Mejor vámonos a un bar que yo invito todo por este día. Conste, que después te toca invitar a ti con gastos incluidos.
—¿Por qué tú y yo seguimos siendo amigos? —dobló su boca en una mueca',8)
INSERT INTO capitulo(titulo, contenido, id_historia)
VALUES('C: 01','Isabella miró con emoción la ciudad, como esta se expandía ante sus ojos bajo el hermoso atardecer.
Cannes, Francia. No llevaba más de cinco minutos en la ciudad y ya se sentía totalmente enamorada de ella.
—No quiero retrasos, tampoco quiero berrinches y menos problemas. Te hablo exactamente a ti Isabella, ¿entendido?
Isabella soltó un suspiro y asintió, sin querer mirar a su padre. Estaba cansada de él, ya lo había soportado durante todo el viaje. 
Llegaron a la mansión, el nuevo hogar de los Romanov. Isabella no se sorprendió al ver que se trataba de una casa gigante y muy elegante, su casa es Moscú era igual o incluso más grande. 
—¿Te gusta? —su madre le sonrió, abrazándola de lado.
—Sí —Isabella le devolvió la sonrisa.
—Es igual que la anterior. No tiene nada de especial, querida madre —Candace llegó al lado de su hermana y de su madre con la maleta en sus manos.
—Nadie pidió tu estúpida opinión —Isabella la miró.
Candace clavó sus ojos en los de Isabella y ambas mantuvieron la mirada por varios minutos, trasmitiéndose todo el odio mutuo que sentían.
—No es momento de peleas, niñitas —Irina las miró con algo de desilusión—. Estamos recién llegando a la ciudad.
Isabella soltó un suspiro, despegando sus ojos de los de Candace y se adentró en la casa, solo para ignorar a su hermana mayor. No pretendía comenzar una discusión y menos frente a su madre; a ella la respetaba muchísimo.
La casa por dentro era aún más elegante que por fuera, la mueblería tenía detalles en oro, al igual que las manillas de las puertas. Los pasillos eran de dos metros y la altura del techo de aproximadamente tres. La casa tenía un aspecto católico antiguo, lo cual se le hacía súper irónico a Isabella ya que su familia era de todo, menos católica.
Isabella subió las grandes escaleras lentamente, analizando todo con detalle. Aquellas escaleras tenían como dos metros de ancho y eran casi infinitas.
—Ni se te ocurra mirar aquella habitación, es la mía —Candace rodó los ojos, pasando por su lado y golpeándole ligeramente el hombro.
—¿Perdón? —Bella la miró con una ceja alzada, ya cansada de la actitud de Candace— Acabamos de llegar y ya te has adueñado de una habitación.
—Es la mía, lo siento.
—¡Yo igual la quiero! —protestó. 
—¿Qué sucede aquí? 
La espalda de Isabella se puso recta por inercia. Candace sonrió y miró a su padre con simpatía.
—Bella se quiere adueñar de mi habitación.

—Igual quiero elegir, papá —protestó, girándose levemente.
—Tú habitación es la del fondo, Isabella —Artur Romanov la tomó del brazo con brusquedad—. Toma tus maletas y vete a cambiar la ropa, es el aniversario del club de yates y no quiero llegar tarde a mi bienvenida por tu culpa.
Isabella soltó un quejido y alejó bruscamente a su padre, mirándolo mal. Artur Romanov la miró con furia por última vez antes de marcharse por los pasillos.
—Ojalá no te pongas uno de esos vestidos de ramera que sueles usar, estoy cansada de que nos dejes en vergüenza —Candace la miró de pies a cabeza con desprecio, antes de encerrarse en su nueva habitación.
Isabella soltó un suspiro y miró la última puerta del pasillo, lo bueno era que estaría bastante alejada de sus padres y de Candace, tanto como para gritar y romper cosas sin que nadie la escuchara.
Entró a la habitación, sorprendiéndose por lo amplia y clara que era, tenía un estilo colonial y un gran balcón por el cual en seguida planeó escaparse algún día que estuviera aburrida.
Lo primero que hizo fue dejarse caer en la cama solo para chequear si era igual de cómoda que su antigua cama, y el gemido de placer que se escapó de su garganta fue la prueba suficiente.
Le hubiese gustado dormir una siesta toda la tarde ya que se encontraba bastante cansada por el viaje, pero no lo hizo. Sabía que tenían compromisos de familia y si no estaba lista en diez minutos, su padre la castigaría.
Se despojó de toda su ropa y caminó desnuda por la habitación en busca de ropa interior, solo se puso unas bragas y luego un vestido negro floreado, no podía usar brasier ya que el vestido tenía un gran escote. Optó por unos tacones no tan altos y se maquilló con polvo y un brillo labial rosa. Cuando se encontró lista, tomó su bolso y metió su teléfono, y antes de salir finalmente, tomó una chaqueta de piel para abrigarse.
Irina, su madre, la cual era igual de hermosa que Isabella, se encontraba usando un vestido parecido. Su padre iba con un traje negro. Y Candace iba vestida con un vestido color coral.
Su padre soltó un gran suspiro cuando la vio llegar a la sala. No le gustaba que Isabella usara aquellos vestidos tan cortos. Irina le regalo una sonrisa y Candace ni siquiera la miró.
—Espero que se comporten, no se hagan amigas de nadie y tampoco hablen con desconocidos. Les presentaré a mi amigo Jeremy y a su familia y espero que sean educadas, sobre todo tú Isabella —Artur miró a su hija menor con fastidio.
Isabella ni siquiera lo miró, solo asintió levemente mientras arreglaba su cabello. Irina hizo una mueca y acarició brevemente la mejilla de Isabella. 
Su madre era la única persona con la cual ella podía sentir afecto.
El camino al club yates se hizo algo largo por el tráfico, pero en cuanto llegaron, todo cansancio se esfumó. La gente iba muy animada, luciendo trajes caros y riéndose en voz alta, el ambiente era motivado y eso contagiaba.
Dos guardaespaldas esperaban por Artur Romanov y su familia, por lo que lo ayudaron a bajar a él y a sus damas de compañía del auto.
—Recuerden lo que les dije —musitó en voz baja, con sus ojos verdes inspeccionando el lugar mientras tomaba la mano de Irina.
Isabella ni siquiera le prestó atención. Estaba demasiada embobada con la costa y varios chicos guapos que se paseaban frente a ella.
El aniversario se llevaba acabo en uno de los yates más grandes, que pertenecía a la familia Bieber. Este estaba conectado con el muelle y por ahí las personas podían obtener acceso a la fiesta.
Artur caminó por el muelle con seriedad, tomado de la mano de su esposa y con sus dos hijas a la cola. Varias personas lo miraban con aparente respeto, muchos sabían de quien se trataba y por esa razón le tenían respeto, además de temor.

—Tengo que ir a resolver unos asuntos, no se alejen —musitó para sus dos hijas, antes de desaparecer con su esposa.
Aquella frase fue el pase a la libertad de Isabella. No tomó en cuenta a Candice y comenzó a caminar por todo el yate, mirando con curiosidad todo lugar; la gente millonaria que se paseaba de un lado a otro y algunos traficantes encubiertos que se encontraban haciendo negocios, pero que Isabella podía descubrir perfectamente gracias a su gran capacidad auditiva.

Jordan rió con fuerza y golpeó el brazo de Will con brusquedad.
—Págame, imbécil —le tendió la mano a su hermano pequeño.
—Justin igual perdió —tiró las fichas sobre la mesa.
Justin rodó los ojos y sacó el fajo de dinero para entregárselo a su hermano mayor. Jordan sonrió, recibiendo el dinero y luego volvió a mirar a Will. El pequeño Will, al notar que no tenía escapatoria, soltó un suspiro y tomó dinero de su billetera para entregárselo a Jordan.
—Nunca más apuesto con ustedes —miró a Jordan y a Justin con fastidio.
—Mejor búscate una vagina para que aprendas a ser hombre —Justin se bebió su vaso de whisky de un sorbo.
—¡Dios mío! Vieron a ese monumento —Román Ferraud llegó hasta sus amigos con un cigarro en la mano.
—¿Vagina? —Jordan en seguida se enderezó.
—¿Tan desesperados están? —Justin miró a su hermano mayor y a su mejor amigo con fastidio— Son tan imbéciles.
—Mira aquella rubia que está en la entrada, parece una perra adinerada y fácil —Román sonrió, dándole una calada a su cigarro mientras sus ojos marrones se pasaban por el cuerpo de la rubia.
—No sé cuándo aprenderán a respetar a la mujeres, trío de imbeciles —Will se levantó de la mesa, solo para pedir otra botella de agua mineral.
Jordan, Justin y Román rieron con fuerza.
—No es de mi gusto, pero esta buena —Justin igual llevó sus ojos hasta la rubia—. Will cada vez es más maricón, no me sorprendería encontrarlo con un hombre.
—Que asco —Jordan hizo una mueca.
—Que respete a las mujeres no significa que sea un maricón —Will miró dolido a sus dos hermanos, e ignoró por completo a Román, jamás le había caído bien.
—Oh, miren esa morena que llegó, ¿será su amiga? —Román exclamó.
Nuevamente, los muchachos volvieron a mirar en dirección a la rubia, esta vez encontrándose con una morena en su compañía.
—Parece menor —Jordan elevó una ceja—, pero efectivamente está más que buena.
Justin no dijo nada. Se quedó en silencio, mirando algo embobado las curvas de la morena que le hacía compañía a la rubia. Ahora podía decir que Román tenía razón, ya que anteriormente la rubia no le había causado tanto impacto.
—Definitivamente —murmuró, expulsado el humo del cigarrillo que acababa de encender—, y sí, se ve algo menor, pero no demasiado —sus ojos se quedaron pegados en el trasero de la chica.
—Quizás haga un trío esta noche —Román elevó ambas cejas, sin dejar de mirar a ambas chicas.
Jordan rió, Will rodó los ojos y Justin lo ignoró.
—Tengo cosas más importantes que hacer esta noche, luego de las doce pensaré en follarme a alguna fácil de las que andan por aquí —Justin de elevó de hombros—; pero sí, mira ese trasero —murmuró, aún mirando a la chica de piel bronceada.

—¿Qué cosas importantes? —finalmente Román dejó de prestarle atención a ambas chicas y miró a Justin con atención.
—Artur Romanov ha llegado a la ciudad —Jordan respondió por Justin—. Y mi padre quiere que nos presentemos. Son amigos —se elevó de hombros.
—¿Artur Romanov? —tosió levemente, algo asombrado.
—Sí —Justin asintió—, el segundo más poderoso en el negocio europeo, después de mi padre, claro —sonrió con algo de arrogancia.
—¿Por qué ha llegado a Francia? —Román inquirió con bastante interés.
—Le han destrozado sus negocios en Inglaterra y recibió varias amenazas en Rusia —Will se metió en la conversación, el era experto es historias—. Se vino a Francia porque cree que podrá encontrar a quienes tratan de matarlo y podrá manejar mejor los negocios desde acá. Sabes que este país tiene una doble cara bastante grande —concluyó.
—Interesante —Román respondió, bastante pensativo.
—Además mi padre es socio en bastantes negociosos con él, están importando desde America del  norte una nueva droga —Jordan bebió de su vaso con whisky.
—Sí, mi padre me comentó algo de eso —Román elevó ambas cejas.
El padre de Román también era un narcotraficante muy buscado; pero en Europa los Ferraud  no eran tan importantes como los Bieber y Romanov.
—El negocio crece y los riesgos también —Will hizo una mueca.
—El hecho de estar metidos en este mundo es complicado. Sabes que puedes morir en cualquier momento —Justin botó el humo del cigarrillo sobre el rostro de Will apropósito—, como también sabes que una vez que entras, jamás sales.
La relación entre los tres hermanos era genial, y aunque fuera difícil de creer, Justin era más cercano a Will que a Jordan. Era irónico ya que Jordan y Justin siempre molestaban al pequeño Will, pero en el fondo de todo, Justin y Will eran más unidos y se tenían mucha más confianza. Quizás porque tenían solo cuatro años de diferencia, Justin veintidós y Will dieciocho, en cambio Jordan tenía un humor mucho más negro y era el mayor, tenía treinta años.
—Iré por tequila, ya me hartó esta mierda —Justin suspiró, prácticamente tirando el vaso con whisky sobre la mesa.
Dejó a sus amigos en la zona en donde se encontraban bebiendo y caminó entre la gente hacia el bar. Justin odiaba estar rodeado de tanta gente, exactamente porque había crecido rodeado de gente falsa que solo quería dañarlo a él y a su familia, pero en esos momentos se sentía bastante cómodo, quizás porque consideraba que los socios del club de yate era gente más real y sana, a excepción de algunos que igual estaban metidos en el negocio.
Llegó hasta la barra y el barman lo reconoció en seguida, por lo que no dudó en atenderlo primero que a los demás.
—¿Algo en especial, Justin? —le sonrió.
—Dame tequila, el más fuerte que tengas —asintió.
Alex, el Barman, no tardó en asentir y comenzar a preparar los mini vasitos.
Justin sintió una mirada clavada en él, tenía una muy buen instinto y una vista periférica bastante entrenada. Digamos que su padre le enseñó a usarla a los quince años, cuando le enseñó a disparar. Una chica delgada, de cabello castaño lo miraba fijamente.
Justin se giró encontrándose con los ojos marrones de la muchacha y sonrió levemente, tenía que admitir que estaba bastante buena. La miró de pies a cabeza descaradamente y le guiñó el ojo.
La castaña se sonrojó levemente y puso su espalda recta, sacando más busto, dejándole más que clara lo dispuesta que estaba a él.
Justin soltó un gran suspiro, dándose cuenta lo necesitado que estaba. Hace más de dos semanas que no estaba con ninguna mujer y esa castaña de allí lo estaba tentando. Estuvo dispuesto a hablarle para llevársela a alguna habitación del yate y follarla rápido, pero una voz suave y exigente llamó su atención.',9)
INSERT INTO capitulo(titulo, contenido, id_historia)
VALUES('1.Delacroix y su prometida','Había entrado a trabajar hace dos años atrás a las empresas Delacroix, una de las empresas más destacadas internacionalmente en uno de los rubros más complicados a nivel mundial, según yo, trabajo en un compañía que estaba estrechamente relacionada con el Arte y la Cultura, existían departamentos de Arqueología, Literatura, Arte, Música y era la más exitosa del mundo.
Y yo trabajaba, nada más ni nada menos, como la secretaria del máximo jefe, Lucian jodido odioso Delacroix.
Disculpen. Para Lucian Delacroix.
Como no terminé la universidad por la enfermedad de mi hijo, tuve que buscar trabajo rápidamente y este puesto, pagaba, excesivamente bien, algo que me convenía mucho.
Tuve suerte. Tuve malditamente suerte de que me haya escogido por encima de todas esas barbies de plástico que vinieron buscando suerte... con el jefe, claro está.
Suspiré al terminar de anotar cada cita que tenía  Delacroix para hoy.
Marqué el número de su oficina.
Contestó al primer tono.
- ¿Señor Delacroix?- Dije por el teléfono.
- Dígame señorita Ward- Respondió con su ronca y fría voz.
- El Señor Williams canceló su cita de hoy para el jueves- Suspiró audiblemente- Y su agenda de hoy está lista- Dije buscando la página de hoy.
- A mi despacho- Con un suspiro, me levanto y camino a su oficina.
Golpeé la puerta color caoba y entré al oír un "Adelante". Estaba sentado en su silla observando la ciudad de Nueva York, mientras fruncía el ceño.
Cerré la puerta y se giró a mirarme.
- Tome asiento- Me senté en la silla que había delante de él, mientras dejaba mi agenda y el lápiz sobre la mesa, dispuesta a anotar todo lo que me dijera.-¿Cuándo llamó John?
- Hace menos de cinco minutos, señor.- Comenté mientras lo miraba fruncir aún más el ceño.- Me pidió que lo disculpara, dijo que un asunto sobre la salud de su esposa lo llevó a cancelarla.- Su mirada no demostró sentimiento alguno.
- Reagendala para el lunes de la próxima semana a las 8 de la mañanan y llámalo para avisarle.
Fruncí el ceño: -Sí, señor.- Anoté sus indicaciones.- Su madre ha llamado para recordar la cena de esta noche.- Asintió.- Dijo y cito: "No quiero ver a Rachel aquí".- Rodó los ojos; todos sabíamos lo mal que se llevaban la familia de mi jefe con su prometida.- También llamó la Señorita Rachel para recordarle sobre la degustación de la comida y dijo, nuevamente cito: "Que conteste sus llamadas o ella vendrá y no será nada agradable tenerla enojada en su lugar de trabajo, la degustación será a las 1 pm y debe ser puntual".- Restregó sus ojos y suspiró, después de todo seguía siendo un ser humano que se agotaba con los comentarios de desaprobación de su familia para con su novia y de su prometida que era una mujer caprichosa y chillona.- También llamó el señor Thomas.- Me miró serio, después de todo es su mayor accionista.- Quiere una reunión privada para hablar sobre la exposición de Italia, debe ser lo antes posible, preferiblemente, hoy.- La exposición de Italia sería la más importante del año y todo respecto a ella era prioridad.- También tiene 2 reuniones con el despacho de Finanzas y Contabilidad, una con el consejo de Creatividad y Desarrollo, y la última reunión del mes con el Comité de Accionistas después de la hora de almuerzo.- Cerré mi agenda.- Eso es todo por hoy, ¿Necesita algo?- Pregunté para poder marcharme a terminar de ordenar unos documentos sobre las 12 últimas exposiciones que se han llevado acabo éstas dos últimas semanas, para así poder presentarlas en la última reunión del Comité de este mes; que sería en 4 horas. 

- Cancela todas las reuniones de hoy.- Me miró por un segundo, sabía lo que se avecinaba.- Y llama a mi prometida para avisar que no podré asistir, luego de haber solucionado eso  llama a Thomas para agendar un almuerzo con él a las 1 pm.
Otra vez tengo que soportar los berrinches de su prometida, siempre era yo que lidiaba con su enfado hacia su novio.
- Si, señor.- Tomé mi agenda, me levanté.- Con su permiso.- Asintió y siguió trabajando en su computadora.
Al salir, suspiré y me armé de valor para hablar con la Bruja.
Al tercer tono, contestó.
- ¿Y bien?- Contestó la señorita Rachel.
- El señor me pidió que lo disculpara, ya que no podrá asistir a la degustación por asuntos de una exposición.- Dije rápidamente. Un chillido de impotencia se escucha por el otro lado, alejé un poco el teléfono de mi oído al oírla gritar como loca, ya que temí por mi pobre y torturado oído.
- Ése imbécil, ése desgraciado piensa que puede Hum...- Y dejé de escuchar sus insultos al señor porque su voz chillona era insoportable.- Y dile que no saldrá tan fácil de esto.- Y colgó.
Lo siguiente que hice fue redactar un correo electrónico para los jefes del Comité, disculpándome por Delacroix y avisando que la reuniones se llevarían acabo el jueves que viene a la misma hora, luego llamé al señor Thomas para indicar lo acordado con mi jefe.
Saqué todos los documentos, desde las críticas hasta las fallas que habían tenido las 12 últimas exposiciones, redacté unos cuantos fundamentos para aclarar cada punto de fallo, también ordené la revisión de finanzas por orden de prioridades y alfabéticamente para que Delacroix pudiera firmar cada documento lo antes posible.
30 minutos después de arduo trabajo, estaba tomándome un dulce y delicioso capuchino.
Miré hacia el ascensor que  había sonado, del ascensor salió una pelirroja, modelo de grandes piernas, dejándolas expuestas por ese llamativo y corto vestido azul eléctrico y labios demasiados rojos para mi gusto, sus ojos iban delineados con negro para resaltar sus azules ojos, me miró alzando su perfecta ceja roja.
- ¿Dónde está?- Dijo Rachel Moore- ¿Qué no me oíste?- Alzó un poco la voz.
Sonreí falsamente: - El señor Delacroix está en una junta importante a través de una vídeo llamada.- Dije serena.
- ¡Y una mierda!- Gritó para después dirigirse hacia su despacho, trató de abrir la puerta una 5 veces, mordí mi labio para evitar reír por el ridículo que estaba haciendo justo ahora. Me miró.- ¿Te parece divertido?- Negué.- Entonces, ¿Por qué mierda no vas a conseguir una llave para abrir la maldita puerta?- Siseo.
- Lo lamento, señorita, pero el señor dijo que no lo molestaran.- Me encogí de hombros y antes de que replicara, añadí:- Y lamento informarle, que las dos únicas personas que tienen la llave de su oficina son él y el conserje quien no llega después de las 6 pm.- Informé.- ¿Desea algo de beber, mientras espera al señor?- Pregunté dulcemente.
- Eres una...- Chilló y comenzó a golpear desesperadamente la puerta de mi jefe.- ¡Lucian! ¡Abre la maldita puerta! - Exclamó.- O te juro que te arrepentirás.- Negué y suspiré.
La señorita siguió gritando que le abriera la puerta mientras lo insultaba y aprovechaba de insultarme a mí por ser una incompetente y buena para nada. Rodé los ojos. 

La puerta se abrió y dejó ver a un muy molesto hombre.
Lo primero que hizo la señorita fue estampar una bofetada en su mejilla, haciendo eco por las paredes del piso que compartíamos mi jefe y yo, su rostro se volvió aún más enfurecido.
- ¿Cómo te atreves, hijo de puta, a dejarme malditamente plantada?- Dijo ella dándole otra bofetada en la otra mejilla.- ¿Cómo en el infierno me avisas por intermedio de ésa puta que tienes por secretaria? ¿Cómo jodi...?- No terminó porque el grito del señor Lucian la hizo callar.
- ¡Ya para de una vez!- Gritó furioso.- ¡Y no permito que hables así, de ésa manera tan vulgar, ni a mí ni a la señorita Elizabeth!- Dijo apuntando hacia mi dirección. - ¿¡Estás demente!?- Preguntó mientras la miraba con repulsión.- ¿Cómo te atreves a hacer un escándalo de ésa magnitud, sabiendo que mis negocios son importantes?- Preguntó, ella bajó la mirada, avergonzada.- No me digas que te comieron la lengua los ratones después de semejante show, ¡contesta!- Ella se sobresaltó y yo, por una milésima de segundo, sentí lástima de ella, cosa que olvidé al recordar todos sus malos tratos hacia mi persona.
- Yo... yo...
- ¿Tú qué?
- Lo lamento.
Río incrédulo: - ¿Lo lamentas? ¿En serio, sólo eso, ninguna excusa para defenderte, sólo eso?- Ella miró a otro lado.- Y una mierda,- Lo miró.- Última vez que haces algo como esto, porque te juro que soy capaz de romper este compromiso, ¿Entendiste?- Asintió.- Bien, porque ahora volverás a donde sea que estabas antes y me dejarás terminar mi trabajo en paz, ¿Comprendiste?- Asintió nuevamente para luego salir de ahí.
Es una mujer que manipula y dejan que la utilicen como quieran. Pobre.
- ¿Terminaste los documentos de las exposiciones o simplemente te quedaste a observar este espectáculo como la metiche que eres?- Me tensé al escuchar su tono hacia mí.
- Señor, le pido por favor, que no me hable así, no soy su prometida para que venga y me trate como se le antoje.- Dije tomando los documentos. Sus cejas se alzaron.- Segundo, no es mi culpa que haya armado semejante espectáculo.- Me levanté y caminé hacia él quedando a unos centímetros de distancia, me miró hacia abajo.- Y tercero, claro que terminé con mi trabajo, no soy una incompetente como me lo ha repetido hasta el cansancio su prometida.- Levanté las carpetas y las deposité en sus manos con fuerza.- Y si me disculpa, tengo mejores cosas que hacer como para quedarme a escuchar cómo me habla y trata, con su permiso.- Me di media vuelta, tomé mi bolso y salí de allí a almorzar.
Pulse el botón del ascensor, hasta ahí quedó mi salida triunfal. Bufé y me crucé de brazos mientras esperaba.
Estaba lo suficientemente cabreada como para dejar que me tratara de ésa forma, allá él si decide despedirme, estaba harta de los malos tratos que recibía por parte de su prometida como para soportarlo ahora a él.
- Señorita Elizabeth.- Llamó mi jefe, me giré, estaba cruzado de brazos con las carpetas en mano y con una leve sonrisa.- Hoy irá al almorzar conmigo y Thomas, así que, espereme abajo para que vayamos juntos.- Dicho esto, entró a su oficina y yo al elevador.
Suspiré y ahora debía soportarlo a él durante el almuerzo también.
Al salir del elevador y me dispuse a esperar a mi jefe mientras hablaba con el portero.
Don Tito, un viejito de origen chileno que vino en busca de nuevas oportunidades ya hace 20 años.
- ¿Qué tal?- Hablé en español, practicando el idioma. Sonreí.
- Buenas tardes, muchacha.- Me sonrío mientras tomaba mi mano y la besaba. Sonreí.
Es aquí donde estuvimos hablando 20 minutos sobre su esposa que deseaba que la visitaramos para cenar y sobre el pequeño Eliot y su nueva manía de correr desnudo por la casa.',10)
INSERT INTO capitulo(titulo, contenido, id_historia)
VALUES('La práctica hace al maestro ','
Años después. 
Meto mis manos en el río y mi cuerpo entero se estremece, está fría.
Creo que necesita calentarse un poco
Muevo mis dedos probando y sintiendo la energía del agua, mis uñas largas y negra resaltan entre el agua cristalina, pura. Ese color negro me marca. Bruja. Las brujas nacen con las uñas de las manos y pies del brillante color de la noche.
Sonrío cerrando mis ojos, concentrándome al máximo.
Tomo una respiración profunda, siento la magia en mis venas dejando rastros chispeantes por mi cuerpo. Concentro esa agradable y conocida sensación en mis manos.
Fuego. Calor. Fuego. Calor.
Imagino el abrumador calor saliendo de mis manos y calentando el agua haciéndola...burbujear.
Abro los ojos para poder admirar mi resultado. El agua hierve frente a mí a causa de mis manos. Las saco del agua lentamente y el calor se va, necesito practicar más. No es fácil extraer calor en el agua helada que baja de las montañas.
El sonido de pasos me alerta de la llegada de alguien. Me levanto y me alejo de la orilla del río, sujeto la tela de mi larga y voluptuosa falda para poder subir la pequeña colina sin problemas. 
Cuando llego arriba veo a un hombre de cabello rubio medianamente largo.
A la luz del sol sus colmillos brillan cuando me sonríe, se acerca.
— Tardaste una eternidad —me quejo cruzando mis brazos, sus ojos de color cielo giran—, ¿es que acaso ya no quieres ser amigo de la bruja? —enarco mi ceja quedándome a un paso de distancia.
Me doy cuenta que su pantalón está manchado de lodo hasta las rodillas, su camisa está mal acomodada y gotas de sudor bajan por su rostro haciéndolo brillar. Eso no es algo habitual de él.
— ¿Sucede algo, Abel? —termino el espacio y lo abrazo, cuando lo toco siento su cansancio. Sin embargo, Abel niega suspirando.
— Tuve que cuidar a mi padre unas horas hasta que llegara mi madre, luego Joshua me llamó y tuve que transformarme, llegué a mi casa y no tenía ropa limpia... —me devuelve el abrazo.
— Mi pobre Abel, tuviste un día agitado —me alejo y golpeo su hombro con cariño—. Te aseguraste de que nadie te siguiera ¿cierto?
Abel sonríe, esta vez con complicidad.
— ¿Cuántos años llevamos haciéndolo? —pregunta.
— Cuatro años.
— ¿Alguna vez me han atrapado? —niego blanqueando mis ojos—, cada vez lo voy perfeccionado, como lo haces tú con tu magia —mueve los dedos de sus manos cerca de mi rostro y lo aparto de un manotazo.
Sabe perfectamente que odio eso.
— Cállate —reímos, saber que este secreto nos pertenece se siente bien.
— Caminemos, Maureen —Abel me ofrece su brazo y no dudo en tomarlo.
Aunque me desagrade el olor a lobo, ya me he acostumbrado al de Abel.
Caminamos por el borde de la colina, siguiendo el rio. Hace ya varios años esta se convirtió en nuestra rutina, caminar y conversar, fuera de la vista de todos, porque un lobo como él no debería ser el mejor amigo de una bruja como yo.  
Abel es lo único bueno que he conseguido de este lugar, lo conocí en la escuela, él me defendió de muchos otros lobos jóvenes que detestan a las brujas, todos parecían hacerlo, menos él. Me percaté de que eso estaba llamando la atención y que era mal visto, por lo cual decidí dejar la escuela y estudiar en casa. Pero el dejar de vernos no funciono para nosotros, simplemente no podíamos estar alejados. Tuvimos que buscar otra manera de seguir juntos y el rio fue nuestro lugar designado para vernos. Desde entonces nos vemos aquí, está cerca de mi casa y es por eso que nadie viene aquí, nosotras somos la razón.

— ¿Cómo está tu padre? 
Recuerdo a Claus como el hombre que nos ayudó a ver al alfa cuando llegamos a estas tierras.
— Enfermo, creo que no mejorara —presiono su brazo—, no quiero hablar de eso, Maureen.
— Te entiendo —él toma este tiempo como una escapada de todos sus problemas, y quiero que consiga relajarse—. Entonces, ¿qué se siente ser el próximo beta de la manada?
— Creí que no te interesaban estas cosas —gira su cabeza para verme de manera traviesa.
— Tengo que conocer al enemigo, Abel.
Saca su lengua en un gesto infantil, yo correspondo de manera madura imitándolo.
— Absolutamente, debí haber visto venir eso...—suspira—. Pues, no se siente tan mal, quizás cuando esté en ese rango logre cambiar la opinión de nuestra manada acerca de ustedes —chasqueo la lengua, aunque mi corazón le agradece—, ¡Oh, vamos! Sabes bien que Josh no te odia, él dentro de poco también se convertirá en el nuevo alfa.
Muy pronto habría una gran celebración en el pueblo, nosotras tendremos que asistir por obligación, si es que no nos mandan una carta pidiéndonos que no asistamos.
— Eso es imposible —refuto—. Los lobos nos odian, y sinceramente, el sentimiento es mutuo.
— Auch, ¿acaso fue eso una indirecta? —lo empujo fuerte.
— Tal vez —subo y bajo mis cejas con diversión.
Me adelanto unos pasos y disfruto de la sensación del césped en mis pies, adoro andar descalza, sin nada encerrando mis pies. Muevo mis piernas exageradamente haciendo que mi tobillera llena de piedras de colores tintinee.
— Ay, Maureen —giro mi rostro y me encuentro con la mirada reprobatoria de Abel—, ¿no tienes vestidos decentes?
Decente.
Solo porque use una falda ligera –aunque voluptuosa- no significa que sea una ofrecida, esta vestimenta es más práctica para las personas como yo, los brujos del bosque. Pero ellos no entienden, porque todo lo que hacemos está mal, nuestra manera de vestir es inapropiada y sugiere que estamos ofreciendo otro tipo de servicios. También que queremos el alma de los hombres para una poción de juventud y toda esa basura.
En el pueblo, las mujeres visten vestidos largos, elegantes y calurosos. No sé cuántas capas de ropa llevan encima, sin contar el pesado armador y el molesto corsé. Nosotras no vestimos como ellas, y no quiero hacerlo.
No me imagino intentando trepar un árbol con uno de esos vestidos.
— ¿Crees que me quiero robar tu honor y tu alma? —camino hacia él juguetonamente—. Créeme que si la quisiera, ya la tendría.
Abel rompe en risas al igual que yo.
Nos conoce bien, sabe que no somos como todos dicen. 
— Tonta —sujeta mi mano y seguimos caminando. Siento su cambio de ánimo antes de que hable nuevamente—. Sé que esto está fuera de lugar pero... ¿tú crees que en la manada esté mi mitad?
Otra vez con lo mismo.
— Ya sabes lo que pienso de eso, Abel.
Él se detiene y me hace sentar en un tronco caído, se sienta a mi lado. Puedo sentir la humedad traspasarse por mi falda. Huele a lluvia.
La puedo sentir.
— Solo piensas eso porque no puedes sentirlo, Mau —me mira con intensidad. 

Es un tema delicado para ellos, y yo por lo general no puedo ser cuidadosa o piadosa cuando se trata de temas de lobos.
— Exacto —afirmo—, no puedo sentirlo —pero siento mucho más—, no podré percibir el olor dulzón que se supone que tiene tu mitad... Lo único que puedo percibir es la pestilencia a lobo —me encojo de hombros haciéndolo reír—. El destino no me empujará bruscamente hacia alguien que cuando mire a los ojos, sepa que me fui directamente a la mier... —Abel se apresura a poner una mano sobre mi boca para callarme.
— Mujer grosera —gruñe—.No hay quien te haga cambiar de opinión ¿Cierto? —retira su mano.
— Rotundamente no —le sonrío, para recompensarlo por mi brusco comentario, pongo mi mano en su hombro—. Y tú no deberías preocuparte por eso, ya llegara la chica para ti. Este corazón precioso —palmeo su pecho—, tendrá un compañero adecuado.
Abel se sonroja y mira donde mi mano toca.
— Eso espero, porque si no sucede tendré que quedarme con la bruja —susurra.
Pongo una mano en mi pecho fingiendo estar ofendida.
— ¿Qué te hace pensar que te aceptaría? Perdón pero no salgo con perros.
Ambos reímos y decidimos que es hora de ir a mi casa, Abel quiere saludar a mi abuela y madre, quienes lo quieren como si fuera de la familia.
En el camino le muestro en que he estado practicando, manejo el aire a nuestro alrededor y el fuego en mis manos, incluso lo salpico cuando provoco el agua. La pequeña y destartalada cabaña aparece en nuestro campo de visión, la parte trasera está llena de flores que nosotras misma cultivamos, sirven para diferentes tipos de cosas y combinadas pueden llegar a ser una poción perfecta. También tenemos dos postes con una larga cuerda para colgar nuestra ropa después de lavarla. Allí esta mamá, controlando el viento a su favor para secar la ropa más rápido. Ella lleva puesto un vestido ligero que siempre hace sonrojar a Abel, esta vez no es la excepción.
Mi madre al sentirnos se gira hacia nosotros.
— ¡Muchachito Abel! —el viento se detiene bruscamente—, que gusto que nos visites.
Nos acercamos a mi amada madre, ella abraza a mi amigo fuertemente, luego me mira a mí.
— Maureen, no deberías andar vagando por allí, tienes trabajo que hacer con tu abuela —me riñe.
— Es un placer verla, señora María —saluda Abel avergonzado separándose de ella.
No es propio de un caballero ser tan libertino con una dama. 
Recuerdo la frase que muchas veces escuché en la escuela, en el pueblo y de él.
— Iré a ver a la abuela, vamos, Abel —muevo mi mano despidiéndome de mi madre.
Tenemos que rodear la cabaña ya que no tenemos puerta trasera. Nuestro porche y el resto de nuestra casa es acogedora y pequeña. Tras la puerta principal esta una mini-tienda improvisada, del techo cuelgan alguna de nuestras flores y plantas que mi abuela trajo del pueblo vecino. También tenemos un pequeño estante con botellas y pequeños frasco de diferentes nombres y cada uno para una función particular, al frente hay un mostrador con más botellas de vidrio, detrás de este está la mesa donde preparamos las pociones y por supuesto, una cortina de cuencas moradas que cubren la entrada al pasillo.',11)
INSERT INTO capitulo(titulo, contenido, id_historia)
VALUES('«1»','Mi cabeza me dolía como los mil demonios y al abrir mis ojos, el sol me golpeó directo haciendo que el dolor fuera un infierno.
— ¡Diablos! — me queje y lleve mis manos a mi cabeza en busca de consolar el terrible dolor que estaba sintiendo.
Me senté en mi cama mientras miraba un punto fijo. ¿Qué había sucedido la noche anterior? Nuevamente lleve una de mis manos a mi sien y suspiré bajando la cabeza. Sin embargo, los recuerdos llegaron a mi como una oleada.
Como pude me levanté y salí de mi habitación corriendo por los pasillos desiertos en busca de mis padres.
— ¡Mamá, papá! — gritaba una y otra vez. Seguí corriendo y abrí la puerta del despacho de mi padre, pero me llevé a alguien puesto, más no me fije quién podría ser. Mi atención estaba en mi padre que se levantó de su asiento para acercarse a mí.
— ¡Jessica! — la voz de mi padre sonó preocupada.
— ¿Jessy?
Gire mi cabeza y lo vi. Se hallaba a un costado de la puerta con un plumero en su mano y un paño en la otra.
Retrocedí por instinto y me dejé caer en la entrada. Mis ojos no se despegaron de los suyos. Sentía que estaba disfrutando verme en ese estado, mofándose de mí, de mi vulnerabilidad ante él.
-—¿Jess? —mi padre se arrodilló y su expresión denotaba preocupación. Tocó mi frente y habló. - Aún estás hirviendo. La fiebre aún no ha pasado. Es peligroso que andes por la casa así.
— ¿Fiebre? -—pregunté confundida a lo que mi padre asintió.
— Así es. Toda la noche estuviste volando de fiebre. Tu madre estuvo velando por tí y JungKook se encargó de que la fiebre bajara.
— N— no... Yo... Anoche yo...
— Anoche estabas bajando las escaleras cuando un mareo te atacó y caíste del los últimos dos escalones.
Estaba segura que mi rostro era un poema, ¿Quién podría haberle dicho algo como ello? Yo… Yo recordaba todo lo que sucedió, JungKook, él…Inmediatamente puse mis ojos sobre él y ni siquiera se inmutó, permanecía callado con un semblante serio—como siempre acostumbraba a tenerlo— moví mis labios pero ningún sonido salió de mi garganta, me sentía estupefacta.
—Pero pa… — Quise acotar algo, pero mi padre negó con una pequeña sonrisa mientras me  tendía su mano.
La tomé para dejarme levantar junto con él, lo mire y pude notar la preocupación aflorar de cada parte de su cuerpo, sabía que se preocupaba por mí y que yo esté en un estado de enfermedad, podría ponerlo con sus pelos de punta.
—Sera mejor que descanses aún más, Jess. Deja que JungKook te lleve hasta tu habitación, no quiero que vuelvas a salir así. ¿Entendido? — Musitó mientras acariciaba mi mejilla dulcemente.
—Puedo ir yo sola, papá. No neces— Pero fue interrumpida nuevamente y eso estaba molestándome en demasía y aún quien era el causante de aquella interrupción tan innecesaria.
—Deja que te lleve, Jessica. Tus padres han estado demasiados preocupados y puedo entenderles, yo también lo he estado por ti — Acercándose a mi con aquel gran porte que tenía, murmuró para que solo nosotros tres pudiéramos escucharle.
Mi padre sonrió en conformidad, pero yo no. No podía creer en sus palabras, sabía que algo ocultaba y me sentía en total desventaja al tener a mis progenitores cegados por el gran perfecto e impecable Jeon JungKook.
Volví mi vista a quien velaba por mí todo el tiempo y pude descifrar una gota de súplica, no podía soportar aquella mirada, suspirando en descontento asentí a lo que JungKook  propuso.
—Esta bien— Acepte derrotada, otra sonrisa volvió a aparecer en el hombre que tenía frente a mí. — Adiós, papá — Me despedí sabiendo que no podría volver a salir del cuarto sin recibir alguna queja de su parte o de mi madre.
—Adiós Jess, si necesitas algo no dudes en pedirle a JungKook. Él estará a tu disposición — Apretando su hombro levemente, mi padre me dejó a merced de alguien que yo desconfiaba con mi vida entera.
« Ni de coña » Pensé para mis adentros.
—Claro que lo haré — Mentí descaradamente.
—Vamos Jessica — Su gruesa voz llamo a mi nombre y yo no pude evitar ponerme tensa, no me agradaba estar con su compañía.
Comencé a caminar con su presencia de mi lado derecho, sintiéndome cohibida y demasiado incómoda por el silencio tan abrumador que nos envolvía a ambos, ni siquiera me inmute en preguntar algo.
—¿Cómo te sientes? — Me queje para mis adentros, él debía preguntarlo por el simple protocolo que debe seguir. Debía ser amable con todos, pero a mí aquello no me iba ni por la justicia.
—Bien — Solo me limité a decir con pocas ganas. 
Otra vez el silencio incómodo.
—Pareces feliz de estar aquí — Comento sobre mis espaldas cuando yo entré a mi habitación soltando un suspiro de alivio, pareció adivinar mi actitud. Dándome vuelta, lo enfrente sin importarme como sus orbes azulados me observaban tan cautelosamente.
—Claro que no, solo deseo estar en mi alcoba y ya — Solté con una leve sonrisa—Una muy falsa— Puedes retirarte ya, JungKook.
—Acuestate y lo haré — Evite rodar los ojos al escuchar sus órdenes.
Pero no deseaba tener una discusión con él, así que sin mucho esfuerzo por mi parte, comencé a caminar lentamente hasta la gran cama que ocupaba mi espacio, levantando las cobijas anteriormente tiradas por mí, me adentre en ella.
—Ya estoy, ahora sí puedes retirarte— Comenté tratando de sonar amable.
Mirándome como comúnmente lo hacía, parecía estar observando cada movimiento de mi persona. Me hacía sentir tan desesperada que ansiaba gritarle en su cara que dejará de hacerlo. 
Eran tan penetrantes que podría sentir como traspasaban mi ser hasta llegar a mi alma, al lugar más incógnito que hasta yo no podría descubrirlo. 
Sin decir nada, sin mover si quiera un músculo de su rostro asintió yéndose del interior de mi habitación dejándome con un pequeño sabor en mi boca, sabía que escondía algo.
« Jeon JungKook, eres un ser que voy a descubrir»',12)
INSERT INTO capitulo(titulo, contenido, id_historia)
VALUES('l.','-Hola, soy Danielle ;)
-Hey, de donde te conozco?
-No me conoces, soy muy tímida como para hablarte en persona...
-Jajaja, venga! Que no muerdo ;)
Yo si, y tengo ganas de morderte desde hace tiempo...
- uwu 
-Vas a mi instituto?
-Si... :3
-Vas en prepa?
-Si, pero no te diré quien soy. :p
-Pues como no tienes foto de perfil tendré que investigar por tu nombre, Danielle...',13)
INSERT INTO capitulo(titulo, contenido, id_historia)
VALUES('1.','Busco en mi bolso los materiales que necesito tratando de que mis dedos no se vuelvan gelatina, tomo el lapicero en mi mano, pero este se cae, lo tomo de nuevo y se vuelve a caer, una tercera vez lo intento y esta vez logro tomarlo, mis dedos están resbaladizos, estoy transpirando, pero ¿cómo no hacerlo? Es imposible, ¿cómo no hacerlo, cuando el dios griego ordenó con voz firme y ronca que sacáramos nuestros cuadernos y lapiceros? Es imposible.
Tomo varias bocanadas de aire tratando de tranquilizarme pero no puedo, no soy capaz de hacerlo cuando siento su mirada sobre mi atravesándome, espera ¿su mirada? Levanto rápido la vista y miro en su dirección, efectivamente, esta mirándome pero casi al instante aleja la mirada de la mía, ahora si que estoy nervioso, estoy temblando, casi que parezco estar en un terremoto privado.
Pongo el cuaderno de apuntes sobre la mesa y a un lado los lapiceros, el dios griego está leyendo algo en un libro.
Un momento después levanta la mirada y se queda pensando un momento ¿en qué? Su mirada baja hasta mi pero sólo una milésima de segundo se conecta con la mía, de pronto me pican los labios, de pronto quiero abalanzarme sobre él.
Se voltea y se dirige hacia el tablero, comienza a copiar, mientras que él lo hace observo como sus músculos grandes se tensan en su camisa y se marcan deliciosamente, lamo mis labios, mientras todos copian lo que el escribe en el tablero yo lo miro e imagino muchos escenarios con él. Ahora, ni que decir del trasero que se le marca en los pantalones. ¡Madre de Dios!
Aun no puedo creer que haya cambiado tanto, recuerdo cuando era más joven y lo lindo que era, aunque ahora esta mucho mejor, aun recuerdo cuando hablábamos y cuando reíamos y jugábamos por horas, él es mayor que yo, pero eso no importaba, recuerdo como a veces rechazaba salir con sus amigos solo por jugar conmigo, pero todo eso cambio, él cambio, pero lo entiendo perdió a su familia, pero lo más doloroso es que perdió a su madre, y se como se siente eso ...
—¡Stilinski! —Un grito me saca de mis pensamientos.
Miro hacia todas las direcciones hasta que veo que es Derek quien me llama, trago saliva al ver su expresión sería, aun así estando enojado se ve malditamente caliente.
—Lo siento, me distraje un poco —Susurro quedamente.
—Si eso noté, después de que terminen las clases, ve a mi oficina —Me ordena el dios griego con voz firme pero sin gritar.
—Esta bien —Le digo en un tono casi sumiso, ¿cómo no contestarle de esta manera? Si habla con su voz grave y ronca, de seguro que si me pide que limpie la institución entera solo, lo haría sin titubear.
—Ahora, copia lo que hay en el tablero —Apunta con su dedo hacia atrás, yo asiento y comienzo a hacerlo.
¿Qué querrá en su oficina? -No lo que quieres, Stiles- Responde mi subconsciente, maldito.',14)
INSERT INTO capitulo(titulo, contenido, id_historia)
VALUES('1','Alicia abrió la boca y la cerró repetidamente totalmente aturdida.
—Aquí no hay nadie.—le respondió la otra voz femenina.—Debió ser tu imaginación tía Amanda.
La dama hizo un intento de sacar su cabeza para observar de quien se trataba, pero el caballero afianzó su agarre en su cintura evitando que se moviera. De espaldas a él, solo podía sentir el sonido de su respiración en la nuca.
—Mi imaginación o no, espero que no nos este avergonzando.
—Él jamás haría eso, lo conoces tía Amanda.
—¿Lo conozco?.—la voz de la mujer mayor estaba marcada por la incredulidad.—Conozco a mucha gente querida, pero él no es uno de ellos.
Alicia giró su rostro buscando la mirada del absurdo conde que los había obligado a esconderse, pero lo que halló en ella no le gusto para nada. Sus ojos verdes parecían querer derretir la estantería con solo mirarla y su cuerpo se aferraba al suyo como si de alguna forma ella le pudiera brindar estabilidad. 
La dama no pudo evitar sentirse incómoda y molesta por su cercanía. Primero le arruinaba su cita con su querido marqués y ahora los ponía en una situación comprometedora.
Si por alguna extraña razón aquellas damas chismosas se interesaban por la estantería de libros a sus espaldas. Ellos tendrían más de una explicación que dar.
¿Por qué un caballero la abrasaba de esa forma tan poca decorosa? o mucho mejor...¿Por qué estaban solos en esta habitación en lugar de hallarse en compañía de medio londres en el salón?
Afortunadamente las mujeres fueron cautas y desaparecieron tan rápido como llegaron. Cuando lo hicieron Alicia se zafó de su agarre conservando una prudente distancia con él.
—Se puede saber porqué hizo eso, milord.
—Solo precautele nuestra reputación, milady.
—¿Nuestra reputación?.—bufó cansina.—Ni siquiera estábamos haciendo nada malo, solo hablábamos. Al menos que...—achicó los ojos con indignación.—No quería que a usted, un caballero con una reputación intachable, lo encontraran con una descarriada como yo ¿verdad?
—Por supuesto que no.—gruñó.
—Entonces porqué nos escondimos.—increpó alzando la voz con más fuerza de la que pretendía.
—Tuve mis razones, milady.—el conde apretó los dientes en un gesto severo que pretendía decirle que se detuviera con las preguntas.
—Las escuchó.
Jonathan no respondió y Alicia sintió una pequeña punzada en el pecho que ella asoció a la molestia que sentía.
—Deberíamos volver al salón.—sugirió la dama sin dar lugar a réplica.
Ambos regresaron en completo silencio asegurándose de separarse para que nadie los asociará con una pareja. 
Es lo mejor, pensó Alicia convencida de que era preferible que el único rumor que circule de ella sean sus aventuras amorosas con Lord Grafton y no sus momentos a solas con el conde.
Ya empezaba a espantar un poco a las nobles más recatadas de Londres como para adjudicarse de a gratis el apodo de “ligera”. Ella no estaba interesada en probar los encantos de otros nobles, solo de su “presa” a la que cuidadosamente había elegido, luego de analizar con rigurosidad los estados de cuenta de varios nobles solteros que cumplían con sus exigentes requisitos. 
Entre los que figuraba ser agraciado, no sobrepasar los cuarenta años y tener una buena dosis de dinero que compartir con su amada esposa.
Lord Grafton cumplía con los requisitos y más, por eso era el candidato ideal para ella. Alicia nunca se iba a ciegas en un negocio y mucho menos en su matrimonio que definiría para siempre su vida, así que sin importar qué, conseguiría su objetivo',15)


---------------------
--Usuario escribe----

--autor-historia-fecha
INSERT INTO usuario_escribe VALUES(3, 1, GETDATE());
INSERT INTO usuario_escribe VALUES(8, 2, GETDATE());
INSERT INTO usuario_escribe VALUES(10, 3, GETDATE());
INSERT INTO usuario_escribe VALUES(13, 4, GETDATE());
INSERT INTO usuario_escribe VALUES(15, 5, GETDATE());

INSERT INTO usuario_escribe VALUES(17, 6, GETDATE());
INSERT INTO usuario_escribe VALUES(18, 7, GETDATE());
INSERT INTO usuario_escribe VALUES(19, 8, GETDATE());
INSERT INTO usuario_escribe VALUES(20, 9, GETDATE());
INSERT INTO usuario_escribe VALUES(13, 10, GETDATE());

INSERT INTO usuario_escribe VALUES(3, 11, GETDATE());
INSERT INTO usuario_escribe VALUES(10, 12, GETDATE());
INSERT INTO usuario_escribe VALUES(15, 13, GETDATE());
INSERT INTO usuario_escribe VALUES(17, 14, GETDATE());
INSERT INTO usuario_escribe VALUES(19, 15, GETDATE());


set dateformat ymd

-- INSERTAR DATOS PARA ADMIN_REVISION
INSERT INTO admin_revision VALUES(1 , 1, '2019-07-15', 2);
INSERT INTO admin_revision VALUES(1 , 2, '2019-07-01', 2);
INSERT INTO admin_revision VALUES(1, 4, '2019-07-16', 2);

--INSERT INTO admin_revision VALUES(1 , 3, '2019-07-11', 2);
--INSERT INTO admin_revision VALUES(1 , 4, '2019-07-14', 2);

-- PROC 3
-- PROCEDURE PARA PODER DAR REVISION A LAS HISTORIAS OBTENIENDO LA FECHE AUTOMATICAMENTE
-- DEL MOMENTO QUE SE HACE LA REVISION
CREATE PROC revision_historias
(
    @id_admin INT,
    @id_historia INT,
    @estatus INT
)
AS
BEGIN
    INSERT INTO admin_revision VALUES(@id_admin, @id_historia, GETDATE(), @estatus);
END

EXEC revision_historias 1, 3, 2;
EXEC revision_historias 1, 5, 2;
EXEC revision_historias 1, 6, 2;
EXEC revision_historias 1, 7, 2;
EXEC revision_historias 1, 8, 2;
EXEC revision_historias 1, 9, 2;
EXEC revision_historias 1, 10, 2;
EXEC revision_historias 1, 11, 2;
EXEC revision_historias 1, 12, 2;
EXEC revision_historias 1, 13, 2;
EXEC revision_historias 1, 14, 2;
EXEC revision_historias 1, 15, 2;

--------------------------------------
-- CALIFICACIONES DE LAS HISTORIAS ---
-- TOMANDO COMO CALIF MAX 5 Y MIN 1

INSERT INTO califica_historia VALUES(2, 1, 5);
INSERT INTO califica_historia VALUES(4, 2, 4);
INSERT INTO califica_historia VALUES(2, 3, 4);
INSERT INTO califica_historia VALUES(5, 4, 2);
INSERT INTO califica_historia VALUES(8, 2, 5);

INSERT INTO califica_historia VALUES(2, 4, 4);
INSERT INTO califica_historia VALUES(9, 2, 5);
INSERT INTO califica_historia VALUES(9, 4, 5);
INSERT INTO califica_historia VALUES(9, 5, 3);
INSERT INTO califica_historia VALUES(3, 9, 5);


-- VISTA 1 ---------------
-- para mostrar resumen de las ultimas 3 historias agregadas en la pagina principal
-- PARA CAMBIAR EL NUMERO DE VISTAS PREVIAS DEVUELTAS SOLO ES NECESARIO CAMBIAR EL TAMAÑO DEL TOP(TAMAÑO)
CREATE VIEW VIEW_PREW_LAST_STORIES
AS
	SELECT TOP(9) h.id_historia, H.titulo, H.portada_url, S.cabecera, S.contenido
	FROM historia H
	INNER JOIN sinopsis S ON H.id_sinopsis = S.id_sinopsis
	INNER JOIN admin_revision A ON A.id_historia = H.id_historia
	WHERE A.id_estatus = 2 --APROVADO
	ORDER BY A.fecha_aprovacion DESC;
GO

SELECT * FROM VIEW_PREW_LAST_STORIES

-- PROC 4 ---------------
CREATE PROC VER_CONVERACIONES_USUARIO
(
	@id_usuario INT
)
AS
BEGIN
    SELECT * FROM conversacion WHERE id_user1 = @id_usuario OR id_user2 = @id_usuario;
END

EXEC VER_CONVERACIONES_USUARIO 2;

-- PROC 5 ----------------
-- PERMITE VER LA LISTA DE LOS ULTIMOS 100 MENSAJES ENTRE 2 PERSONAS UNICAMENTE
CREATE PROC VER_MENSAJES_USUARIO
(
	@user_1 INT,
	@user_2 INT
)
AS
BEGIN
	DECLARE @conversacion INT
	SET @conversacion = (SELECT id_conversacion FROM conversacion WHERE id_user1 = @user_1 AND id_user2 = @user_2
                        OR id_user1 = @user_2 AND id_user2 = @user_1);

	SELECT TOP(100) * FROM mensaje 
	WHERE id_conversacion = @conversacion
	ORDER BY mensaje.fecha DESC
END

-- EJEMPLO- VER TODOS LOS MENSAJES ENTRE EL USUARIO 1 y 2
EXEC VER_MENSAJES_USUARIO 1,2;

-- CREACION DE INDICES
-- INDEX 1
CREATE INDEX PAISNB ON pais(nombre)
GO

-- INDEX 2
CREATE INDEX INDEX_TIPO_US ON tipo_usuario(nombre_tipo)
GO

-- INDEX 3
CREATE INDEX INDEX_USERNAME ON usuario(username)
GO

-- INDEX 4
CREATE INDEX INDEX_CATEGORIA ON categoria(nombre_cat);

-- INDEX 5
CREATE INDEX INDEX_FAV ON favoritos(ruta_url)
GO

-- VISTA 2
-- Vista para mostrar informacion de cada usuario de manera mas presentable para el front-end de la pagina web
CREATE VIEW VISTA_USUARIO
AS
	SELECT u.id_usuario, u.nombre, u.apellido_pat, u.apellido_mat, u.correo, u.avatar, c.nombre as municipio,
			u.telefono, u.sitio_web, u.biografia, u.username, u.contrasenia, t.nombre_tipo as tipo_usuario
	FROM usuario u
	INNER JOIN tipo_usuario t ON u.tipo_usuario = t.id_tipo_usuario
	INNER JOIN ciudad c ON u.municipio = c.id_ciudad;
GO

SELECT * FROM VISTA_USUARIO;

-- VISTA 3
-- VISTA PARA MOSTRAR LAS HISTORIAS JUNTO CON SU PROLOGO Y SINOPSIS Y CATEGORIA
CREATE VIEW VISTA_HISTORIA
AS
	SELECT h.id_historia, u.id_usuario as id_autor, u.nombre as autor, p.contenido as prologo, h.titulo, h.portada_url, s.contenido as sinopsis, c.nombre_cat as categoria
	FROM prologo p
	INNER JOIN historia h ON p.id_prologo = h.id_prologo
	INNER JOIN sinopsis s ON h.id_sinopsis = s.id_sinopsis
	INNER JOIN categoria c ON c.id_categoria = h.id_categoria
    INNER JOIN usuario_escribe u_e ON u_e.id_historia = h.id_historia
	INNER JOIN usuario u ON u_e.id_autor = u.id_usuario;
GO

SELECT * FROM VISTA_HISTORIA;

-- VISTA 4
-- VISTA PARA VER TODOS LOS CAPITULOS CON EL NOMBRE DE SU HISTORIA
CREATE VIEW VISTA_CAPITULOS_HIST
AS
	SELECT c.id_capitulo, c.titulo as capitulo, c.contenido, h.titulo
	FROM capitulo c
	INNER JOIN historia h ON h.id_historia = c.id_historia
GO

SELECT * FROM VISTA_CAPITULOS_HIST;

-- VISTA 5
-- VISTA PARA VISUALIZAR LA CALIFICACION DE CADA HISTORIA POR SU NOMBRE
CREATE VIEW VISTA_CALIFICACIONES
AS
	SELECT h.id_historia, h.titulo, h.portada_url, c.cant_estrellas as estrellas, u.nombre as usuario
	FROM califica_historia c
	INNER JOIN historia h ON h.id_historia = c.id_historia
	INNER JOIN usuario u ON u.id_usuario = c.id_usuario
GO

SELECT * FROM VISTA_CALIFICACIONES;

-- TRIGGER 2
-- VALIDAR QUE NO SE INGRESE UNA CALIFICACION ENTRE 1 Y 5
CREATE TRIGGER VALIDAR_CALIFICACION
ON califica_historia
FOR INSERT
AS
BEGIN
	DECLARE @estrellas INT
	SET @estrellas = (SELECT cant_estrellas FROM inserted);

	IF @estrellas <= 5 AND @estrellas >= 1
	BEGIN
		PRINT 'INSERCION EXITOSA';
	END
	ELSE
	BEGIN
		RAISERROR('LA CALIFICACION DEBE SER ENTRE 1 Y 5 ESTRELLAS', 16, 1);
		ROLLBACK TRANSACTION
	END
END

-- EJEMPLO DE ERROR
INSERT INTO califica_historia VALUES(2, 1, 6);

-- TRIGGER 3
-- VALIDAR QUE LA APROBACION DE UNA HISTORIA SOLO TENGA ESTATUS 0 = NO PENDIENTE,  1 = NO APROBADA, 2 = APROBADO
CREATE TRIGGER VALIDAR_ESTATUS_HISTORIA
ON admin_revision
FOR INSERT
AS
BEGIN
	DECLARE @estado INT
	SET @estado = (SELECT inserted.id_estatus FROM inserted);

	IF(@estado = 0)
	BEGIN
		PRINT 'HISTORIA PENDIENTE'
	END
	IF(@estado = 1)
	BEGIN
		PRINT 'HISTORIA NO APROBADA'
	END
	IF(@estado = 2)
	BEGIN
		PRINT 'HISTORIA APROBADA'
	END
	
	IF(@estado < 0 OR @estado > 2)
	BEGIN
		RAISERROR('EL ESTATUS DEBE ESTAR EN RANGO DE 0 A 2', 16, 1);
		ROLLBACK TRANSACTION
	END
END

-- EJEMPLO DE INSERCION EXITOSA
EXEC revision_historias 1, 3, 2;

-- EJEMPLO DE ERROR CON TRIGGER
EXEC revision_historias 1, 3, 3;

CREATE PROCEDURE DEVOLVER_HISTORIA
(
	@id_autor int
)
AS
BEGIN
	IF EXISTS(SELECT * FROM usuario WHERE id_usuario=@id_autor)
	BEGIN
		SELECT h.id_historia, h.portada_url, h.titulo, s.contenido as sinopsis, estado.nombre_estatus as estatus
		FROM historia h
		INNER JOIN sinopsis s on s.id_sinopsis=h.id_sinopsis
		INNER JOIN admin_revision admi on admi.id_historia=h.id_historia
		INNER JOIN status_historia estado ON estado.id_status = admi.id_estatus
		INNER JOIN usuario_escribe u_E ON u_e.id_historia = h.id_historia
		INNER JOIN usuario u ON u.id_usuario = u_e.id_autor
		WHERE u_e.id_autor = @id_autor;
	END
END
GO

-- EJEMPLO TEST
EXEC DEVOLVER_HISTORIA 13;

CREATE PROC SP_INSERTAR_HISTORIA
(
	@id_autor INT,
	@titulo VARCHAR(50),
	@portada_url VARCHAR(150),
	@sinopsis TEXT,
	@prologo TEXT,
	@id_categoria INT
)
AS
BEGIN
	BEGIN TRANSACTION
		DECLARE @id_sinopsis INT
		DECLARE @id_prologo INT
		DECLARE @id_historia INT

		INSERT INTO sinopsis
		VALUES('SINOPSIS', @sinopsis);

		SET @id_sinopsis = SCOPE_IDENTITY();

		INSERT INTO prologo
		VALUES('PROLOGO', @prologo);

		SET @id_prologo = SCOPE_IDENTITY();

		INSERT INTO historia VALUES(@titulo, @portada_url, @id_sinopsis, @id_prologo, @id_categoria);
		SET @id_historia = SCOPE_IDENTITY();

		INSERT INTO usuario_escribe VALUES(@id_autor, @id_historia, GETDATE());

		-- ESTATUS 0 ES PENDIENTE
		INSERT INTO admin_revision VALUES(null, @id_historia, GETDATE(), 0)

	COMMIT TRANSACTION
END
GO

-- EJEMPLO INSERTAR HISTORIA
EXEC SP_INSERTAR_HISTORIA 3, 'titulo de prueba', 'http://www.estaesunaprueba.com', 'sinopsis de prueba', 'prologo de prueba', 3;


CREATE PROC SP_ELIMINAR_HISTORIA
(
	@id_historia INT
)
AS
BEGIN
	BEGIN TRY
		DELETE FROM usuario_escribe WHERE id_historia = @id_historia;
		DELETE FROM admin_revision WHERE id_historia = @id_historia;
		DELETE FROM capitulo WHERE id_historia = @id_historia;

		DECLARE @id_prologo INT
		DECLARE @id_sinopsis INT

		SET @id_sinopsis = (SELECT id_sinopsis FROM historia WHERE id_historia = @id_historia);
		SET @id_prologo = (SELECT id_prologo FROM historia WHERE id_historia = @id_historia);

		DELETE FROM historia WHERE id_historia = @id_historia;

		DELETE FROM sinopsis WHERE id_sinopsis = @id_sinopsis;
		DELETE FROM prologo WHERE id_prologo = @id_prologo;
	END TRY
	BEGIN CATCH
		RAISERROR('Se producido un error, se revertiran los cambios', 16, 1)
		ROLLBACK TRANSACTION
	END CATCH
END
GO

--EXEC SP_ELIMINAR_HISTORIA 18;